<?php

/**
 * @var WebModule $webModule
 * @var Translate $outputlangs
 */

?>


<?php include __DIR__ . '/_headbanner.tpl.php' ?>

<?php include __DIR__ . '/_module_infos.tpl.php'; ?>

<?php include __DIR__ . '/_documents_links.tpl.php'; ?>

<?php
// Affichage de la description longue
if(!empty($webModule->array_options['options_desc_'.$language])) {
	print '<p><br/></p>';
	print $webModule->array_options['options_desc_'.$language];
}
?>


<p>
	<br/>
</p>


<table width="100%" style="border: 0; background-color: #ffffff;">
	<tbody>
	<tr>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d">COMPRIS DANS L'ACHAT</span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>

			<p>
				<span class="icon-check right" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d"></span>L’accès aux mises à jour du module et certaines évolutions</span><br/>
				<span class="icon-check right" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d">Corrections fonctionnelles du module</span><br/>
			</p>

			<p style="text-align: left;">
				<span class="icon-warning-sign" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d">La formation et l'aide à l'installation du module ne sont pas inclus dans le prix</span>
			</p>

		</td>
		<td> </td>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d">MISES A JOUR</span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>
			<p style="text-align: left;">
				<span class="icon-puzzle-piece" style="color: #2d2d2d">&nbsp;</span>  <span style="color: #2d2d2d">Les correctifs du module seront publiés sur Dolistore</span>
			</p>
			<p> </p>
		</td>
		<td> </td>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d"><strong>CONTACT</strong></span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>
			<p>
				<span style="color: #2d2d2d"><strong>Votre module présente une anomalie technique ? </strong><br/>Nos ingénieurs peuvent intervenir à distance pour 450€&nbsp;HT&nbsp;/&nbsp;demi-journée.</span>
			</p>
			<p>
				<span style="color: #2d2d2d"><strong>Votre module présente une anomalie technique ?</strong>
					<br/>Envoyez nous un e-mail via le formulaire de contact : <a href="https://www.atm-consulting.fr/contact-dolibarr/dolistore-contactez-nous/">"dolistore contactez-nous"</a>
					en nous expliquant votre problème en détail (si votre demande n'est pas claire nous ne serons pas en mesure de vous répondre).</span>
			</p>
		</td>
	</tr>
	</tbody>
</table>


<p>
	<br/>
</p>

<?php include __DIR__ . '/_social_media.tpl.php' ?>
