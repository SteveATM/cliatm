<?php

/**
 * @var WebModule $webModule
 * @var Translate $outputlangs
 */

?>

<table width="100%">
	<tbody>
	<tr>
		<td style="font-family: arial, sans-serif; width: 50%; padding: 20px;" >
			<p> </p>
			<p><img src="https://www.atm-consulting.fr/wp-content/uploads/2018/06/Logo-ATM-bleu_orange-transparent.png" alt="Logo ATM Consulting" width="243" height="57" /></p>
			<p> </p>
		</td>
		<td style="font-family: arial, sans-serif;  width: 50%; padding: 20px; text-align: center;" >
			<img src="https://www.atm-consulting.fr/wp-content/uploads/2018/06/Dolibarr-PP-Rond-1-ombre.png" alt="ATM Consulting Dolibarr Prefered Partner" width="73" height="73" />
		</td>
	</tr>
	</tbody>
</table>
