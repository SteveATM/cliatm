
<table  width="100%" style="border: 0; background-color: #f1f1f1;">
	<tbody>
	<tr style="border: none;">
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 50%; padding: 20px; border: 1px solid #f1f1f1; margin: 0; vertical-align: top;">
			<p>
				<span class="icon-caret-right right">&nbsp;</span> <span style="color: #2d2d2d"><strong><span ><?php print $outputlangs->trans('Publisher'); ?> :</span> </strong>ATM Consulting</span><br/>
				<span class="icon-caret-right right">&nbsp;</span> <span style="color: #2d2d2d"><span ><strong><?php print $outputlangs->trans('Licence'); ?> :</strong></span> AGPL</span><br/>
				<span class="icon-caret-right right">&nbsp;</span> <span style="color: #2d2d2d"><span ><strong><?php print $outputlangs->trans('ModuleVersion'); ?> :</strong></span> <span class="module_version_desc">auto</span></span>
			</p>
		</td>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 50%; padding: 20px; border: 1px solid #f1f1f1; vertical-align: top;">
			<p>
				<span class="icon-caret-right right">&nbsp;</span> <span style="color: #2d2d2d"><span ><strong><?php print $outputlangs->trans('Compatibility'); ?> :</strong></span> Dolibarr <span class="dolibarr_min_desc">auto</span> - <span class="dolibarr_max_desc">auto</span></span><br/>
				<span class="icon-caret-right right">&nbsp;</span> <span style="color: #2d2d2d"><span ><strong><?php print $outputlangs->trans('Languages'); ?> :</strong></span>   <?php include __DIR__ . '/_langs.tpl.php' ?></span>
			</p>
		</td>
	</tr>
	</tbody>
</table>
