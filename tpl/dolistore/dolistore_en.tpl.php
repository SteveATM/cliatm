<?php

/**
 * @var WebModule $webModule
 */

?>


<?php include __DIR__ . '/_headbanner.tpl.php' ?>

<?php include __DIR__ . '/_module_infos.tpl.php'; ?>

<?php include __DIR__ . '/_documents_links.tpl.php'; ?>

<?php
// Affichage de la description longue
if(!empty($webModule->array_options['options_desc_'.$language])) {
	print '<p><br/></p>';
	print $webModule->array_options['options_desc_'.$language];
}
?>


<p>
	<br/>
</p>


<table width="100%" style="border: 0; background-color: #ffffff;">
	<tbody>
	<tr>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d">INCLUDED</span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>

			<p>
				<span class="icon-check right" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d"></span>Access to module updates and some evolutions</span><br/>
				<span class="icon-check right" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d">Functional correction of the module</span><br/>
			</p>

			<p style="text-align: left;">
				<span class="icon-warning-sign" style="color: #2d2d2d">&nbsp;</span> <span style="color: #2d2d2d">Please note that the training and help with the installation are not included</span>
			</p>

		</td>
		<td> </td>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d">UPDATES</span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>
			<p style="text-align: left;">
				<span class="icon-puzzle-piece" style="color: #2d2d2d">&nbsp;</span>  <span style="color: #2d2d2d">Corrective module updates will be published in the Dolistore</span>
			</p>
			<p> </p>
		</td>
		<td> </td>
		<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; vertical-align: top;">
			<h4 style="text-align: center;"><span style="color: #2d2d2d"><strong>CONTACT</strong></span></h4>
			<hr style="border: none; border-top: 1px solid #d7d7d7;"/>
			<p>
				<span style="color: #2d2d2d"><strong>You need help? </strong><br/>Our engineers can intervene remotly for  450.00 €/ ½ day excluding tax </span>
			</p>
			<p >
				<span style="color: #2d2d2d"><strong>Your module has a functional anomaly&nbsp;?</strong><br/>Start by updating it and send us an email here : <a href="https://www.atm-consulting.fr/contact-dolibarr/dolistore-contactez-nous/">"contact us"</a> if the problem persists. Please explain clearly the details of your issue otherwise we won’t be able to answer.</span>
			</p>
		</td>
	</tr>
	</tbody>
</table>


<p>
	<br/>
</p>


<?php include __DIR__ . '/_social_media.tpl.php' ?>

