<?php

/**
 * @var WebModule $webModule
 * @var Translate $outputlangs
 */

$moduleLangCompatible = explode(',', $webModule->array_options['options_language']);

$langsDolistoreMapping = array(
	'FR' => 2, // fr
	'GB' => 1, // en
	'US' => 1, // en
	'ES' => 3,
	'IT' => 4, // it
	'DE' => 5
);

if(is_array($moduleLangCompatible)){
	foreach ($moduleLangCompatible as $code){
		if(isset($langsDolistoreMapping[$code])){
			print ' <img style="vertical-align:middle;" src="https://www.dolistore.com/img/l/'.intval($langsDolistoreMapping[$code]).'.jpg" alt="'.$code.'" title="'.$code.'" width="16" height="11" />';
		}
		else{
			print ' '.$code;
		}
	}
}


