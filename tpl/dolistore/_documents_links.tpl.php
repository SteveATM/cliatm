
<p>
	<br/>
</p>


<table width="100%" style="border: 0; background-color: #ffffff;">
	<tbody>
	<tr>
		<?php if(!empty($webModule->doc_url)) { ?>
			<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; text-align: center;">
				<h4 style="text-align: center;"><span style="color: #2d2d2d"><strong><?php print $outputlangs->trans('DOCUMENTATION'); ?></strong></span></h4>
				<p style="text-align: center;"><a target="_blank" href="<?php print $webModule->doc_url; ?>"><img style="display: block; margin-left: auto; margin-right: auto;" src="https://www.atm-consulting.fr/wp-content/uploads/2018/06/léments-fiche-dolistore-2-e1530089608621.png" alt="Documentation du module" width="64" height="50" /></a></p>
			</td>
		<?php } ?>

		<td> </td>

		<?php if(!empty($webModule->array_options['options_video_link'])) { ?>
			<td style="font-family: arial, sans-serif; background-color: #f1f1f1; width: 33%; padding: 20px; text-align: center;">
				<h4><span style="color: #2d2d2d"><strong><?php print $outputlangs->trans('TUTO_VIDEO'); ?><br /></strong></span></h4>
				<p>
					<a target="_blank" href="<?php print $webModule->array_options['options_video_link']; ?>">
						<img src="https://www.atm-consulting.fr/wp-content/uploads/2018/06/léments-fiche-dolistore-e1530089587951.png" alt="<?php print $outputlangs->trans('TutoVideoTitle'); ?>" width="65" height="50" />
					</a>
				</p>
			</td>
		<?php } ?>

		<td> </td>

		<?php if(!empty($webModule->array_options['options_main_demo_url'])) { ?>
			<td style="font-family: arial, sans-serif; background-color: #f1f1f1;  width: 33%; padding: 20px; text-align: center;">
				<p style="text-align: center;"> </p>
				<h4 style="text-align: center;"><span style="color: #2d2d2d"><strong><?php print $outputlangs->trans('ONLINE_DEMO'); ?></strong></span></h4>
				<p style="text-align: center;">
					<a target="_blank" href="<?php print $webModule->array_options['options_main_demo_url']; ?>">
						<img src="https://www.atm-consulting.fr/wp-content/uploads/2018/06/léments-fiche-dolistore-3-e1530090106320.png" alt="<?php print $outputlangs->trans('OnlineDemoTitle'); ?>" width="65" height="50" />
					</a>
				</p>
				<p style="text-align: center;">  </p>
			</td>
		<?php } ?>
	</tr>
	</tbody>
</table>

