<?php

/**
 * genarate Dolistore card from webhost module
 *
 * @param WebModule $webModule
 * @param string    $language
 */
function generateDolistoreDescription(WebModule $webModule, $language = 'fr' ) {
	global $db, $conf;
	$webModule->fetch_optionals();

	$TLangs = array(
		'en' => array(
			'langCode' => 'en_US',
			'tplFile' => 'dolistore_en.tpl.php'
		),
		'fr' => array(
			'langCode' => 'fr_FR',
			'tplFile' => 'dolistore_fr.tpl.php'
		)
	);

	// security test
	if(!isset($TLangs[$language])){
		return '';
	}

	$newlang = !empty($TLangs[$language]['langCode'])?$TLangs[$language]['langCode']:"";

	$outputlangs = new Translate("", $conf);
	$outputlangs->setDefaultLang($newlang);
	$res = $outputlangs->load('cliatmdolistore@cliatm');
	$tplFile = __DIR__ .'/../tpl/dolistore/'.$TLangs[$language]['tplFile'];

	if(file_exists($tplFile)){
		ob_start();
		include $tplFile;
		return ob_get_clean();
	}
}
