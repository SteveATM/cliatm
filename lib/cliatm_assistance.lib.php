<?php
/*
 * Functions used to retrieve informations about customer service
 * - Assistance invoiced to customer
 * - Assistance consumed bu customer
 * - Last delivery to customer
 * - Commercial and project manager informations
 */

/**
 * @param int $projectId
 * @return array|false
 * @throws Exception
 */
function getAssistanceInfosFromProject($projectId) {
	global $db,$langs,$user,$conf;

	require_once DOL_DOCUMENT_ROOT . '/projet/class/project.class.php';
	$project = new Project($db);
	$project->fetch($projectId);
	$project->fetch_thirdparty();

	$langs->load('cliatm@cliatm');
	$langs->load('contracts');
	$langs->load('commercial');
	$langs->load('main');
	$langs->load('bills');

	$project->fetch_thirdparty();

	// Data to display
	$data = array();

	// Commercial
	$TCommerciaux = $project->thirdparty->getSalesRepresentatives($user);
	$data['commercial'] = '';
	if(! empty($TCommerciaux)) {
		$comm = new User($db);
		$comm->fetch($TCommerciaux[0]['id']);
		$data['commercial'] = $comm;
	}

	// Chef de projets
	$data['cdp'] = '';
	if(!empty($project->thirdparty->array_options['options_fk_cdp'])) {
		$cdp = new User($db);
		$cdp->fetch($project->thirdparty->array_options['options_fk_cdp']);
		$data['cdp'] = $cdp;
	}

	// N-1
	$datedeb = date('Y-01-01', strtotime('-1 year'));
	$datefin = date('Y-12-31', strtotime('-1 year'));
	// Heures facturées / consommées
	$data['purchasedNM1'] = getHoursPurchased($project->id, $datedeb, $datefin);
	$data['consumedNM1'] = getHoursConsumed($project->id, $datedeb, $datefin);

	// N
	$datedeb = date('Y-01-01');
	$datefin = date('Y-12-31');
	// Heures facturées / consommées
	$data['purchasedN'] = getHoursPurchased($project->id, $datedeb, $datefin);
	$data['consumedN'] = getHoursConsumed($project->id, $datedeb, $datefin);

	// Expéditions de moins de 3 mois ?
	$datedeb = date('Y-m-d', strtotime('-3 month'));
	$datefin = date('Y-m-d');
	$data['retours'] = getRecentExpeditions($project->id, $datedeb, $datefin);

	return $data;
}


/**
 * Renvoie le nombre d'heures d'assistance facturées entre 2 dates
 *
 * @param $projectid Id du projet
 * @param $datedeb Date de début de la période à prendre en compte
 * @param $datefin Date de fin de la période à prendre en compte
 * @return float|int Nombre d'heures facturées liées au projet
 * @throws Exception
 */
function getHoursPurchased($projectid, $datedeb, $datefin) {
	global $db;

	$sql= "SELECT s.nom, f.ref as facnumber, p.label, (CASE WHEN f.type = 0 THEN fd.qty ELSE fd.qty * -1 END) qte, pro.ref as projet, fd.date_start, fd.date_end, p.ref
			FROM ".MAIN_DB_PREFIX."facturedet fd
			LEFT JOIN ".MAIN_DB_PREFIX."facture f ON f.rowid = fd.fk_facture
			LEFT JOIN ".MAIN_DB_PREFIX."product p ON p.rowid = fd.fk_product
			LEFT JOIN ".MAIN_DB_PREFIX."societe s ON s.rowid = f.fk_soc
			LEFT JOIN ".MAIN_DB_PREFIX."projet pro ON pro.rowid = f.fk_projet
			WHERE p.ref IN ('SP015', 'SP020', 'SP022', 'SP026', 'SP029', 'SP027', 'SP031', 'SP028')
			AND f.datef BETWEEN '$datedeb' AND '$datefin'
			AND pro.rowid = $projectid
			AND f.fk_statut > 0";

	$resql = $db->query($sql);

	$total = 0;
	while($obj = $db->fetch_object($resql)) {
		if($obj->ref == 'SP028' && $obj->qte < 0) continue; // Régul négative = avoir commercial sur le dépassement. On ne le déduit pas

		$nb_mois = 1;
		if($obj->ref != 'SP028' && strtotime($obj->date_start) < strtotime('2019-08-01')) { // Factures avant le 1er aout 2019, il faut multiplier la quantité par la périodicité
			$d1 = new DateTime($obj->date_start);
			$d2 = new DateTime($obj->date_end);
			$interval = $d1->diff($d2);
			$nb_mois = $interval->m + 1;
		}

		$total += (float)$obj->qte * $nb_mois;
	}

	return $total;
}

/**
 * Renvoie le nombre d'heures consommées entre 2 dates
 *
 * @param $projectid Id du projet
 * @param $datedeb  Date de début de la période à prendre en compte
 * @param $datefin Date de fin de la période à prendre en compte
 * @return float|int Nombre d'heures consommées liées au projet
 */
function getHoursConsumed($projectid, $datedeb, $datefin) {
	global $db;

	$sql= "SELECT s.nom, p.ref, p.title, SUM(ptt.task_duration / 3600) heure
			FROM llx_projet_task_time ptt
			LEFT JOIN llx_projet_task pt ON pt.rowid = ptt.fk_task
			LEFT JOIN llx_projet p ON p.rowid = pt.fk_projet
			LEFT JOIN llx_societe s ON s.rowid = p.fk_soc
			WHERE pt.label = 'Tickets'
			AND ptt.task_date BETWEEN '$datedeb' AND '$datefin'
			AND p.rowid = $projectid
			GROUP BY p.rowid";

	$resql = $db->query($sql);

	$total = 0;
	if($obj = $db->fetch_object($resql)) {
		$total = (float)$obj->heure;
	}

	return $total;
}

function getRecentExpeditions($projectid, $datedeb, $datefin) {
	global $db;
	require_once DOL_DOCUMENT_ROOT . '/expedition/class/expedition.class.php';

	$sql= "SELECT e.rowid
			FROM llx_expedition e
			LEFT JOIN llx_expeditiondet ed ON e.rowid = ed.fk_expedition
			LEFT JOIN llx_commandedet cd ON cd.rowid = ed.fk_origin_line
			LEFT JOIN llx_product p ON cd.fk_product = p.rowid
			LEFT JOIN llx_categorie_product cp ON cp.fk_product = p.rowid
			LEFT JOIN llx_categorie c ON cp.fk_categorie = c.rowid
			LEFT JOIN llx_societe s ON e.fk_soc = s.rowid
			LEFT JOIN llx_projet pro ON pro.fk_soc = s.rowid
			WHERE COALESCE(e.date_delivery, e.date_valid) BETWEEN '$datedeb' AND '$datefin'
			AND c.rowid IN (23, 45)
			AND pro.rowid = $projectid
			GROUP BY e.rowid";

	$resql = $db->query($sql);

	$TExped = array();
	while($obj = $db->fetch_object($resql)) {
		$exp = new Expedition($db);
		$exp->fetch($obj->rowid);
		$TExped[] = $exp;
	}

	return $TExped;
}
