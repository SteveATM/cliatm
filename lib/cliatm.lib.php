<?php

/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015 ATM Consulting <support@atm-consulting.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file		lib/cliatm.lib.php
 * 	\ingroup	cliatm
 * 	\brief		This file is an example module library
 * 				Put some comments here
 */
function cliatmAdminPrepareHead() {
    global $langs, $conf;

    $langs->load("cliatm@cliatm");

    $h = 0;
    $head = array();

    $head[$h][0] = dol_buildpath("/cliatm/admin/cliatm_setup.php", 1);
    $head[$h][1] = $langs->trans("Parameters");
    $head[$h][2] = 'settings';
    $h++;
    $head[$h][0] = dol_buildpath("/cliatm/admin/propalboard_setup.php", 1);
    $head[$h][1] = $langs->trans("PropalBoard");
    $head[$h][2] = 'propalboard';
    $h++;
    $head[$h][0] = dol_buildpath("/cliatm/admin/cliatm_about.php", 1);
    $head[$h][1] = $langs->trans("About");
    $head[$h][2] = 'about';
    $h++;

    // Show more tabs from modules
    // Entries must be declared in modules descriptor with line
    //$this->tabs = array(
    //	'entity:+tabname:Title:@cliatm:/cliatm/mypage.php?id=__ID__'
    //); // to add new tab
    //$this->tabs = array(
    //	'entity:-tabname:Title:@cliatm:/cliatm/mypage.php?id=__ID__'
    //); // to remove a tab
    complete_head_from_modules($conf, $langs, $object, $head, $h, 'cliatm');

    return $head;
}

/**
 * Get date in the future by calculate with duration
 *
 * @param int $dateInitial
 * @param int $duration
 * @return array
 */
function getFutureDateWithWorkingDays($dateInitial, $duration) {

    // Used to determine future date
    $dateReaFull = dol_time_plus_duree($dateInitial, (int) $duration, 'd');
    //print 'date début : '.dol_print_date($dateInitial) . ' / durée initiale : '.$duration;
    // nombre de jours férié et ajout à date initial
    $nbNoWorkingDays = num_public_holiday($dateInitial, $dateReaFull, $countrycode = 'FR', $lastday = 0);
    $dateReaWorkingDays = dol_time_plus_duree($dateInitial, (int) $duration + $nbNoWorkingDays, 'd');

    $nbNoWorkingDaysRea = num_public_holiday($dateInitial, $dateReaWorkingDays, $countrycode = 'FR', $lastday = 0);
    $nbWorkingDaysLeft = $nbNoWorkingDaysRea - $nbNoWorkingDays;
    // Recursive loop if working days left
    if($nbWorkingDaysLeft > 0) {
        $resArray = getFutureDateWithWorkingDays($dateReaWorkingDays, (int) $nbWorkingDaysLeft);
        $nbNoWorkingDays += $resArray['nbNoWorkingDays'];
        $dateReaWorkingDays = $resArray['dateReaWorkingDays'];
    }

    $res = array(
        'nbNoWorkingDays' => $nbNoWorkingDays,
        'dateReaWorkingDays' => $dateReaWorkingDays
    );
    return $res;
}

/**
 * Get ATM propal board
 *
 * @param Propal $object
 * @param int $withId		Add html `id` attribute onto `div tag` (used by JS)
 * @param int $hidden		Set to 1 to hide div
 * @return string
 */
function getAtmPropalBoard($object, $withId = 1, $hidden = 0) {

    global $db, $conf, $langs;
    $langs->load('products');

    require_once DOL_DOCUMENT_ROOT.'/core/lib/date.lib.php';
    require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';

    $cat = new Categorie($db);
    $qtyCategDev = $qtyCategProjectDir = $qtyCategProjectAccomp = $qtyCategProjectProv = $montantCategProjectMaintenance = $montantTech = 0;
    $baseJourDev = $conf->global->ATM_PRBOARD_RATIO_BASE_DEV;

    foreach($object->lines as $line) {

        if($line->product_type != 9) {

            // Check if product is in categ ATM_PRBOARD_CATEG_DEV
            $cat->id = $conf->global->ATM_PRBOARD_CATEG_DEV;
            $res = $cat->containsObject('product', $line->fk_product);
            if($res) {
                $qtyCategDev += $line->qty;
                $montantTech += $line->qty * $line->subprice;   // On refait le calcul car si la ligne est NC, le total_ht vaut 0
            }

            // Check if product is in categ ATM_PRBOARD_CATEG_PROJECT
            $cat->id = $conf->global->ATM_PRBOARD_CATEG_PROJECT;
            $res = $cat->containsObject('product', $line->fk_product);
            if($res) {
                $qtyCategProjectDir += $line->qty;
            }

            // Check if product is in categ ATM_PRBOARD_CATEG_ACCOMPA
            $cat->id = $conf->global->ATM_PRBOARD_CATEG_ACCOMPA;
            $res = $cat->containsObject('product', $line->fk_product);
            if($res) {
                $qtyCategProjectAccomp += $line->qty;
            }

            // Check if product is ATM_PRBOARD_SERVICE_PROVISION
            if($line->fk_product == $conf->global->ATM_PRBOARD_SERVICE_PROVISION) {
                $qtyCategProjectProv += $line->qty;
            }

            // Check if product is ATM_PRBOARD_SERVICE_MAINTENANCE
            if($line->fk_product == $conf->global->ATM_PRBOARD_SERVICE_MAINTENANCE) {
                $montantCategProjectMaintenance += $line->qty * $line->subprice;    // On refait le calcul car si la ligne est NC, le total_ht vaut 0
            }
        }
    }
    /*
     *  - nombre de jour de dev = quantité des services de la catégorie 'développements'
      - Nombre de jour détaillé par service CDP
      - direction => ratio de 1JH pour 5JH de dev
      - accompagnement
      - ratio de 1JH pour 5JH si devis > 30k
      - ratio de 0,5JH pour 5JH si devis < 30k
      - durée de réalisation du projet à caculer en jour ouvré = nb jours de dev et import * 4 + nb jours CDP
     */
    $out = '';
    $out .= '<div '.($withId ? 'id="propalBoard"' : '' ).' '.($hidden ? 'style="display: none"' : '' ).'>';
    $out .= '<table class="border" width="100%">';

    // Nombre de jour de dev
    $out .= '<tr class="liste_titre"><td colspan="3">'.$langs->trans('ATM_PRBOARD_Deve').'</td></tr>';
    $out .= '<tr><td width="210">Total jours développement</td><td colspan="2"> '.$qtyCategDev.'</td>';

    // Direction de projet
    $out .= '<tr class="liste_titre"><td>'.$langs->trans('Service').'</td><td>'.$langs->trans('ATM_PRBOARD_Present').'</td><td>'.$langs->trans('ATM_PRBOARD_Calculated').'</td></tr>';
    $ProjDirCalcul = $conf->global->ATM_PRBOARD_RATIO_DIRECTION * ( $qtyCategDev / $conf->global->ATM_PRBOARD_RATIO_BASE_DEV);
    $styleProjectDir = '';
    if($qtyCategProjectDir < $ProjDirCalcul) $styleProjectDir = ' style="font-weight: bold; color: red;"';
    $out .= '<tr><td>'.$langs->trans('ATM_PRBOARD_TotalDirectionProject').'</td><td'.$styleProjectDir.'> '.$qtyCategProjectDir.'</td><td>'.$ProjDirCalcul.'</td></tr>';

    // Accompagnement
    $ratioAccomp = $conf->global->ATM_PRBOARD_RATIO_ACCOMP_NORM;
    if($object->total_ht > $conf->global->ATM_PRBOARD_AMOUNT_GAP) $ratioAccomp = $conf->global->ATM_PRBOARD_RATIO_ACCOMP;
    $ProjDirAccompCalcul = $ratioAccomp * ( $qtyCategDev / $conf->global->ATM_PRBOARD_RATIO_BASE_DEV );

    $styleProjectAccomp = '';
    if($qtyCategProjectAccomp < $ProjDirAccompCalcul) $styleProjectAccomp = ' style="font-weight: bold; color: red;"';
    $out .= '<tr><td>'.$langs->trans('ATM_PRBOARD_TotalAccompProject').'</td><td'.$styleProjectAccomp.'> '.$qtyCategProjectAccomp.'</td><td>'.$ProjDirAccompCalcul.'</td></tr>';

    // Provision
    $ratio_provision = $conf->global->ATM_PRBOARD_RATIO_PROVISION;
    $ProjDirProvCalcul = $ratio_provision / 100 * $qtyCategDev;

    $styleProjectProv = '';
    if($qtyCategProjectProv < $ProjDirProvCalcul) $styleProjectProv = ' style="font-weight: bold; color: red;"';
    $out .= '<tr><td>'.$langs->trans('ATM_PRBOARD_TotalProvision').'</td><td'.$styleProjectProv.'> '.$qtyCategProjectProv.'</td><td>'.$ProjDirProvCalcul.'</td></tr>';

    // Maintenance
    $ratio_maintenace = $conf->global->ATM_PRBOARD_RATIO_MAINTENANCE;
    $ProjDirMaintenanceCalcul = $ratio_maintenace / 100 * $montantTech / 12;
    if($ProjDirMaintenanceCalcul < 50) $ProjDirMaintenanceCalcul = 50;  // Montant mensuel minimum de 50€ pour la maintenance

    $styleProjectMaintenance = '';
    if($montantCategProjectMaintenance < $ProjDirMaintenanceCalcul) $styleProjectMaintenance = ' style="font-weight: bold; color: red;"';
    $out .= '<tr><td>'.$langs->trans('ATM_PRBOARD_TotalMaintenance').'</td><td'.$styleProjectMaintenance.'>'.price($montantCategProjectMaintenance, 0, '', 1, -1, -1, 'auto').'</td><td>'.price($ProjDirMaintenanceCalcul, 0, '', 1, -1, -1, 'auto').'</td></tr>';

    // Totaux
    $out .= '<tr class="liste_titre"><td colspan="3">'.$langs->trans('ATM_PRBOARD_Totals').'</td></tr>';
    // Durée totale devis
    $dureePropale = $qtyCategDev + $qtyCategProjectDir + $qtyCategProjectAccomp;
    $out .= '<tr><td  class="liste_titre">'.$langs->trans('ATM_PRBOARD_DureeTotaleDevis').'</td><td colspan="2">'.$dureePropale.'</td></tr>';

    // Calcul durée avec ratio
    $dureeCalculBrut = ( $qtyCategDev * $conf->global->ATM_PRBOARD_FACTOR_DURATION ) + $qtyCategProjectAccomp;

    $dateInitial = dol_now();
    $resDates = getFutureDateWithWorkingDays($dateInitial, $dureeCalculBrut);

    $dureeWorkingDaysWeeks = convertSecondToTime($dureeCalculBrut * $conf->global->MAIN_DURATION_OF_WORKDAY, 'all', $conf->global->MAIN_DURATION_OF_WORKDAY, $conf->global->ATM_PRBOARD_RATIO_BASE_DEV);
    $dureeFormule = "(".$qtyCategDev.'*'.$conf->global->ATM_PRBOARD_FACTOR_DURATION.') + '.$qtyCategProjectAccomp;
    $out .= '<tr><td  class="liste_titre">'.$langs->trans('ATM_PRBOARD_DureeReaCalculJour').'</td><td colspan="2">'.$dureeFormule.' = '.$dureeWorkingDaysWeeks.'</td></tr>';
    $out .= '<tr><td  class="liste_titre">'.$langs->trans('ATM_PRBOARD_DatePrevisionnelleWorkingDays').'</td><td colspan="2">'.dol_print_date($resDates['dateReaWorkingDays']).' ('.$langs->trans('ATM_PRBOARD_JoursOff', $resDates['nbNoWorkingDays']).' )</td></tr>';
    $out .= '</table>';
    $out .= '</div>';
    return $out;
}

/*
 * function import salaries salary
 *
 */

function importCStoSalary($info = true, $chid = null) {
    global $user, $conf;
    dol_include_once('/cliatm/config.php');
    dol_include_once('/comm/propal/class/propal.class.php');
    dol_include_once('/product/class/product.class.php');
    dol_include_once('/fourn/class/fournisseur.product.class.php');
    //dol_include_once('/compta/salaries/class/paymentsalary.class.php');

    $sql = 'SELECT rowid, libelle, amount,  periode
			FROM '.MAIN_DB_PREFIX.'chargesociales
			WHERE fk_type = 234';
    if(!empty($chid)) {
        $sql .= ' AND rowid = '.$chid;
    }
    else {
        $sql .= ' AND periode < "'.date('Y-m-d').'"';
    }
    $sql .= ' ORDER BY periode';

    $bid = 2; // Caisse épargne
    $mode_paiement = 2; // Virement

    $TPDOdb = new TPDOdb;
    $TPDOdb->Execute($sql);

    $TRes = $TPDOdb->Get_All();

    $conf->banque->enabled = 0;
    // parcours charge sociale
    foreach($TRes as $res) {
        _generate_salary($res, $bid, $mode_paiement, $info);
    }
    $conf->banque->enabled = 1;
}

/*
 * function import boss salary
 *
 */

function importBOSSCStoSalary($info = true, $chid = null) {
    global $user, $conf;
    dol_include_once('/cliatm/config.php');
    dol_include_once('/comm/propal/class/propal.class.php');
    dol_include_once('/product/class/product.class.php');
    dol_include_once('/fourn/class/fournisseur.product.class.php');
    //dol_include_once('/compta/salaries/class/paymentsalary.class.php');

    $sql = 'SELECT rowid, libelle, amount,  periode
			FROM '.MAIN_DB_PREFIX.'chargesociales
			WHERE fk_type IN (245,246,247)';
    if(!empty($chid)) {
        $sql .= ' AND rowid = '.$chid;
    }
    else {
        $sql .= ' AND periode < "'.date('Y-m-d').'"';
    }

    $bid = 2; // Caisse épargne
    $mode_paiement = 2; // Virement

    $TPDOdb = new TPDOdb;
    $TPDOdb->Execute($sql);

    $TRes = $TPDOdb->Get_All();

    $conf->banque->enabled = 0;
    // parcours charge sociale
    foreach($TRes as $res) {
        _generate_salary($res, $bid, $mode_paiement, $info);
    }
    $conf->banque->enabled = 1;
}

function _generate_salary($obj, $bid, $mode_paiement, $info) {
    global $db;

    $label_tronc = strtoupper($obj->libelle);

    // replace du mot clé indemnité
    $label_tronc = str_replace(strtoupper('indemnités '), '', $label_tronc);
    $label_tronc = str_replace(strtoupper('indemnités'), '', $label_tronc);
    $label_tronc = str_replace(strtoupper('indemnité '), '', $label_tronc);
    $label_tronc = str_replace(strtoupper('indemnité'), '', $label_tronc);

    // replace du mot clé salaire
    $label_tronc = str_replace('SALAIRES ', '', $label_tronc);
    $label_tronc = str_replace('SALAIRES', '', $label_tronc);
    $label_tronc = str_replace('SALAIRE ', '', $label_tronc);
    $label_tronc = str_replace('SALAIRE', '', $label_tronc);

    $label_tronc = str_replace(' / ', '/', $label_tronc);

    // Explode le / qui délimite prénom / nom
    $TBlocks = explode('/', $label_tronc);
    $prenom = $TBlocks[0];
    $nom = $TBlocks[1];


    $u = new User($db);
    $userid = _fetch_user_by_prenom($prenom, $nom);
    $u->fetch($userid);


    $res = _create_salary($obj, $u, $bid, $mode_paiement, $info);

    // Debug
    if(!empty($info)) flush();
}

function _create_salary($obj, $u, $bid, $mode_paiement, $info = null) {
    global $db, $user;
	dol_include_once('/salaries/class/paymentsalary.class.php');
    $res = 0;
    if(!empty($obj) && !empty($u->id) && !empty($bid) && !empty($mode_paiement)) {
        // Test si payment n'existe pas déjà
        $TPDOdb = new TPDOdb;

        $amount = GETPOST('amount_'.$obj->rowid);

        $sql = 'SELECT rowid
		FROM '.MAIN_DB_PREFIX.'payment_salary
		WHERE fk_user = '.$u->id.'
		AND datep = "'.$obj->periode.'"';


        $TPDOdb->Execute($sql);
        $TRes = $TPDOdb->Get_All();
        if(empty($TRes)) {
            $res = 1;
            // Pas déjà enregistré alors on enregistre
            $datetime = strtotime($obj->periode);
            $paymentSalary = new PaymentSalary($db);
            $paymentSalary->label = 'Règlement salaire '.$u->firstname.' '.$u->lastname;
			$paymentSalary->amount = (! empty($amount) ? $amount : $obj->amount);
            $paymentSalary->fk_bank = $bid;
            $paymentSalary->accountid = $bid;
            $paymentSalary->fk_user_author = $user->id;
            $paymentSalary->fk_user_modif = $user->id;
            $paymentSalary->fk_user = $u->id;
            $paymentSalary->datep = $obj->periode;
            $paymentSalary->datev = $obj->periode;
            $paymentSalary->datesp = date('Y-m-01', $datetime);
            $paymentSalary->dateep = date('Y-m-t', $datetime);
            $paymentSalary->type_payment = $mode_paiement;
            // Comment à cause du fait que ca duplique les mouvements de banque (Charges Sociales + SALAIRE)
            $res = $paymentSalary->create($user);
            if(!empty($info)) var_dump($obj);
        }
    } else {
        // Debug
        if(!empty($info)) {
            echo '<br/>ERREUR : <br/>';
            var_dump($obj);
            echo '<br/>-----------------------------------<br/>';
        }
    }

    return $res;
}

function _fetch_user_by_prenom($prenom, $nom) {
    global $db;

    $TPDOdb = new TPDOdb;
    $id = null;

    $sql = 'SELECT rowid
		FROM '.MAIN_DB_PREFIX.'user
		WHERE firstname LIKE "'.$prenom.'"
		AND lastname LIKE "'.$nom.'"';

    $TPDOdb->Execute($sql);
    $TRes = $TPDOdb->Get_All();
    if(!empty($TRes)) {
        $id = $TRes[0]->rowid;
    }

    return $id;
}

function setContactOnTask(&$project, $fk_type_contact_project) {
    if(empty($fk_type_contact_project)) return;

    $TTask = getTasksToLink($project, $fk_type_contact_project);

    $fk_type_contact_task = getTypeTaskId($fk_type_contact_project);
    foreach($TTask as $task) {
        $task->add_contact(GETPOST('userid', 'int'), $fk_type_contact_task, 'internal');
    }
}

/**
 * @param		object		$project
 * @param		int			$fk_type_contact_project
 * @param		boolean		$get_only_empty_progress
 * @return		array							Array of task which are linked to a product
 */
// Renvoie les tâches Dev ou Chef de projet par rapport à $type_contact
function getTasksToLink(&$project, $fk_type_contact_project, $get_only_empty_progress = true) {
    global $conf;

    $TTask = array();
    foreach($project->lines as &$task) {
        $task->fetchObjectLinked();
        if($task->progress == 0 || !$get_only_empty_progress) { // Utile si on veut également récupérer les tâches dont la progression n'est pas nulle (c.f. Onglet Rapport)
//			$Tab = explode($conf->global->DOC2PROJECT_TASK_REF_PREFIX, $task->ref);//		Si demain la conf évolue, on ne pourra plus retrouver l'identifiant de la ligne d'origine
//			$fk_propaldet_origin = $Tab[1];
            $TLinkedObjectsIds = $task->linkedObjectsIds;
            if(!empty($TLinkedObjectsIds)) {
                $sourceType = key($TLinkedObjectsIds);
                $fk_propaldet_origin = current($TLinkedObjectsIds[$sourceType]);
            }
            else {
                $fk_propaldet_origin = -1;
                $sourceType = 'propaldet';
            }

            if(checkCategoryProduct($fk_propaldet_origin, $fk_type_contact_project, $sourceType)) {
                $TTask[] = $task;
            }
        }
    }

    return $TTask;
}

/**
 * Ce charge de vérifier l'appartenance à la catégorie "Dev" (ou non appartenance si l'ajout concerne un CDP)
 *
 * @param type $fk_propaldet_origin
 * @param type $type_contact
 * @return boolean true || false
 */
function checkCategoryProduct($fk_propaldet_origin, $type_contact, $sourceType = 'propaldet') {
    global $db, $conf, $generic, $cliatm_code, $cliatm_category_dev;

    if(empty($conf->global->ATM_PRBOARD_CATEG_DEV)) return false;

    $cliatm_code = ''; // Si on commence par chercher les tâches CDP, la variable ne sera plus jamais vide même si on cherche par la suite les tâches DEV
    if(empty($cliatm_code)) {
        require_once DOL_DOCUMENT_ROOT.'/core/class/genericobject.class.php';
        require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';

        $generic = new GenericObject($db);
        $cliatm_code = $generic->getValueFrom('c_type_contact', $type_contact, 'code'); // PROJECTCONTRIBUTOR = dev

        $cliatm_category_dev = new Categorie($db);
        $cliatm_category_dev->fetch($conf->global->ATM_PRBOARD_CATEG_DEV); // TODO à adapter
    }

    $fk_product = getProductId($fk_propaldet_origin, $sourceType);

    // Si DEV
    if($cliatm_code == 'PROJECTCONTRIBUTOR') {
        // check categorie du produit car il doit etre dans "dev"
        if(!empty($fk_product) && $cliatm_category_dev->containsObject('product', $fk_product)) {
            return true;
        }
    }
    else { // CDP
        // check categorie produit != de "dev"
        if(empty($fk_product) || !$cliatm_category_dev->containsObject('product', $fk_product)) {
            return true;
        }
    }

    return false;
}

// Renvoi l'id du produit de la ligne identifiée par $fk_propaldet_origin
function getProductId($fk_propaldet_origin, $sourceType = 'propaldet') {
    global $db;

    if(empty($fk_propaldet_origin)) return 0;

    if($sourceType == 'propaldet') $line = new PropaleLigne($db);
    elseif($sourceType == 'orderline') $line = new OrderLine($db);

    $line->fetch($fk_propaldet_origin);

    return $line->fk_product;
}

/**
 * Renvoi le rowid du type de contact à utiliser pour l'association aux tâches
 *
 * @global type $db
 * @param type $fk_type_contact_project
 * @return type
 */
function getTypeTaskId($fk_type_contact_project) {
    global $db;

    $sqlGetCode = 'SELECT code';
    $sqlGetCode .= ' FROM '.MAIN_DB_PREFIX.'c_type_contact';
    $sqlGetCode .= ' WHERE rowid='.$fk_type_contact_project;

    $res = $db->query($sqlGetCode);
    $obj = $db->fetch_object($res);

    $sql = 'SELECT rowid';
    $sql .= ' FROM '.MAIN_DB_PREFIX.'c_type_contact';
    $sql .= ' WHERE source="internal"';
    if($obj->code == 'PROJECTCONTRIBUTOR') $sql .= ' AND code="TASKCONTRIBUTOR"';
    else $sql .= ' AND code="TASKEXECUTIVE"';

    $resql = $db->query($sql);
    $obj = $db->fetch_object($resql);

    return $obj->rowid;
}

/**
 * Affiche une liste des tâches de type $type (Dev ou CDP)
 * @param	Project $object
 * @param	String	$type	'dev' | 'cdp'
 * @return void
 */
function printTaskList(&$object, $type) {
    if($type != 'dev' && $type != 'cdp') return;

    global $langs, $conf;

    $nb_second_per_day = $conf->global->DOC2PROJECT_NB_HOURS_PER_DAY * 3600;

    if($type == 'dev') $title = $langs->trans("ListOfDevTasks");
    else if($type == 'cdp') $title = $langs->trans("ListOfCDPTasks");

    print load_fiche_titre($title, '', 'title_generic.png');

    print '<div class="div-table-responsive">';
    print '<table id="tablelines" class="noborder" width="100%">';

    print '<tr class="liste_titre nodrag nodrop">';
    print '<td>&nbsp;</td>';
    print '<td width="100">'.$langs->trans("RefTask").'</td>';
    print '<td>'.$langs->trans("LabelTask").'</td>';
    print '<td>'.$langs->trans("TaskDescription").'</td>';
    print '<td align="right">'.$langs->trans("PlannedWorkload").'</td>';
    print '<td align="right">'.$langs->trans("TimeSpent").'</td>';
    print '<td align="right">'.$langs->trans("TimeRemaining").'</td>';
    print '<td>&nbsp;</td>';
    print "</tr>\n";

    $TTasks = getTasks($object, $type);

    if(count($TTasks) > 0) {
        $total_planned_workload = $total_time_spent = $total_time_remaining = 0;
		for($i = 0 ; $i < count($TTasks) ; $i++) {
            if(in_array($TTasks[$i]->label, array('Tickets', 'Tickets internes'))) continue;

            print '<tr>';
            print '<td>&nbsp;</td>';

            // Ref.
            print '<td width="10%">';
            print $TTasks[$i]->getNomUrl(1, 'withproject');
            print '</td>';

            // Title
            print '<td width="10%">';
            print $TTasks[$i]->label;
            print '</td>';

            // Description
            print '<td width="50%">';
            print nl2br($TTasks[$i]->description);
            print '</td>';

            // Planned_workload
            $total_planned_workload += $TTasks[$i]->planned_workload;
            print '<td width="10%" align="right">';
            print convertSecondToTime($TTasks[$i]->planned_workload, 'all', $nb_second_per_day);
            print '</td>';

            // Time spent
            $total_time_spent += $TTasks[$i]->duration;
            print '<td width="10%" align="right">';
            print convertSecondToTime($TTasks[$i]->duration, 'all', $nb_second_per_day);
            print '</td>';

            // Time spent
            print '<td width="10%" align="right"';
            $time_remaining = ($TTasks[$i]->planned_workload - $TTasks[$i]->duration);
            $total_time_remaining += $time_remaining;

            if($time_remaining < 0) {
                $time_remaining = abs($time_remaining);
                print ' style="color: red; font-weight: bold;">';
                print '- ';
            }
            else {
                print '>';
            }
            print convertSecondToTime($time_remaining, 'all', $nb_second_per_day);
            print '</td>';

            print '<td>&nbsp;</td>';
            print '</tr>';
        }
        print '<tr>';

        print '<td class="liste_titre" align="right" colspan="4">'.$langs->trans('Total').' :</td>';
        print '<td class="liste_titre" align="right">'.convertSecondToTime($total_planned_workload, 'all', $nb_second_per_day).'</td>';
        print '<td class="liste_titre" align="right">'.convertSecondToTime($total_time_spent, 'all', $nb_second_per_day).'</td>';

        print '<td class="liste_titre" align="right"';
        if($total_time_remaining < 0) {
            $total_time_remaining = abs($total_time_remaining);
            print ' style="color: red;">';
            print '- ';
        }
        else {
            print '>';
        }
        print convertSecondToTime($total_time_remaining, 'all', $nb_second_per_day);
        print '</td>';

        print '<td>&nbsp;</td>';
        print '</tr>';
    }
    else {
        print '<tr class="oddeven"><td colspan="8" class="opacitymedium">'.$langs->trans("NoTasks").'</td></tr>';
    }

    print "</table>";
    print '</div>';
}

/**
 * Return all dev or cdp tasks even if progress is not empty
 * @param	String	$type	dev | cdp
 * @return Task	[]
 */
function getTasks(&$object, $task_type='dev') {
    global $db, $user;

    $TData = array();

    $sql = 'SELECT rowid';
    $sql .= ' FROM '.MAIN_DB_PREFIX.'c_type_contact';
    $sql .= ' WHERE source="internal"';
    if($task_type == 'dev') $sql .= ' AND code="PROJECTCONTRIBUTOR"';
    else if($task_type == 'cdp') $sql .= ' AND code="PROJECTLEADER"';
    else return;

    $resql = $db->query($sql);
    if($obj = $db->fetch_object($resql)) {
        $taskstatic = new Task($db);
        $object->lines = $taskstatic->getTasksArray(0, 0, $object->id, 0, 0);
        $TData = getTasksToLink($object, $obj->rowid, false);
    }

    return $TData;
}

/**
 * @param string $confkey   Name of an attribute in $conf->global
 * @param string $separator Value separator in the conf value (by default comma)
 * @param string $converter Callback for type validation (by default intval)
 * @return array
 */
function getConfArray($confkey, $separator = ',', $converter = 'intval') {
	global $conf;
	if ($conf->global->{$confkey}) {
		$confArray = explode($separator, $conf->global->{$confkey});
		return array_map($converter, $confArray);
	}
	return array();
}


/**
 * récuperée et modifiée de la version 13 dolibarr
 *
 * @param $prefix
 * @param string $modelType
 * @param int $default
 * @param int $addjscombo
 * @return string
 */
function selectModelMail($prefix, $modelType = '', $default = 0, $addjscombo = 0)
{
	global $langs, $db, $user,$conf;

	$retstring = '';

	$TModels = array();

	include_once DOL_DOCUMENT_ROOT.'/core/class/html.formmail.class.php';
	$formmail = new FormMail($db);
	$result = $formmail->fetchAllEMailTemplate($modelType, $user, $langs);

	if ($default) $TModels[0] = $langs->trans('DefaultMailModel');
	if ($result > 0) {
		foreach ($formmail->lines_model as $model) {
			$TModels[$model->id] = $model->label;
		}
	}

	$retstring .= '<select class="flat" id="select_'.$prefix.'model_mail" name="'.$prefix.'">';

	foreach ($TModels as $id_model=>$label_model) {
		$retstring .= '<option value="'. $id_model .'"';

		if ($conf->global->$prefix == $id_model){
			$retstring .= " selected >";
		}else{
			$retstring .= ">";
		}
		$retstring .= $label_model."</option>";
	}

	$retstring .= "</select>";

	if ($addjscombo) $retstring .= ajax_combobox('select_'.$prefix.'model_mail');

	return $retstring;
}
