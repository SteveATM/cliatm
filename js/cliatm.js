$( function() {
	
	$( "#openerPropalBoard" ).on( "click", function( event ) {
		$( "#propalBoard" ).dialog({
			resizable: false,
			height: 'auto',
			width: 'auto',
			modal: true,
			show: {
				effect: "blind",
				duration: 500
			},
			hide: {
				effect: "explode",
				duration: 500
			}
		});
		event.preventDefault();
	});
});