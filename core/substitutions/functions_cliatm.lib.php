<?php

/** 		Function called to complete substitution array (before generating on ODT, or a personalized email)
 * 		functions xxx_completesubstitutionarray are called by make_substitutions() if file
 * 		is inside directory htdocs/core/substitutions
 *
 * 		@param	array		$substitutionarray	Array with substitution key=>val
 * 		@param	Translate	$langs			Output langs
 * 		@param	Object		$object			Object to use to get values
 * 		@return	void					The entry parameter $substitutionarray is modified
 */
function cliatm_completesubstitutionarray(&$substitutionarray, $langs, $object) {
    global $conf, $db;

    if($object->element == 'societe' || !empty($object->thirdparty)) {
        if($object->element == 'societe') $soc = $object;
        else $soc = $object->thirdparty;

        $soc->fetch_thirdparty();

        $substitutionarray['company_logo'] = _get_logo($soc);

        $TContacts = $soc->contact_array_objects();

        $responsable = '';
        if(!empty($TContacts)) {
            $contact = $TContacts[0];

            switch($contact->civility_id) {
                case 'MR':
                    $responsable .= 'Monsieur ';
                    break;
                case 'MME':
                    $responsable .= 'Madame ';
                    break;
                case 'MTRE':
                    $responsable .= 'Maître ';
                    break;
            }

            $responsable .= $contact->firstname.' '.$contact->lastname;
        }
        else {
            $substitutionarray['company_logo'] = '';
            $responsable = 'Inconnu';
        }

        // 2019.03.25 MKO : je mets en commentaire car mettre une clé comme cela est dangereux
        // Il est arrivé que le mot "responsable" soit remplacé par le nom complet du 1er contact lié
        // Je ne vois pas où est-ce que nous avons besoin de cette substitution, à revoir
        //$substitutionarray['responsable'] = $responsable;
    }
}

/** 		Function called to complete substitution array for lines (before generating on ODT, or a personalized email)
 * 		functions xxx_completesubstitutionarray_lines are called by make_substitutions() if file
 * 		is inside directory htdocs/core/substitutions
 *
 * 		@param	array		$substitutionarray	Array with substitution key=>val
 * 		@param	Translate	$langs			Output langs
 * 		@param	Object		$object			Object to use to get values
 *              @param  Object          $line                   Current line being processed, use this object to get values
 * 		@return	void					The entry parameter $substitutionarray is modified
 */
function cliatm_completesubstitutionarray_lines(&$substitutionarray, $langs, $object, $line) {
    global $conf, $db;

    $substitutionarray['line_show'] = '1'; // Par défaut on affiche la ligne
    $substitutionarray['line_desc'] = (!empty($line->desc) ? $line->desc : $line->label);

    $substitutionarray['line_is_title'] = '0'; // Défini si la ligne est un titre

    if($line->product_type == 9 && $line->qty == 1) {
        $substitutionarray['line_is_title'] = '1';
    }
    else if($line->product_type != 9) { // Si ce n'est pas un titre
        if($line->fk_product > 0) {
            // On récupére le produit concerné
            $p = new Product($db);
            $p->fetch($line->fk_product);

            $ref = $line->ref;

            // On détermine la ligne actuelle
            $current_line = 0;
            foreach($object->lines as $i => $l) {
                if($l->rowid == $line->rowid) {
                    $current_line = $i;
                    break;
                }
            }

            // On regarde en arrière si un produit de la même ref était déjà présent
            $already = false;

            // On évite de comparer sur le même élement...
            $start_search = $current_line - 1;

            if($start_search >= 0) {
                for($i = $start_search; $i > 0; $i--) {
                    $l = $object->lines[$i];

                    if($l->product_type != 9) { // Si ce n'est pas un titre
                        if($l->ref == $ref) {
                            $already = true;
                            break;
                        }
                    }
                }
            }

            if(!$already) {
                switch($p->duration_unit) {
                    case 'd':
                        $substitutionarray['line_in_days'] = '1';
                        $substitutionarray['line_in_hours'] = '0';
                        break;
                    case 'h':
                        $substitutionarray['line_in_hours'] = '1';
                        $substitutionarray['line_in_days'] = '0';
                        break;
                }

                if($line->qty <= 0 || empty($p->rowid)) {
                    $substitutionarray['line_in_hours'] = '0';
                    $substitutionarray['line_in_days'] = '0';
                }

                if($current_line + 1 < count($object->lines)) {
                    for($i = $current_line + 1; $i < count($object->lines); $i++) {
                        $l = $object->lines[$i];

                        if($l->product_type != 9) {
                            if($l->ref == $ref) {
                                // Concaténation des descriptions pour afficher sous une seule catégorie
                                $substitutionarray['line_desc'] .= '<br /><br />'.(!empty($l->desc) ? $l->desc : $l->label);

                                // Somme des quantités pour affiché la durée totale dans le fichier généré
                                $substitutionarray['line_qty'] += $l->qty;
                            }
                        }
                    }
                }
            }
            else {
                $substitutionarray['line_show'] = '0';
            }
        }
    }
    else {
        $substitutionarray['line_show'] = '0';
    }
}

function _get_logo(&$object) {
    global $conf;

    $logo_path = '';
    $logo_dir = $conf->societe->dir_output.'/'.$object->client->id.'/logos/thumbs/';

    if(preg_match('/(.*)(\.[a-zA-Z]*)/', $object->client->logo, $matches)) {
        $filename = $matches[1];
        $extension = $matches[2];

        $logo_small = $filename.'_small'.$extension;
    }
    else if(!empty($object->logo)) {
        $logo_small = $filename.'_small'.$extension;
    }

    if(!empty($logo_small)) {
        $logo_path = $logo_dir.$logo_small;
    }

    return $logo_path;
}
