<?php
/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015 ATM Consulting <support@atm-consulting.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file		core/triggers/interface_99_modMyodule_CliATMtrigger.class.php
 * 	\ingroup	cliatm
 * 	\brief		Sample trigger
 * 	\remarks	You can create other triggers by copying this one
 * 				- File name should be either:
 * 					interface_99_modMymodule_Mytrigger.class.php
 * 					interface_99_all_Mytrigger.class.php
 * 				- The file must stay in core/triggers
 * 				- The class name must be InterfaceMytrigger
 * 				- The constructor method must be named InterfaceMytrigger
 * 				- The name property name must be Mytrigger
 */

/**
 * Trigger class
 */
class InterfaceCliATMtrigger
{

    private $db;

    /**
     * Constructor
     *
     * 	@param		DoliDB		$db		Database handler
     */
    public function __construct($db)
    {
        $this->db = $db;

        $this->name = preg_replace('/^Interface/i', '', get_class($this));
        $this->family = "demo";
        $this->description = "Triggers of this module are empty functions."
            . "They have no effect."
            . "They are provided for tutorial purpose only.";
        // 'development', 'experimental', 'dolibarr' or version
        $this->version = 'development';
        $this->picto = 'cliatm@cliatm';
    }

    /**
     * Trigger name
     *
     * 	@return		string	Name of trigger file
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Trigger description
     *
     * 	@return		string	Description of trigger file
     */
    public function getDesc()
    {
        return $this->description;
    }

    /**
     * Trigger version
     *
     * 	@return		string	Version of trigger file
     */
    public function getVersion()
    {
        global $langs;
        $langs->load("admin");

        if ($this->version == 'development') {
            return $langs->trans("Development");
        } elseif ($this->version == 'experimental')

                return $langs->trans("Experimental");
        elseif ($this->version == 'dolibarr') return DOL_VERSION;
        elseif ($this->version) return $this->version;
        else {
            return $langs->trans("Unknown");
        }
    }

    /**
     * Function called when a Dolibarrr business event is done.
     * All functions "run_trigger" are triggered if file
     * is inside directory core/triggers
     *
     * 	@param		string		$action		Event action code
     * 	@param		Object		$object		Object
     * 	@param		User		$user		Object user
     * 	@param		Translate	$langs		Object langs
     * 	@param		conf		$conf		Object conf
     * 	@return		int						<0 if KO, 0 if no triggered ran, >0 if OK
     */
    public function run_trigger($action, $object, $user, $langs, $conf)
    {
    	global $mysoc;
        // Put here code you want to execute when a Dolibarr business events occurs.
        // Data and type of action are stored into $object and $action
		if(! defined('INC_FROM_DOLIBARR')) define('INC_FROM_DOLIBARR', true);
		require_once dol_buildpath('/cliatm/config.php');

		if ($action == 'COMPANY_CREATE') {
    		dol_syslog(
    			"Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id
    		);

    		// Status prospect
    		if ($object->client == 2 || $object->client == 3) {

				$object->cond_reglement_id = 2;
				$object->mode_reglement_id = 2;
				$object->fk_prospectlevel = 'PL_HIGH';
				$object->stcomm_id = dol_getIdFromCode($object->db, 'ST_PEND', 'c_stcomm');

				$result=$object->update($object->id, $user);

				if ($result < 0) {
					setEventMessages($object->error, $object->errors, 'errors');
				} else {
					return 1;
				}
			}

    	} elseif ($action == 'PROPAL_CREATE') {
    		dol_syslog(
    				"Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id
    				);

    		// Vidage des champs complémentaires si clone et du projet
			if(isset($object->context['createfromclone'])) {
				$object->setProject(0);
				$object->array_options['options_date_signature'] = '';
				$object->array_options['options_proba'] = '5';
				$object->array_options['options_date_cloture_prev'] = '';
				$object->array_options['options_date_liv_prev'] = '';
				$object->insertExtrafields();
			}

			// Association du commercial du tiers à la propale
			$listsalesrepresentatives=$object->thirdparty->getSalesRepresentatives($user);
			if(!empty($listsalesrepresentatives)) {
				$object->add_contact($listsalesrepresentatives[0]['id'], 'SALESREPFOLL', 'internal');
			} else {
				$this->error = $langs->trans("NoSalesRepresentativeAffected");
				return -1;
			}

    	} elseif ($action == 'PROPAL_VALIDATE') {
            dol_syslog(
                "Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id
            );

			// Si propale de moins de 500 € HT, condition de règlement = A réception
			if($object->total_ht <= 500) {
				$object->setPaymentTerms(1);
			}

			dol_include_once('/core/class/extrafields.class.php');
			$extrafields = new ExtraFields($this->db);
			$extrafields->fetch_name_optionals_label($object->table_element);

			if(empty($object->array_options)) $object->fetch_optionals();

			foreach($extrafields->attribute_required as $key => $required) {
				if(! empty($required) && empty($object->array_options['options_'.$key])) {
					setEventMessage($langs->trans('CantValidateWithEmptyRequiredFields'), 'errors');
					return -1;	// Bloque la validation de la propale si un champ requis est vide
				}
			}

		} elseif ($action == 'PROPAL_CLOSE_SIGNED') {
			/**
			 * @var Propal $object
			 */
			require_once DOL_DOCUMENT_ROOT . '/commande/class/commande.class.php';
			dol_include_once('/cliatm/lib/cliatm.lib.php');
			dol_include_once('/cliatm/class/cliatm.class.php');
			dol_include_once('/doc2project/class/doc2project.class.php');

			// Vérification de la présence d’un commercial affecté au tiers
			global $user;
			if (empty($object->thirdparty->getSalesRepresentatives($user, 1))) {
				$this->errors[] = $langs->trans('ATMPropalSignRequiresSalesperson');
				return -1;
			}

			// Vérification des rôles de contacts obligatoires sur le tiers pour pouvoir mettre une proposition
			// en signée
			$aMandatoryRoles = getConfArray('CLIATM_CONTACT_CODES_REQUIRED_FOR_PROPOSAL_SIGNATURE');

			$aFoundRoles = []; // rôles trouvés sur les contacts du tiers
			$aContact = $object->thirdparty->contact_array_objects();

			foreach ($aContact as $contact) {
				if ($contact->statut == 0) continue;
				$contact->fetchRoles();
				foreach ($contact->roles as $role) {
					$aFoundRoles[] = $role['id'];
				}
			}

			if ($aMissingRoles = array_diff($aMandatoryRoles, $aFoundRoles)) {
				include_once DOL_DOCUMENT_ROOT . 'contact/class/contact.class.php';
				$contactStatic = new Contact($this->db);
				$contactTypes = $contactStatic->listeTypeContacts('external', '', 1);
				$contactTypes = array_filter(
					$contactTypes,
					function ($rowid) use ($aMissingRoles) {
						return in_array($rowid, $aMissingRoles);
					}, ARRAY_FILTER_USE_KEY
				);
				$contactTypes = implode('</li><li>', $contactTypes);
				$this->errors[] = $langs->trans('ATMPropalSignRequiresContact', '<ul><li>' . $contactTypes . '</li></ul>');
				return -1;
			}


            if(! class_exists('TStory')) dol_include_once('/scrumboard/class/scrumboard.class.php');

            global $conf;


			$TCommande = array();
			$PDOdb = new TPDOdb;
			$orderType = new TOrderType;

			$TOrderType = $orderType->LoadAllBy($PDOdb);
			foreach($TOrderType as $type) {
				$TPropalLine = $type->getLinesFromCode($object->lines);

				foreach ($TPropalLine as $line) {
					$commande = new Commande($db);
					$commande = $type->getCommande($object, $user);
					$res = $commande->addline(	$line->desc,
										$line->subprice,
										$line->qty,
										$line->tva_tx,
										$line->localtax1_tx,
										$line->localtax2_tx,
										$line->fk_product,
										$line->remise_percent,
										$line->info_bits,
										$line->fk_remise_except,
										'HT',
										0,
										$line->date_start,
										$line->date_end,
										$line->product_type,
										$line->rang,
										$line->special_code,
										$line->fk_parent_line,
										0,
										$line->pa_ht,
										$line->label,
										$line->array_options,
										$line->fk_unit,
										$line->origin,
										$line->origin_id,
										$line->multicurrency_subprice);

					$commande->fetch_lines();
					if (!isset($TCommande[$commande->id])) {
						$TCommande[$commande->id] = $commande;
						$commande->add_object_linked('propal', $object->id);
					}
				}
			}

            $story = new TStory;
            $storyAlreadyExists = false;
            $TStory = array();
            $storyLabel = $object->ref;
			$project = null;
			foreach ($TCommande as &$commande) {
				if (!empty($project->id)) $commande->setProject($project->id);

				$project = Doc2Project::createProject($commande);

				if(! empty($project)) {
					$end = '';

                    if(! empty($conf->global->DOC2PROJECT_USE_SPECIFIC_STORY_TO_CREATE_TASKS) && empty($TStory)) {
                        $TStory = $story->getAllStoriesFromProject($project->id);

                        foreach($TStory as $sprint) {
                            if($sprint->label == $storyLabel) {
                                $storyAlreadyExists = true;
                                break;
                            }
                        }

                        // Création du sprint Ref propal
                        if(! $storyAlreadyExists) {
                            $newStory = new TStory;
                            $newStory->fk_projet = $project->id;
                            $newStory->storie_order = count($TStory)+1;
                            $newStory->label = $storyLabel;

                            $newStory->save($PDOdb);
                        }
                    }

					// Création des tâches pour le projet en vérifiant les exclusions de la conf DOC2PROJECT_EXCLUDED_PRODUCTS
					Doc2Project::parseLines($commande, $project, $project->date_start, $end, $storyLabel);
				}
			}

			if(! empty($project->id)) {
				// Ajout des liens entre le projet et la propale
				$object->setProject($project->id);
			}

		} elseif ($action == 'ORDER_VALIDATE') {
            dol_syslog(
                "Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id
            );
			global $db;

			dol_include_once('/cliatm/lib/cliatm.lib.php');
			dol_include_once('/cliatm/class/cliatm.class.php');
			require_once DOL_DOCUMENT_ROOT . '/contrat/class/contrat.class.php';


			$object->fetch_optionals();

			if(! empty($object->array_options['options_order_type'])) {
				$PDOdb = new TPDOdb;
				$orderType = new TOrderType;
				$orderType->load($PDOdb, $object->array_options['options_order_type']);

				if($orderType->code == 'projet')
				{
					/*
					 * TK1609-0158 :
					 * Sur clôture signé d'un devis
					 * Cherche s'il n'y a pas déjà des factures
					 * Si client no_deposit = Oui (extrafield sur fiche client, à créer)
					 * alors
					 * 	Création d’un acompte 30%, extrafield facture bon pour envoi Oui mai on laisse l'acompte en brouillon
					 */
					$res = $object->fetchObjectLinked();
					$nbFacture = count($object->linkedObjectsIds['facture']);
					if( $nbFacture === 0 && $object->thirdparty->array_options['options_no_deposit'] !== '1' && $object->total_ht > 500) {
						require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php';
						$facture = new Facture($this->db);

						// Même calcul que pour le pdf : /cliatm/core/modules/propale/doc/pdf_azur_ATM.modules.php
						$percent_acompte = $conf->global->MAIN_DEPOSIT_PERCENTAGE_INITIAL;

						$amountdeposit = $object->total_ttc * $percent_acompte / 100;
						if(!empty($conf->global->MAIN_DEPOSIT_ROUND_AMOUNT)) {
							$amountdeposit = round($amountdeposit, -$conf->global->MAIN_DEPOSIT_ROUND_AMOUNT);
						}


						$dateinvoice = dol_now();
						$facture->socid				= $object->thirdparty->id;
						$facture->type				= Facture::TYPE_DEPOSIT;

						$facture->date				= $dateinvoice;
						$facture->note_public		= '';
						$facture->note_private		= '';
						$facture->ref_client		= '';
						$facture->ref_int			= '';
						$facture->modelpdf			= 'crabe_ATM';
						$facture->fk_project		= $object->fk_project;
						$facture->cond_reglement_id	= 1; // Acompte = A RECEPTION
						$facture->mode_reglement_id	= $object->mode_reglement_id;
						//$facture->fk_account      = GETPOST('fk_account', 'int');
						$facture->amount				= $amountdeposit;
						$facture->remise_absolue		= 0;
						$facture->remise_percent		= 0;
						$facture->fk_incoterms 		= $object->incoterm_id;
						$facture->location_incoterms = $object->location_incoterms;
						$facture->multicurrency_code = $object->multicurrency_code;
						$facture->multicurrency_tx = $object->originmulticurrency_tx;

						$facture->origin = 'commande';
						$facture->origin_id = $object->id;

						$facture->array_options['options_ok_envoi'] = 1;

						// Possibility to add external linked objects with hooks
						$facture->linked_objects[$object->origin] = $object->origin_id;

						$idFactureAcompte = $facture->create($user);
						$tvatx = get_default_tva($mysoc, $object->thirdparty);
						$amountdepositHt = price2num($amountdeposit - ( $amountdeposit * $tvatx / 100), 2);
						$propalOrigin = current($object->linkedObjects['propal']);

						if($idFactureAcompte > 0) {
							$result = $facture->addline(
								$langs->trans('Deposit'). ' - ref '.$propalOrigin->ref.' ('. $percent_acompte .'% de '.price($object->total_ttc).' '.$object->multicurrency_code.')',
								$amountdepositHt,		 	// subprice
								1, 						// quantity
								$tvatx, 				// VAT rate
								0,						// localtax1_tx
								0, 						// localtax2_tx
								0, 						// fk_product
								0, 						// remise_percent
								0, 						// date_start
								0, 						// date_end
								0,						// Ventil
								$lines[$i]->info_bits, // info_bits
								0, 						// fk_remise_except
								'TTC',					// Price base type
								$amountdeposit,			// TTC price
								1, 						// product_type
								1,						// rang
								$lines[$i]->special_code,
								$object->origin,
								$object->id,
								0,
								0,
								0,
								$langs->trans('Deposit')
							);
						}
					}
				}
				else if($orderType->code == 'abonnement') {
					// Créer un contrat
					$contrat = new Contrat($db);

					$contrat->fk_project = $object->fk_project;
					$contrat->socid = $object->socid;
					$contrat->commercial_signature_id = $user->id;
					$contrat->commercial_suivi_id = $user->id;
					$contrat->date_contrat = $object->date;

					if($contrat->create($user) > 0) {
						setEventMessage('ContractCreated');
						$contrat->add_object_linked('commande', $object->id);

						$TLines = $orderType->getLinesFromCode($object->lines);
						foreach($TLines as &$line) {
							$contrat->addline($line->desc, $line->subprice, $line->qty, $line->tva_tx, $line->localtax1_tx, $line->localtax2_tx, $line->fk_product, $line->remise_percent, $line->date_start, $line->date_end);
						}
					}
				}
			}
			else {}

		}else if($action == 'PAYMENTSOCIALECONTRIBUTION_CREATE'){
			if($object->element == 'paiementcharge') {

				dol_include_once('/cliatm/lib/cliatm.lib.php');

				importCStoSalary(false, $object->chid);
				importBOSSCStoSalary(false, $object->chid);
			}
        } else if ($action == 'CONTACT_CREATE') {
			/** @var Contact $object */

			// Add roles suivi propal + facturation + signataire contrat on the first contact created on a thirparty
			if(empty($object->socid)) return 0;

			// Req compte nb contact
			$sql = "SELECT count(*) as total";
			$sql .= " FROM ".MAIN_DB_PREFIX."socpeople as p";
			$sql .= " WHERE p.fk_soc = ".$object->socid;
			$resql = $object->db->query($sql);
			$obj = $object->db->fetch_object($resql);

			$totalContact = $obj->total;
			if($totalContact == 1) { // The first contact has just been created
				include_once dirname(dirname(__DIR__)) . '/lib/cliatm.lib.php';
				// on affecte automatiquement à ce contact les rôles requis pour la signature de propositions
				$object->roles = array_unique(array_merge(
					$object->roles,
					getConfArray('CLIATM_CONTACT_CODES_REQUIRED_FOR_PROPOSAL_SIGNATURE')
				));
				$object->updateRoles();
			}

        	// Add roles suivi_propal + suivi_facture on the first contact created on a thirparty
        	if(empty($object->socid)) return 0;

        	// Req compte nb contact
        	$sql = "SELECT count(*) as total";
        	$sql .= " FROM ".MAIN_DB_PREFIX."socpeople as p";
        	$sql .= " WHERE p.fk_soc = ".$object->socid;
        	$resql= $object->db->query($sql);
            $obj = $object->db->fetch_object($resql);

        	$totalContact = $obj->total;
        	if($totalContact == 1) { // The first contact has just been created
				$object->roles[] = 60; // Suivi facture
				$object->roles[] = 41; // Suivi propale

				$object->updateRoles();
        	}
        }
		elseif ($action == 'PROJECT_ADD_CONTACT') {
            if (empty($conf->global->ATM_PRBOARD_CATEG_DEV))
            {
                return 0;
            }

            $source = GETPOST('source', 'alpha');

            if ($source != 'internal')
            {
                return 0;
            }

            $userID = GETPOST('userid', 'int');

            if ($userID <= 0)
            {
                return 0;
            }

            $fk_c_type_contact = GETPOST('type', 'int');

            // PROJECTLEADER => CdP, PROJECTCONTRIBUTOR => Dev
            $projectContactTypeCode = $object->getValueFrom('c_type_contact', $fk_c_type_contact, 'code');

            if (! in_array($projectContactTypeCode, array('PROJECTLEADER', 'PROJECTCONTRIBUTOR')))
            {
                return 0;
            }


			require_once DOL_DOCUMENT_ROOT . '/categories/class/categorie.class.php';
			require_once DOL_DOCUMENT_ROOT . '/comm/propal/class/propal.class.php';
			require_once DOL_DOCUMENT_ROOT . '/commande/class/commande.class.php';

            $staticCategDev = new Categorie($object->db);
            $staticCategDev->id = intval($conf->global->ATM_PRBOARD_CATEG_DEV);


            $object->getLinesArray($user);

            // H4cK 4N0nYm0u$-style : nécessaire pour récuper les liens avec les lignes de propal/commande dans CommonObject::fetchObjectLinked()...
            $conf->orderline = new stdClass();
            $conf->propaldet = new stdClass();

            $conf->orderline->enabled = 1;
            $conf->propaldet->enabled = 1;

            foreach ($object->lines as &$task)
            {
                if (in_array($userID, $task->liste_contact(-1, 'internal', 1)))
                {
                    continue;
                }

                $task->fetchObjectLinked();

                if (empty($task->linkedObjectsIds))
                {
                    continue;
                }

                $fk_product = 0;

                foreach ($task->linkedObjectsIds as $type => $TIDs)
                {
                    if (empty($TIDs))
                    {
                        continue;
                    }

                    foreach ($TIDs as $id)
                    {
                        switch ($type)
                        {
                            case 'propaldet':
                                $fk_product = $object->getValueFrom('propaldet', $id, 'fk_product');
                                break;

                            case 'orderline':
                                $fk_product = $object->getValueFrom('commandedet', $id, 'fk_product');
                                break;
                        }

                        if (! empty($fk_product))
                        {
                            break 2; // On a trouvé une correspondance, on sort de la boucle parente
                        }
                    }
                }

                if (empty($fk_product))
                {
                    continue;
                }

                $mustAddContact = false;

                if ($projectContactTypeCode == 'PROJECTCONTRIBUTOR'
                    && $staticCategDev->containsObject('product', $fk_product) > 0
                ) {
                    $mustAddContact = true;
                }

                if (
                    $projectContactTypeCode != 'PROJECTCONTRIBUTOR'
                    && $staticCategDev->containsObject('product', $fk_product) == 0
                ) {
                    $mustAddContact = true;
                }

                if ($mustAddContact)
                {
                    $task->add_contact($userID, 'TASKEXECUTIVE', 'internal');
                }
            }
		}
		elseif ($action == 'BILL_CREATE') {
		    // Link between facture modèle et facture générée
            if(! empty($object->fac_rec)) {
                $object->add_object_linked('facturerec', $object->fac_rec);
            }
		}
		elseif($action == 'LINEBILL_INSERT') {
			if(! TSubtotal::isModSubtotalLine($object)) {
				// Spé ATM : Pas de produit chez ATM
				$object->product_type = 1;
				$object->update($user);
			}
		}
		elseif ($action == 'TASK_MODIFY' || $action == 'TASK_TIMESPENT_CREATE')
		{
			define('INC_FROM_DOLIBARR', true);
			dol_include_once('/cliatm/config.php');
			dol_include_once('/cliatm/class/cliatm.class.php');

			$PDOdb = new TPDOdb();

			$sql = 'SELECT progress FROM ' . MAIN_DB_PREFIX . $object->table_element . ' WHERE rowid = ' . $object->id;

			$TRes = $PDOdb->ExecuteAsArray($sql);

			if(! empty($TRes))
			{
				$oldprogress = intval($TRes[0]->progress);

				if ($oldprogress != $object->progress) {
					$progressLog = new TaskProgressLog($this->db);
					$progressLog->fk_task = $object->id;
					$progressLog->oldprogress = $oldprogress;
					$progressLog->newprogress = intval($object->progress);

					$progressLog->create($user);
				}
			}

			$PDOdb->close();
		}
		elseif ($action == 'PROJECT_CREATE')
		{
			include_once DOL_DOCUMENT_ROOT . '/projet/class/task.class.php';

			// Création de 2 tâches automatiques pour les temps passés sur Dolibarr communautaire et sur les modules ATM
			$task = new Task($this->db);
			$task->ref = 'T' . $object->id . 'COMMU';
			$task->label = $langs->trans('TaskAutoTCOMMULabel');
			$task->progress = 100;
			$task->fk_task_parent = 0;
			$task->fk_project = $object->id;
			$task->create($user);

			$task->ref = 'T' . $object->id . 'MODULE';
			$task->label = $langs->trans('TaskAutoTMODULELabel');
			$task->progress = 100;
			$task->fk_task_parent = 0;
			$task->fk_project = $object->id;
			$task->create($user);
		}
		elseif ($action == 'OBSERVERINSTANCEAPICALL_SERVERMANAGER_V1_UPDATEINFO')
		{
			// Updates many specific fields on instance linked to ATM use
			$object->webInstance->array_options['options_bdd'] = $object->data->db->name;
			$object->webInstance->url_admin = $object->data->dolibarr->path->http;
			$object->webInstance->consumed_disk_space = shorthandFileSize2Bytes($object->data->dolibarr->data->size);
			$object->webInstance->array_options['options_folder_size'] = bytes2ShorthandFileSize($object->data->dolibarr->repertoire_client->size);
			$object->webInstance->array_options['options_security_database_pwd_encrypted'] = $object->data->security->database_pwd_encrypted;
			$object->webInstance->array_options['options_security_main_features_level'] = $object->data->security->main_features_level;
			$object->webInstance->array_options['options_security_install_lock'] = $object->data->security->install_lock;
		}
        elseif($action == 'TICKET_CREATE') {
            /** @var Ticket $object */
            global $context;

            if(! is_null($context) && get_class($context) === 'Context') { // On est dans le portail client
                if(empty($object->fk_project)) {
                    $client = new Societe($this->db);
                    $res = $client->fetch($object->fk_soc);    // Dans le cas d'une ouverture depuis le portail, le fk_soc est toujours set

                    if($res > 0 && ! empty($client->array_options) && ! empty($client->array_options['options_pj_assistance'])) {
                        $object->fk_project = $client->array_options['options_pj_assistance'];
                        $object->update();
                    }
                }
            }
        }

        return 0;
    }
}
