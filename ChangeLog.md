# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]



## Version 1.15 - *07/07/2021*

- FIX : Missing directory name in tech.atm clone-modules-in-pack *15/09/2021* - 1.15.4
- FIX : Dolistore template for FR *30/08/2021* - 1.15.3
- FIX : Fix contract list  *27/07/2021* - 1.15.2
- FIX : Fix contact not loaded before compare  *07/07/2021* - 1.15.1
- NEW : Update about page template *06/07/2021* - 1.15.0
- NEW : Portail client ticket ajoute un bandeau compte temps N-1 et N par hook. *01/07/2021* - 1.14.0  
  need portail client V 1.17.0 mini
- NEW : Système centralisé de données techniques *16/04/2021* - 1.13.0
    - Ajout du dossier www-public pour création de virtual host apache
    - Ajout de la génération de page about sur www-public
    - Ajout de la gestion de maj de la page about basé sur www-public
    - Ajout de la récupération de la dernière version stable du module

## Version 1.12

- NEW : Tâche CRON Traitement des commandes brouillon type "Projet" *14/04/2021* - 1.12.0 
  - validation de la commande
  - validation de la facture d’acompte créée par le trigger ORDER_VALIDATE
  - génération du PDF de la facture
  - envoi du PDF par e-mail au(x) contact(s) "BILLING" avec CC au commercial du tiers (modèle d’e-mail
    configurable). On ne traite que les commandes datant de plus de 48h. L’utilisateur effectuant les actions est
    "administration".
- NEW : Tâche CRON Traitement des commandes brouillon type "Abonnement" *14/04/2021* - 1.12.0
  - validation de la commande
  - validation du contrat créé par le trigger ORDER_VALIDATE
  - génération du PDF du contrat (modèle DocEdit)
  - envoi du PDF par e-mail au(x) contact(s) "SALESREPSIGN" avec CC au commercial du tiers (modèle d’e-mail
    configurable). On ne traite que les commandes datant de plus de 48h. L’utilisateur effectuant les actions est
    "administration".

## Version 1.11

- FIX : Correction orthographique *06/05/2021* - 1.11.1
- NEW : Empêcher la signature d'une propale si le tiers n'a pas de commercial ou s'il manque un des 3 types de contacts
  (avec adresses e-mail renseignées) : suivi propale, signature contrat, facturation *09/04/2021* - 1.11.0


## Version 1.10

- NEW : Affichage des informations client assistance sur un ticket *11/04/2021* - 1.10.0

## Version 1.9

- NEW : Ajout de la saisie de temps rapide sur un ticket *06/04/2021* - 1.9.0 

## Version 1.8

- NEW : Ajout d'un paramètre admin permettant de choisir le type de contact à utiliser pour l'envoi de mail de
  maintenance *26/03/2021* - 1.8.0
- NEW : Dolistore card module generator *07/01/2021*

## Version 1.2
