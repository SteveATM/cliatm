<?php

if (! class_exists('TObjetStd') || ! class_exists('SeedObject'))
{
	/**
	 * Needed if $form->showLinkedObjectBlock() is call
	 */
	define('INC_FROM_DOLIBARR', true);
	require_once dirname(__FILE__).'/../config.php';
}

class TOrderType extends TObjetStd
{
	public static $tablename = 'c_order_type';
	public $TOrderType = array();
	
	public function __construct()
	{
		global $conf;
		
		$this->set_table(MAIN_DB_PREFIX.self::$tablename);
		
		$this->add_champs('label', array('type' => 'string', 'length' => 100, 'index' => true));
		$this->add_champs('code', array('type' => 'string', 'length' => 100, 'index' => true));
		$this->add_champs('active', array('type' => 'integer', 'default' => 1));
		$this->add_champs('entity', array('type' => 'integer', 'index' => true, 'default' => 1));
		
		$this->_init_vars();
		$this->start();
		
		$this->entity = $conf->entity;
		$this->active = 1;
	}
	
	public function LoadAllBy(&$db, $TConditions = array(), $annexe = true)
	{
		 $this->TOrderType = parent::LoadAllBy($db, $TConditions, $annexe);
		 
		 return $this->TOrderType;
	}
	
	public function getCommande(&$objectSrc, &$user)
	{
		global $atmCommande, $db;

		if ($this->code == 'formation' || empty($atmCommande) || $atmCommande->array_options['options_order_type'] != $this->id)
		{
			$atmCommande = new Commande($db);
			$atmCommande->socid = $objectSrc->socid;
			$atmCommande->array_options['options_order_type'] = $this->id;
			$atmCommande->fk_project = $objectSrc->fk_project;
			$atmCommande->date = dol_now();
			$atmCommande->cond_reglement_id = $objectSrc->cond_reglement_id;
			$atmCommande->mode_reglement_id = $objectSrc->mode_reglement_id;

			if ($atmCommande->create($user) < 0) dol_print_error($atmCommande->db);
		}

		return $atmCommande;
	}
	
	/**
	 * Récupère les lignes dont les produits/services appartiennent aux catégories identifiées par l'attribut $this->code
	 * @param array		$TLines	Object lines
	 * @return array
	 */
	public function getLinesFromCode(&$TLines) {
		global $db, $conf;

		dol_include_once('/categories/class/categorie.class.php');

		$categorie = new Categorie($db);
		$TConfValue = explode(',', $conf->global->{'MAIN_ORDER_CATEG_'.strtoupper($this->code)});

		$TData = array();
		foreach($TConfValue as $confValue) {
			$categorie->fetch($confValue);

			foreach($TLines as &$line) {
				if(! empty($line->fk_product) && empty($TData[$line->id]) && $categorie->containsObject('product', $line->fk_product) && $line->qty > 0) {
					$TData[$line->id] = $line;
				}
			}
		}

		return $TData;
	}
}


class TaskProgressLog extends SeedObject
{
	public $element = 'projet_task_progress_log';
	public $table_element = 'projet_task_progress_log';
	public $fields = array(
		'fk_task' => array('type' => 'varchar(50)', 'label' => 'Name', 'enabled' => 1, 'visible' => 1, 'notnull' => 1, 'index' => 1, 'position' => 10),
		'oldprogress' => array('type' => 'int', 'label' => 'OldProgress', 'enabled' => 1, 'visible' => 1, 'notnull' => 1, 'position' => 20),
		'newprogress' => array('type' => 'int', 'label' => 'NewProgress', 'enabled' => 1, 'visible' => 1, 'notnull' => 1, 'position' => 30)
	);

	public $fk_task;
	public $oldprogress;
	public $newprogress;
}

