<?php


class WebModuleStoreUpdate
{
	/**
	 * @var DoliDB $db
	 */
	public $db;

	/**
	 * @var string $output used for cron task
	 */
	public $output;

	/**
	 * @var string[] $errors
	 */
	public $errors = array();

	/**
	 * @var string[] $logs
	 */
	public $logs = array();

	/**
	 * @var string $ftp_server  Le domaine FTP de blackmirror
	 */
	protected $ftp_server;

	/**
	 * @var string $ftp_user_name Le login FTP de blackmirror
	 */
	protected $ftp_user_name;

	/**
	 * @var string $ftp_user_pass Le password FTP de blackmirror
	 */
	protected $ftp_user_pass;

	/**
	 * @var string $ftp_user_pass Le port FTP de blackmirror
	 */
	protected $ftp_server_port;

	/**
	 * @var string $ftp_path Le dossier relatif au dossier par defaut du FTP de blackmirror où se situe les modules
	 */
	public $ftp_path;


	/**
	 * @var resource|false $ftp_conn_id  a FTP stream on success or <b>FALSE</b> on error.
	 */
	public $ftp_conn_id;

	/**
	 * @var string $ftp_protocol  the FTP protocol to use.
	 */
	public $ftp_protocol;

	/**
	 * @var string $originalPath default path on login
	 */
	public $originalPath;

	/**
	 * @var string $currentPath the current path in FTP
	 */
	public $currentPath;

	/**
	 * @var Translate $langs
	 */
	public $langs;

	public function __construct()
	{
		global $conf, $langs, $db;

		$this->db = $db;

		$this->langs = $langs;
		$this->langs->load('cliatm@cliatm');

		//DEFINITION DES VARIABLE DE CONNEXION
		$this->ftp_server = $conf->global->WEBMOD_BLACKMIRROR_FTP_DOMAIN ;
		$this->ftp_user_name = $conf->global->WEBMOD_BLACKMIRROR_FTP_LOGIN ;
		$this->ftp_user_pass = $conf->global->WEBMOD_BLACKMIRROR_FTP_PASSWORD ;
		$this->ftp_server_port = $conf->global->WEBMOD_BLACKMIRROR_FTP_PORT ;

		$TProtocol = WebModuleStoreUpdate::availableFtpProtocols();
		if(in_array($conf->global->WEBMOD_BLACKMIRROR_FTP_PROTOCOL, $TProtocol)){
			$this->ftp_protocol = $conf->global->WEBMOD_BLACKMIRROR_FTP_PROTOCOL;
		}
		else{
			$this->ftp_protocol = 'ftp';
		}

		$this->ftp_path = $conf->global->WEBMOD_BLACKMIRROR_FTP_PATH;
		$this->ftp_path = rtrim($this->ftp_path, '/');
		$this->ftp_path = ltrim($this->ftp_path, '/');

		if(empty($this->ftp_server_port)){
			if($this->ftp_protocol == 'sftp'){
				$this->ftp_server_port = 22;
			}
			else{
				$this->ftp_server_port = 21;
			}
		}
	}

	/**
	 * Le destructeur de la class ferme automatiquement la connexion FTP
	 */
	function __destruct() {
		if(!empty($this->ftp_conn_id)) $this->ftpClose();
	}

	/**
	 * Permet gérer les retour d'erreur avec message
	 *
	 * @param string $err
	 */
	public function setError($err){
		if(!empty($err)){
			$this->error = $err;
			$this->errors[] = $this->error;
		}
	}

	/**
	 * Permet gérer les logs avec message
	 *
	 * @param string $log
	 */
	public function setLog($log){
		if(!empty($log)){
			$this->log = $log;
			$this->logs[] = $this->log;
		}
	}

	/**
	 * @return string[]
	 */
	public static function availableFtpProtocols(){
		$TProtocols = array('ftp', 'sslftp');

		if(function_exists('ssh2_connect')){
			// if ssh2_connect missing, install libssh2-php
			$TProtocols[] = 'sftp';
		}

		return $TProtocols;
	}

	/**
	 * Connect to FTP with connexion cache
	 *
	 * @param int $forceReconnect
	 * @return bool
	 */
	function ftp_connect($forceReconnect = 0, $timeout = 10){
		global $conf;

		if(empty($this->ftp_conn_id) || $forceReconnect){

			//CONNEXION FTP AVEC PHP
			if ($this->ftp_protocol == 'sslftp'){
				$this->ftp_conn_id = ftp_ssl_connect($this->ftp_server,$this->ftp_server_port, $timeout);
			}
			else if ($this->ftp_protocol == 'sftp'){
				$this->sshConnection = @ssh2_connect($this->ftp_server,$this->ftp_server_port);
			}
			else {
				$this->ftp_conn_id = ftp_connect($this->ftp_server,$this->ftp_server_port, $timeout);
			}

			if ($this->ftp_protocol == 'sftp'){
				if (! $this->sshConnection){
					$this->setError($this->langs->trans('FailToConnectToFTP'));
					return false;
				}

				//IDENTIFICATION SSH
				$login_result = @ssh2_auth_password($this->sshConnection, $this->ftp_user_name, $this->ftp_user_pass);
				if($login_result){

					// Init SFTP subsysteme
					$this->ftp_conn_id = @ssh2_sftp($this->connection);
					if (! $this->ftp_conn_id){
						$this->setError($this->langs->trans('CouldNotInitializeSFTPSubsystem'));
						return false;
					}
				}
			}
			else{
				if(!$this->ftp_conn_id){
					$this->setError($this->langs->trans('FailToConnectToFTP'));
					return false;
				}

				//IDENTIFICATION FTP
				$login_result = ftp_login($this->ftp_conn_id, $this->ftp_user_name, $this->ftp_user_pass);
			}


			if(!$login_result){
				$this->setError($this->langs->trans('FailToConnectToFTP').' : '.$this->langs->trans('FTPLoginFail'));
				return false;
			}


			// Activation du mode passif
			if($conf->global->WEBMOD_BLACKMIRROR_FTP_USE_PASSIVE){
				ftp_pasv($this->ftp_conn_id, true);
			}

			// get current directory
			$this->originalPath = ftp_pwd($this->ftp_conn_id);
			$this->currentPath = $this->originalPath;
		}

		// CONNEXION REUSSIE
		return true;
	}

	/**
	 * disconnect from FTP
	 *
	 * @return bool
	 */
	function ftpClose(){
		$res = ftp_close($this->ftp_conn_id);
		if($res){
			$this->ftp_conn_id = false;
		}

		return $res;
	}

	/**
	 * @param string $dir
	 * @return bool
	 */
	public function ftpChdir($dir){
		if(@ftp_chdir($this->ftp_conn_id, $dir ))
		{
			$this->currentPath = $dir;
			return true;
		}
		else{
			$this->setError($this->langs->trans('NotAnFTPDirectory', $dir));
			return false;
		}
	}


	/**
	 * Return to home dir
	 *
	 * @return bool
	 */
	public function ftpReturnToHomeDir(){

		$returnPath = explode('/',$this->ftpCurrentDir());
		$homeDir = str_repeat('../', count($returnPath) - 1);

		return $this->ftpChdir($homeDir);
	}

	/**
	 * @param string $local_file The local file path (will be overwritten if the file already exists).
	 * @param string $remote_file The remote file path.
	 * @param int $mode [optional] The transfer mode. Must be either FTP_ASCII or FTP_BINARY.
	 * @param int $resumepos [optional] The position in the remote file to start downloading from.
	 *
	 * @return bool
	 */
	public function ftpDownload($remote_file, $local_file, $mode = FTP_BINARY, $resumepos = 0){
		if(ftp_get ( $this->ftp_conn_id , $local_file , $remote_file, $mode, $resumepos ))
		{
			return true;
		}
		else{
			$this->setError($this->langs->trans('FTPFileTransfertError').' : '.$remote_file .' '.$this->langs->trans('to') .' '. $local_file);
			return false;
		}
	}

	/**
	 * @return string
	 */
	public function ftpCurrentDir(){
		$this->currentPath = ftp_pwd($this->ftp_conn_id);
		return $this->currentPath;
	}

	/**
	 * check if file exist on FTP
	 * Important note : check your curent directory before with ftpChdir or use $relative == false
	 *
	 * @param string $file
	 * @param bool $relative
	 *
	 * @return string
	 */
	public function ftpFileExists($file, $relative = true){

		if(!$relative){
			// backup curent folder and return to /
			$curentDir = $this->ftpCurrentDir();
			$this->ftpReturnToHomeDir();
		}

		$file_size = ftp_size($this->ftp_conn_id, $file);

		if(!$relative){
			$this->ftpChdir($curentDir);
		}

		if ($file_size != -1) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Get module zip package from black mirror
	 *
	 * @param User $user need for rights tests on object save
	 * @param string $modulesPath
	 * @param string $moduleInstallName
	 * @param bool $burnAfterReading supprimer le fichier après import
	 * @return int
	 */
	public function getModulesZip($user, $modulesPath = '', $moduleInstallName = '', $burnAfterReading = false){
		global $conf;

		$error = count($this->errors);

		// check module install name
		if(!empty($moduleInstallName) && !self::isModuleName($moduleInstallName)) {
			$this->setError($this->langs->trans('ErrorModuleInstallName').' : '.$moduleInstallName );
			return -1;
		}

		dol_include_once('webhost/class/webmodule.class.php');
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';

		//CONNEXION FTP
		if($this->ftp_connect()){

			if(empty($modulesPath)){
				$modulesPath = $this->ftp_path;
			}

			if($this->ftpChdir($modulesPath)){

				$localManifestJson = DOL_DATA_ROOT .'/cliatm/temp/manifest.json';


				if(file_exists($localManifestJson)){
					unlink($localManifestJson);
				}

				if($this->ftpFileExists('manifest.json') && $this->ftpDownload ('manifest.json', $localManifestJson))
				{
					if(file_exists($localManifestJson)){
						$manifestJson = file_get_contents($localManifestJson);
						if($manifestJson){
							$TManifest = json_decode($manifestJson, true);
							if(is_array($TManifest)){

								// Traitement du manifest
								foreach ($TManifest as $remoteFileName => $moduleFolderName){

									// check module name
									if(!self::isModuleName($moduleFolderName)) {
										continue;
									}

									// skip if check only one module
									if(!empty($moduleInstallName) && $moduleInstallName !== $moduleFolderName){
										continue;
									}

									if(!$this->ftpFileExists($remoteFileName)){
										$this->setLog($this->langs->trans('FileNoteFound', $remoteFileName));
										continue;
									}


									// Get module infos
									$webModule = new WebModule($this->db);

									$sql = "SELECT rowid id"
										." FROM ".MAIN_DB_PREFIX.$webModule->table_element
										." WHERE install_name = '".$this->db->escape($moduleFolderName)."'"
//										." AND status IN (".WebModule::STATUS_STABLE.",".WebModule::STATUS_DRAFT.",".WebModule::STATUS_ANALYSE.")"
										." AND entity = ".intval($conf->entity)
										." LIMIT 1";

									$resql = $this->db->query($sql);
									if($resql){

										$num = $this->db->num_rows($resql);

										if($obj = $this->db->fetch_object($resql)){
											if($webModule->fetch($obj->id) > 0){
												$upload_dir = $conf->webhost->multidir_output[$webModule->entity?$webModule->entity:$conf->entity] . "/webmodule/" . dol_sanitizeFileName($webModule->id);
												$relativedir = "/webhost/webmodule/" . dol_sanitizeFileName($webModule->id);

												if(!is_dir($upload_dir)){
													if(!dol_mkdir($upload_dir, DOL_DATA_ROOT)){
														$this->setError($this->langs->trans('ErrorMakeDir').' : '.$upload_dir);
													}
												}

												$localFileName = 'module_'.$moduleFolderName.'.zip';
												if (preg_match('/^[\/\w\-. ]+$/', $remoteFileName)){
													$localFileName = basename($remoteFileName);
												}

												$moduleZipFilePath = $upload_dir.'/'.$localFileName;
												if($this->ftpDownload ($remoteFileName, $moduleZipFilePath))
												{
													$this->setLog($this->langs->trans('ModuleZipCopied').' : '.$relativedir.'/'.$localFileName );

													// Update date on module
													$webModule->array_options['options_zip_updated'] = 1;
													$webModule->array_options['options_zip_date'] = time();
													$resUp = $webModule->update($user);
													if($resUp<0){
														$this->setError($this->langs->trans('ErrorUpdateModuleExtra', $remoteFileName));
													}


													// Update module info about last file update for dash board
													$this->updateModuleEcmFiles($user, $webModule, $localFileName, $relativedir);

													// Delete archive from FTP if needed
													if($burnAfterReading && strpos($remoteFileName, '/') === false){
														// Tente d'effacer le fichier $fileName sur le FTP
														if (ftp_delete($this->ftp_conn_id, $remoteFileName)) {
															$this->setLog($this->langs->trans('DeletedFromFtp', $remoteFileName));
														} else {
															$this->setError($this->langs->trans('ErrorDeletedFromFtp', $remoteFileName));
														}
													}
												}
												else{
													$this->setError($this->langs->trans('ErrorModuleZipCopy').' : '.$moduleZipFilePath );
												}
											}
											else{
												$this->setError($this->langs->trans('ErrorFetchingModule').' : '.$obj->id );
											}
										}
										else{
											$this->setLog($this->langs->trans('ModuleNotFound').' : '.$moduleFolderName );
										}
									}
									else{
										$this->setError($this->db->error());
									}
								}
							}
							else{
								$this->setError($this->langs->trans('ErrorJsonFileNotReadable').' : '.$localManifestJson );
							}
						}
						else{
							$this->setError($this->langs->trans('ErrorJsonFileNotReadable').' : '.$localManifestJson );
						}
					}
					else {
						$this->setError($this->langs->trans('ErrorJsonFileNotExist').' : '.$localManifestJson );
					}
				}
			}
		}

		return $error - count($this->errors);
	}

	/**
	 *
	 * @param User   $user
	 * @param WebModule $webModule
	 * @param string $filename
	 * @param string $relativepath Relative path of file from document directory. Example: path/path2/file
	 */
	public function updateModuleEcmFiles($user, $webModule, $fileFullPath, $relativepath){

		require_once DOL_DOCUMENT_ROOT.'/ecm/class/ecmfiles.class.php';


		$destfull = dirname($fileFullPath);
		$filename = basename($fileFullPath);

		$relativepath = rtrim($relativepath, '/');
		$relativepath = ltrim($relativepath, '/');

		$ecmfile = new EcmFiles($this->db);
		$result = $ecmfile->fetch(0, '', ($relativepath ? $relativepath.'/' : '').$filename);

		// Set the public "share" key
		$setsharekey = false;

		if ($setsharekey) {
			if (empty($ecmfile->share))	// Because object not found or share not set yet
			{
				require_once DOL_DOCUMENT_ROOT.'/core/lib/security2.lib.php';
				$ecmfile->share = getRandomPassword(true);
			}
		}

		$ecmfile->gen_or_uploaded = 'uploaded';
		$ecmfile->fullpath_orig = '';
		$ecmfile->description = ''; // indexed content
		$ecmfile->keyword = 'moduleUpdate'; // keyword content

		if ($result > 0)
		{
			$ecmfile->label = md5_file(dol_osencode($destfull)); // hash of file content
			$result = $ecmfile->update($user);
		}
		else
		{
			$ecmfile->entity = $webModule->entity;
			$ecmfile->filepath = $relativepath;
			$ecmfile->filename = $filename;
			$ecmfile->label = md5_file(dol_osencode($destfull)); // hash of file content
			$ecmfile->src_object_type = $webModule->element;
			$ecmfile->src_object_id   = $webModule->id;
			$result = $ecmfile->create($user);
		}

		if ($result < 0) {
			$this->setError($ecmfile->error);
		}

	}

	/**
	 * Planned task compatible method to get module zip package from black mirror
	 *
	 * @param string $modulesPath
	 * @param string $moduleInstallName
	 * @param bool $burnAfterReading supprimer le fichier après import
	 *
	 * @return int   0 on success | -1 on error
	 */
	public function cron_getModulesZip($modulesPath = '', $moduleInstallName = '', $burnAfterReading = false){
		global $user;

		$result = $this->getModulesZip($user, $modulesPath, $moduleInstallName, $burnAfterReading);

		if(!empty($this->errors))
		{
			if(!empty($this->output)){$this->output.='<br/>';}
			$this->output.= '<div class="error" >'.implode('<br/>', $this->errors).'</div>';
		}

		if(!empty($this->logs))
		{
			if(!empty($this->output)){$this->output.='<br/>';}
			$this->output.= implode('<br/>', $this->logs);
		}

		return $result;
	}


	/**
	 * Check for module name validity
	 *
	 * @param string $moduleName Module name to validate
	 * @return boolean Validity is ok or not
	 */
	static public function isModuleName($moduleName)
	{
		return preg_match('/^[a-z0-9_-]+$/ui', $moduleName);
	}

}
