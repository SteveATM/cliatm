<?php


/**
 * Class CliATMCron
 */
class CliATMCron
{
	/**
	 * @var DoliDB $db
	 */
	public $db;

	/**
	 * @var string $output used for cron task
	 */
	public $output;

	/**
	 * @var string[] $errors
	 */
	public $errors = array();

	/**
	 * @var string[] $logs
	 */
	public $logs = array();

	/**
	 * @var Translate $langs
	 */
	public $langs;

	const ORDER_TYPE_PROJECT = 1;
	const ORDER_TYPE_SUBSCRIPTION = 3;
	const ROLE_BILLING = "BILLING";
	const ROLE_CONTRAT = "SALESREPSIGN";
    const DAY_GAP = -2;  // nombres de jours avant de considérer une commande brouillon élligible au cron

	/** Statuts utilisés pour le log d’événements ( @see setLog ) */
	const INFO     = 'info';
	const SUCCESS  = 'success';
	const WARNING  = 'warning';
	const ERROR    = 'error';
	const CRITICAL = 'critical';

	/** Valeurs de retour de la méthode CRON */
	const CRON_RESPONSE_OK = 0;
	const CRON_RESPONSE_KO = 1;

	public function __construct()
	{
		global $conf, $langs, $db;

		$this->db = $db;

		$this->langs = $langs;
		$this->langs->load('cliatm@cliatm');

		// basic stylesheet for log output
		$styles = array(
			self::INFO => 'color: black;',
			self::SUCCESS => 'color: green;',
			self::WARNING => 'color: orange;',
			self::ERROR => 'color: red; font-weight: bold;',
			self::CRITICAL => 'color: red; font-weight: bold;',
		);
		$this->output .= '<style>' . "\n";
		foreach ($styles as $name => $properties) {
			$this->output .= '.' . $name . ' {' . $properties . '}' . "\n";
		}
		$this->output .= '</style>';

	}

	function __destruct() {
		// close any opened files, free resources, etc.
	}

	/**
	- NEW : Tâche CRON Traitement des commandes brouillon type "Abonnement" :
	- validation de la commande
	- validation du contrat créé par le trigger ORDER_VALIDATE
	- génération du PDF du contrat (modèle DocEdit)
	- envoi du PDF par e-mail au(x) contact(s) "SALESREPSIGN" avec CC au commercial du tiers (modèle d’e-mail
	configurable). On ne traite que les commandes datant de plus de 48h. L’utilisateur effectuant les actions est
	"administration". - 1.12.0 *14/04/2021*
	 */
	public function CRON_workflow_ATM_subscription()
	{
		include_once DOL_DOCUMENT_ROOT . '/commande/class/commande.class.php';
		include_once DOL_DOCUMENT_ROOT . '/contrat/class/contrat.class.php';
		dol_include_once('/referenceletters/class/referenceletters.class.php');
		global $conf;

		// récupération du modèle DocEdit à mettre sur les contrats qui n’en ont pas
		$docedit = new ReferenceLetters($this->db);
		$resfetch = $docedit->fetch_all(
			'DESC',
			'tms',
			1,
			0,
			array(
				't.status' => 1,
				't.element_type' => 'contract',
			)
		);
		if ($resfetch < 1 || empty($docedit->lines)) {
			$this->setError($this->langs->trans('CronCannotFindDocEditTemplateForContract'));
			return self::CRON_RESPONSE_KO;
		} else {
			$docedit_tpl = $docedit->lines[0]->id;
		}

		// 1) fetch commandes brouillon type "Abonnement"
		global $user;
		if (!$user->id) {
			$user = new User($this->db);
			if ($user->fetch(0, 'administration') < 0) {
				return self::CRON_RESPONSE_KO;
			}
		}
		// @TODO harmoniser la gestion des erreurs entre le code de J-P et le mien

		$dateFilterCreatedBefore = dol_time_plus_duree(dol_now(), self::DAY_GAP, 'd');
		$TOrderId =  $this->getDraftOrders(self::ORDER_TYPE_SUBSCRIPTION, $dateFilterCreatedBefore);
		if ($TOrderId == -1) {
			return self::CRON_RESPONSE_KO;
		} elseif (empty($TOrderId)) {
			$this->setMessage($this->langs->trans('NoDraftSubscriptionOrderFound', -self::DAY_GAP));
			return self::CRON_RESPONSE_OK;
		}

		foreach ($TOrderId as $orderId) {
			$error = false;
			$commande = new Commande($this->db);
			if (!$error && ($commande->fetch($orderId) <= 0 || $commande->fetch_thirdparty() <= 0)) {
				$this->setError($this->langs->trans(
					'CronCannotFetchOrder',
					$orderId,
					$commande->error
				));
				$error = true;
			}
			$this->db->begin();
			$this->setMessage('DB BEGIN');
			if (!$error && $commande->valid($user) <= 0) {
				$this->setError($this->langs->trans(
					'CronCannotValidateOrder',
					$orderId,
					$commande->error
				));
				$error = true;
			}
			$commande->fetchObjectLinked();
			if (!$error && !empty($commande->linkedObjectsIds['contrat'])) {
				foreach ($commande->linkedObjectsIds['contrat'] as $fk_contrat) {
					$contrat = new Contrat($this->db);
					if ($contrat->fetch($fk_contrat) <= 0) {
						$this->setError($this->langs->trans(
							'CronCannotFetchContract',
							$fk_contrat,
							$contrat->error
						));
						$error = true;
					}
					if (!$error && $contrat->validate($user) <= 0) {
						$this->setError($this->langs->trans(
							'CronCannotValidateContract',
							$fk_contrat,
							$contrat->error
						));
						$error = true;
					}
					if (!$error && empty($contrat->array_options['options_rfltr_model_id'])) {
						// si aucun modèle DocEdit sur le contrat, on sélectionne le premier qui correspond au type
						$contrat->array_options['options_rfltr_model_id'] = $docedit_tpl;
						$contrat->insertExtraFields();
					}
					if (!$error) {
						if (!$contrat->generateDocument('rfltr_dol_contrat', $this->langs)) {
							$this->setError($this->langs->trans(
								'CronCannotGeneratePDF',
								$fk_contrat,
								$contrat->error
							));
							$error = true;
						}
					}
					if (!$error) {
						if (!empty($conf->global->CLIATM_CONTRACT_MAIL_MODEL)) {
							//  envoi mail
							$resultMail = $this->sendMail(
								$contrat,
								self::ROLE_CONTRAT,
								$conf->global->CLIATM_CONTRACT_MAIL_MODEL,
								$commande->thirdparty
							);

							if ($resultMail < 0) {
								// rôle
								if ($resultMail == -1) {
									$this->setError($this->langs->trans("PDFNotFound"));
								}

								// destinataire
								if ($resultMail == -2) {
									$this->setError($this->langs->trans("NoContactWithRoleFound", self::ROLE_CONTRAT, $contrat->thirdparty->getNomUrl()));
								}

								// cmailfile generique
								if ($resultMail == -3) {
									$this->$this->setError($this->langs->trans("mailSendingError"));
								}
								$this->setMessage($this->langs->trans("DB ROLLBACK"));
								$this->db->rollBack();
							} else {
								$this->setMessage($this->langs->trans('SubscriptionOrderDone', $commande->getNomUrl(), $contrat->getNomUrl(), $contrat->thirdparty->getNomUrl()));
								$this->setMessage($this->langs->trans("DB COMMIT"));
								$this->db->commit();
							}
							$error = true;
						}
					}
				}
			}
		}
		if (count($this->errors)) {
			return self::CRON_RESPONSE_KO;
		} else {
			return self::CRON_RESPONSE_OK;
		}


		// 2) foreach ($commandesAbo as $commandeAbo) {

		// 3) 	valider commande
		// 3) 	le trigger crée un contrat
		// 3)   valider contrat
		// 3)   générer PDF (DocEdit)
		// 3)   envoyer contrat par e-mail au(x) contacts de type "SALESREPSIGN", copie aux commerciaux, en utilisant un modèle existant configurable

		// 4) }
	}

	/**
	- NEW : Tâche CRON Traitement des commandes brouillon type "Projet" :
	- validation de la commande
	- validation de la facture d’acompte créée par le trigger ORDER_VALIDATE
	- génération du PDF de la facture
	- envoi du PDF par e-mail au(x) contact(s) "BILLING" avec CC au commercial du tiers (modèle d’e-mail
	configurable). On ne traite que les commandes datant de plus de 48h. L’utilisateur effectuant les actions est
	"administration". - 1.12.0 *14/04/2021*
	 */
	public function CRON_workflow_ATM_project()
	{
		global $conf,$langs,$user,$db;
		$error = 0;
		$dateFilterCreatedBefore = dol_time_plus_duree(dol_now(), self::DAY_GAP, 'd');
		$TOrders =  $this->getDraftOrders(self::ORDER_TYPE_PROJECT, $dateFilterCreatedBefore);

		if(is_array($TOrders) && count($TOrders) > 0) {

			foreach ($TOrders as $order) {
				$ord = new Commande($this->db);
				$error = 0;
				$ord->fetch($order);
				$ord->fetch_thirdparty();

				$db->begin();
				$this->setMessage('DB BEGIN');

				$res = $ord->valid($user);

				if ($res > 0) {
					$this->setMessage($ord->ref . ' '.$langs->trans("validated"));
					//  récuperation  de la facture generé par Trigger
					$ord->fetchObjectLinked();

					if (count($ord->linkedObjectsIds['facture']) == 1) {

						$InvoiceId = array_pop($ord->linkedObjectsIds['facture']);
						$fac = new Facture($this->db);
						$fac->fetch($InvoiceId);

						if ($fac->statut == Facture::STATUS_DRAFT) {
							//  validation de la facture
							$resfac = $fac->validate($user);

							if ($resfac > 0) {
								$this->setMessage($fac->ref . ' '.$langs->trans("validated"));

								// 4 generation du pdf
								$fac->generateDocument($fac->modelpdf, $langs);


								if (!empty($conf->global->CLIATM_DEPOSIT_MAIL_MODEL)){
									//  envoi mail
									$resultMail = $this->sendMail($fac, self::ROLE_BILLING, $conf->global->CLIATM_DEPOSIT_MAIL_MODEL,$ord->thirdparty);
									if ($resultMail < 0) {
										// rôle
										if ($resultMail == -1) {
											$this->setError($this->langs->trans("PDFNotFound"));
										}

										// destinataire
										if ($resultMail == -2) {
											$this->setError($langs->trans("NoContactWithRoleFound", self::ROLE_BILLING, $ord->thirdparty->getNomUrl()));
										}

										// cmailfile generique
										if ($resultMail == -3) {
											$this->setError($langs->trans("mailSendingError"));
										}
										$this->setMessage($langs->trans("DB ROLLBACK"));
										$db->rollBack();
									} else {
										$this->setMessage($langs->trans("DB COMMIT"));
										$db->commit();
									}
								}else{
									// pas de model selectionné on rollback et retour message utilisateur
									$db->rollBack();
									setEventMessage($langs->trans("defaultMailModelNotSelected"));
								}
							} else {
								$this->setError($langs->trans("errorinvoiceassociate"));
							}
						}else{
							$this->setError($langs->trans("errorNoInvoiceDraft"));
						}
					}else{
						$this->setError($langs->trans("errorNoInvoiceDraftFromLinkedObject"));
					}
				} else {
					$this->setError($langs->trans("errorvalidatedCommand"));
				}
			}
		}else{
			$this->setError($langs->trans("NoDraftCommandToProcess"));
		}
		return 0;

	}

	/**
	 * @param Facture|Contrat $object
	 * @param string $Role
	 * @param int $idtemplate
	 * @param Societe $thirdparty
	 */
	public function sendMail($object, $Role, $idtemplate, &$thirdparty)
	{
		global $conf, $langs, $user;
		$sent = 0;
		$error  = 0;
		$message = "";
		include_once DOL_DOCUMENT_ROOT.'/core/class/html.formmail.class.php';
		$formMail = new FormMail($this->db);
		$type_template = '';
		if ($object->element === 'facture') $type_template = 'facture_send';
		if ($object->element === 'contrat') $type_template = 'contract';
		$TMessages = $formMail->getEMailTemplate($this->db, $type_template, $user, $langs, $idtemplate);
		$formMail->setSubstitFromObject($object, $langs);

		// PDF attachment
		$listofpaths = array();
		$listofmimes = array();
		$listofnames = array();
		$dir_output = $conf->{$object->element}->dir_output; // this works for facture and contrat; could fail for others
		$fileparams = dol_most_recent_file(
			$dir_output . '/' . $object->ref,
			preg_quote($object->ref, '/') . '[^\-]+'
		);
		if (!empty($fileparams)) {
			$file = $fileparams['fullname'];
			$listofpaths[] = $file;
			$listofmimes[] = dol_mimetype($file);
			$listofnames[] = basename($file);
		} else {
			return -1;
		}

		// message and object
		$sendContent = make_substitutions($TMessages->content, $formMail->substit);
		$sendTopic = make_substitutions($TMessages->topic, $formMail->substit);

		// Recipient
		$TContact = $object->liste_contact(-1,'external', $Role);
		if (empty($TContact)) {
			// on récupère tous les contacts du tiers
			$TContact = $thirdparty->contact_array_objects();
			// on filtre sur le rôle désiré
			$TContact = array_filter($TContact, function ($contact) use ($Role) {
				/** @var Contact $contact */
				$roleCodes = array_column($contact->roles, 'code');
				return in_array($Role, $roleCodes);
			});
			$TsendTo = array_map(
				function ($contact) use ($langs) {
					return dol_string_nospecial($contact->getFullName($langs), ' ', array(',')) . ' <' . $contact->email . '>';
					},
				$TContact
			);
		} else {
			$TsendTo = array();
			foreach ($TContact as $contactId) {
				$object->fetch_contact($contactId);
				$TsendTo[] = dol_string_nospecial($object->contact->getFullName($langs),' ',array(",")). " <".$object->contact->email.">";
			}
		}

		//
		if (empty($TsendTo)){
			return -2;
		}

		//Ajout des commerciaux SI existants
		$salerep = $thirdparty->getSalesRepresentatives($user);

		foreach ($salerep as $saler){
			$salerepobj = new User($this->db);
			$salerepobj->fetch($saler['id']);
			$TsendTo[] = dol_string_nospecial($salerepobj->getFullName($langs),' ',array(",")). " <".$salerepobj->email.">";
		}

		// Prepare for CMAILFILE
		$to = implode(",",$TsendTo);

		//sender
		$from = $user->email;
		include_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';
		$cMailFile = new CMailFile($sendTopic, $to, $from, $sendContent, $listofpaths, $listofmimes, $listofnames, '', "",  0, 1, '', '', '', '','' , '');
		if($cMailFile->sendfile()){
			$sent++;
		} else{
			$message.=  $cMailFile->error .' : '.dol_escape_htmltag($to)."\n";
			$error++;
		}


		if (!$error) {
			return 0;
		} else {
			return -3;
		}

	}

	/**
	 * récupère les commandes brouillon avec un type passé en params
	 * @param int $type                     filtre sur extrafield "type de commande"
	 * @param int $dateFilterCreatedBefore  Les commandes créées après cette date (timestamp) ne seront pas retournées
	 * @return int|int[]   array des ids commandes  ou -1 si erreur pas de commande
	 */
	private function getDraftOrders($type, $dateFilterCreatedBefore){
		global $db;
		$error = 0;
		$TOrders = array();

		require_once DOL_DOCUMENT_ROOT."/commande/class/commande.class.php";
		$orders = new Commande($db);

		$sql = " SELECT c.rowid  FROM ".MAIN_DB_PREFIX."commande as c";
		$sql .= " LEFT JOIN ".MAIN_DB_PREFIX ."commande_extrafields  as ce on ce.fk_object = c.rowid ";
		$sql .= " WHERE c.fk_statut = ".Commande::STATUS_DRAFT;
        $sql .= " AND c.date_creation < '" . $this->db->idate($dateFilterCreatedBefore) . "'";
		$sql .= " AND ce.order_type = ". intval($type);

		$result = $this->db->query($sql);

		if ($result) {
			$aRes = array();
			while($obj = $this->db->fetch_object($result)) {
				$TOrders[] = $obj->rowid;
			}
			return $TOrders;
		} else {
			$this->setError($this->langs->trans('CronErrorGetDraftOrdersSQL', $sql, $this->db->lasterror()));
			return -1;
		}
	}


	/**
	 * Permet gérer les retour d'erreur avec messages
	 *
	 * @param string $err
	 */
	public function setError($err)
	{
		$this->output .= '<p class="' . self::ERROR . '">' . $err . '</p>';
	}

	/**
	 * Permet gérer les retour d'erreur avec messages
	 *
	 * @param string $msg
	 */
	public function setMessage($msg)
	{
		$this->output .= '<p class="' . self::INFO . '">' . $msg . '</p>';
	}
}
