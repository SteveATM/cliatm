<?php
/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015 ATM Consulting <support@atm-consulting.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    class/actions_cliatm.class.php
 * \ingroup cliatm
 * \brief   This file is an example hook overload class file
 *          Put some comments here
 */

/**
 * Class Actionscliatm
 */
class Actionscliatm
{
	/**
	 * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
	 */
	public $results = array();

	/**
	 * @var string String displayed by executeHook() immediately after return
	 */
	public $resprints;

	/**
	 * @var array Errors
	 */
	public $errors = array();

	/**
	 * Constructor
	 */
	public function __construct()
	{
	}

	/**
	 * Overloading the doActions function : replacing the parent's function with the one below
	 * - Do stuff on a propal card modification
	 * - Save time spent on a ticket card thanks to a dedicated extrafield
	 * - Deals with the webinstance initialization on a webhost card
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function doActions($parameters, &$object, &$action, $hookmanager)
	{
	    global $conf, $langs, $arrayfields, $db, $user;
		$error = 0; // Error counter

		$contexts = explode(':',$parameters['context']);

		if (in_array('propalcard', $contexts))
		{
			if($action == 'edit_extras' && GETPOST('attribute') == 'date_cloture_prev') {
				setEventMessage('La date de facturation prévisionnelle sera actualisée en fonction de la saisie.', 'warnings');
			}
			if($action == 'update_extras' && GETPOST('attribute') == 'date_cloture_prev') {
				if(empty($conf->global->MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON)) {
					setEventMessage('Param MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON must be filled! See CLIATM module setup', 'errors');
					return 0;
				}
				require_once DOL_DOCUMENT_ROOT.'/core/lib/date.lib.php';
				$dateCloturePrev = dol_mktime(12, 0, 0, GETPOST('options_date_cloture_prevmonth'), GETPOST('options_date_cloture_prevday'), GETPOST('options_date_cloture_prevyear'));

				// Si champs 'options_date_cloture_prev' modifié
				if(! empty($dateCloturePrev)) {
					// dol_time_plus_duree ne supporte pas les dates au format YYYY-MM-DD
					$object->array_options['options_date_cloture_prev'] = $dateCloturePrev;

					// remplir options_date_liv_prev à options_date_cloture_prev + 1mois
					$delta = !empty($conf->global->MAIN_NB_DAY_CALCULATE_DATE_LIVRAISON) ? $conf->global->MAIN_NB_DAY_CALCULATE_DATE_LIVRAISON : 30;
					$object->array_options['options_date_liv_prev'] = dol_time_plus_duree($object->array_options['options_date_cloture_prev'], $delta, 'd');

					// Ajout nombre de jour = total / 250
					$nbJourTotal = ($object->total_ht / $conf->global->MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON);
					$object->array_options['options_date_liv_prev'] = dol_time_plus_duree($object->array_options['options_date_liv_prev'], (int) $nbJourTotal, 'd');
				}
			}
		}

		if (in_array('tasklist', $contexts)) {

			$arrayfields['t.description'] = array(
				'label' => $langs->trans('Description'),
				'position' => 90
			);
		}

		// Save a time spent on the update of a dedicated extrafields (trick because the ticket/card.php is not standard and doesn't allow a proper formconfirm and doaction)
		if (in_array('ticketcard', $contexts)) {
			if($action == 'update_extras') {
				$att = GETPOST('attribute', 'alpha');
				if($att == 'timespent') {
					$timespent_duration = GETPOST('options_timespent', 'int');

					// Do some value check
					if(empty($object->fk_project)) setEventMessage($langs->trans('ErrorProjectIsMandatory'), 'errors');
					else if(empty($timespent_duration)) setEventMessage($langs->trans('ErrorTimeSpentEmpty'), 'errors');
					else if($timespent_duration * 100 % 25 != 0) setEventMessage($langs->trans('ErrorTimeSpentMustBeQuarterHour'), 'errors');
					else if($timespent_duration > 4) setEventMessage($langs->trans('ErrorTimeSpentMoreThan4Hours'), 'errors');
					else if($timespent_duration < 0) setEventMessage($langs->trans('ErrorTimeSpentNegative'), 'errors');
					else {
						require_once DOL_DOCUMENT_ROOT.'/projet/class/task.class.php';

						// Get the correct task to record the time spent
						$label = ($object->type_code == 'PROJECT') ? 'Tickets internes' : 'Tickets';
						$sql = "SELECT pt.rowid";
						$sql.= " FROM ".MAIN_DB_PREFIX."projet_task pt";
						$sql.= " WHERE pt.fk_projet = ".$object->fk_project;
						$sql.= " AND pt.label = '".$label."'";

						$resql = $db->query($sql);
						if($resql) {
							$obj = $db->fetch_object($resql);
							if(empty($obj->rowid)) setEventMessage($langs->trans('ErrorTaskNotFoundInProject', $label), 'errors');
							else {
								$task = new Task($db);
								$task->fetch($obj->rowid);

								$task->timespent_note = 'Ticket#'.$object->ref;
								$task->timespent_duration = $timespent_duration * 60 * 60;
								$task->timespent_date = dol_now();
								$task->timespent_fk_user = $user->id;

								$res = $task->addTimeSpent($user);
								if($res > 0) setEventMessage($langs->trans('RecordSaved'));
								else setEventMessage($task->error, 'errors');
							}
						} else {
							dol_print_error($db);
						}
					}

					// Force the action back to view so the extrafield value is not saved with the standard update_extra action
					$action = 'view';
				}
			}
		}

		if(in_array('webhostcard', $contexts)) {
			if($action == 'webinstance_init') {
				$form = new Form($db);
				$token = $object->api_token;
				$tokenTime = time();
				$hash = md5($token . $tokenTime);

				$urlobserver = $object->getObserverUrl();;
				$urlobserver.= '?action=get-all-instance&hash='.$hash.'&time='.$tokenTime;

				$res = file_get_contents($urlobserver);

				if($res) {
					$instances = json_decode($res);
					if(!empty($instances)) {
						$actionstodo = $this->_webinstance_init($object, $instances);

						$msgs = array();
						$msgs[] = $langs->trans('NbInstancesToCreate', count($actionstodo['create']));

						foreach($actionstodo['create'] as $iname => $ipath) {
							$newinstance = $this->_webinstance_create($object, $iname, $ipath);
							if($newinstance->id > 0) {
								$msgs[] = ' - ' . $langs->trans('InstanceCreated', $newinstance->label);
							} else {
								$msgs[] = ' - ' . $langs->trans('InstanceCreationError', $newinstance->error);
							}
						}

						$msgs[] = $langs->trans('NbInstancesToUpdate', count($actionstodo['update']));

						/*foreach($actionstodo['update'] as $curintance) {
							$curintance->updateMonitoring() // TODO : coder cette fonction
							if($newinstance->id > 0) {
								$msgs[] = ' - ' . $langs->trans('InstanceCreated', $newinstance->name);
							} else {
								$msgs[] = ' - ' . $langs->trans('InstanceCreaionError', $newinstance->name);
							}
						}*/

						$msgs[] = $langs->trans('NbInstancesToCheck', count($actionstodo['check']));

						foreach($actionstodo['check'] as $curintance) {
							$msgs[] = ' - ' . $curintance->label. ' ('.$curintance->folder.')';
						}

						setEventMessages('', $msgs, 'warnings');
					} else {
						setEventMessage($res, 'errors');
					}
				} else {

				}
			}
		}

		if (! $error)
		{
			$this->results = array();
			$this->resprints = '';
			return 0; // or return 1 to replace standard code
		}
		else
		{
			$this->errors[] = 'Error message';
			return -1;
		}
	}

	function _webinstance_init(WebHost $webHost, array $instances) {
		global $langs, $user;
		$msgs = array();
		$actionstodo = array('check' => array(), 'create' => array(), 'update' => array());

		// Get all instances linked to the webhost
		$curInstances = $webHost->getAllInstances();

		$actionstodo['nb_existing_instances'] = 0;
		if(!empty($curInstances)) {
			$actionstodo['nb_existing_instances'] = count($curInstances);
			$msgs[] = $langs->trans('NbInstancesLinkedToThisHost', count($curInstances));
			foreach ($curInstances as $inst) {
				// Check if instance is still present on the server
				$inst_folder = $inst->folder;
				if(!in_array($inst_folder, $instances)) {
					$actionstodo['check'][$inst_folder] = $inst;
				} else {
					$actionstodo['update'][$inst_folder] = $inst;
				}
			}
		}

		// Get all instances found on the server
		$actionstodo['nb_instances_on_server'] = 0;
		if(!empty($instances)) {
			$msgs[] = $langs->trans('NbInstancesFoundOnTheServer', count($instances));
			$actionstodo['nb_instances_on_server'] = count($instances);
			foreach ($instances as $inst_name) {
				$inst_path = $webHost->array_options['options_instance_path'].$inst_name;
				if(empty($actionstodo['update'][$inst_name])) {
					$actionstodo['create'][$inst_name] = $inst_path;
				}
			}
		}

		setEventMessages('', $msgs);

		return $actionstodo;
	}

	function _webinstance_create(WebHost $webHost, $instname, $instpath) {
		global $langs, $db, $user;

		$instance = new WebInstance($db);
		$instance->ref = $instance->getRef();
		$instance->label = $instname;
		$instance->folder = $instpath;
		$instance->fk_webinstance_type = 1;
		$instance->fk_webhost = $webHost->id;
		$instance->api_observer_type = 'servermanager_v1';
		$instance->activate_monitoring = 1;
		$instance->date_install = '';
		$instance->date_disable = '';
		$instance->date_cancel = '';
		$instance->create($user);

		return $instance;
	}

	/**
	 * Overloading the insertExtraFields function : replacing the parent's function with the one below
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function insertExtraFields($parameters, &$object, &$action, $hookmanager)
	{
		global $conf;

		$error = 0; // Error counter
		$myvalue = 'test'; // A result value

		if (in_array('propalcard', explode(':', $parameters['context'])))
		{

		}

		if (! $error)
		{
			$this->results = array();
			$this->resprints = '';
			return 0; // or return 1 to replace standard code
		}
		else
		{
			$this->errors[] = 'Error message';
			return -1;
		}

	}

	/**
	 * Override addMoreActionsButtons function : add button to dispaly ATM propal board
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function addMoreActionsButtons($parameters, &$object, &$action, $hookmanager) {

		global $langs,$db,$user, $conf;

		$contexts = explode(':',$parameters['context']);

		if(in_array('propalcard',$contexts) || in_array('invoicecard',$contexts)) {

			$out = '<div class="inline-block divButAction"><a id="openerPropalBoard" class="butAction" href="">' . $langs->trans('BtnPropalBoard') . '</a></div>';

			$this->results = array();
			$this->resprints = $out;
			print $out;
			return 0; // or return 1 to replace standard code
		}

		if(in_array('webhostcard',$contexts)) {

			$out = '';
			if(!empty($object->url_api_observer)) {
				$actionUrl = $_SERVER["PHP_SELF"] . '?id=' . $object->id . '&amp;action=';
				$params = array('attr' =>array('class'=>'classfortooltip', 'title' => $langs->trans('WebHostInitInstancesHelp')));
				$out = dolGetButtonAction($langs->trans("WebHostInitInstances"), '', 'default', $actionUrl . 'webinstance_init', '', $user->rights->webhost->instance->write, $params);
			}

			$this->results = array();
			$this->resprints = $out;
			print $out;
			return 0; // or return 1 to replace standard code
		}
	}

	/**
	 * Override formConfirm function : display ATM propal board
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function formConfirm($parameters, &$object, &$action, $hookmanager)
	{
		global $langs,$db,$user, $conf;

		$langs->load('cliatm@cliatm');

		require_once DOL_DOCUMENT_ROOT . '/core/lib/date.lib.php';
		require_once DOL_DOCUMENT_ROOT . '/categories/class/categorie.class.php';
		dol_include_once('/cliatm/lib/cliatm.lib.php');

		$contexts = explode(':',$parameters['context']);

		if(in_array('ordercard',$contexts) || in_array('propalcard',$contexts) || in_array('invoicecard',$contexts)) {

			$out = getAtmPropalBoard($object, 1, 1);
			print $out;
			$this->resprints = $out;
		}

		return 0;
	}
	/**
	 * EXTERNAL ACCESS Override externalAccessTicketCardSummary function
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function externalAccessTicketCardSummary($parameters, &$object, &$action, $hookmanager)
	{
		global $langs,$db,$user, $conf;

		$langs->load('cliatm@cliatm');

		$contexts = explode(':',$parameters['context']);

		if(in_array('externalaccesspage',$contexts)) {

			if(!empty($object->fk_project)) {
				require_once DOL_DOCUMENT_ROOT.'/projet/class/task.class.php';

				// Get total of time spent on this ticket. Found because the note of the time spent is "Ticket#" + ticket ref
				$sql = "SELECT pt.rowid, SUM(ptt.task_duration) as total_time";
				$sql.= " FROM ".MAIN_DB_PREFIX."projet_task pt";
				$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."projet_task_time ptt ON ptt.fk_task = pt.rowid";
				$sql.= " WHERE ptt.note = 'Ticket#".$object->ref."'";
				$sql.= " GROUP BY pt.rowid";

				$resql = $db->query($sql);
				if($resql) {
					$out = $out2 = '';
					if($db->num_rows($resql) > 0) {
						// Time spent on ticket can be stored on 2 tasks : "Tickets" and "Tickets internes" (when ticket type is "Retours projet")
						while ($obj = $db->fetch_object($resql)) {
							$task = new Task($db);
							$task->fetch($obj->rowid);

							$total_time = $obj->total_time / 3600;
							$out2.= $total_time . ' h';
						}
					} else {
						$out2 = $langs->trans('None');
					}


					$out.= '<hr/><h6 class="text-center">'.$langs->trans('TicketTimeSpentInformations').'</h6>';


					// Total temp passé
					$out.= '
						<div class="row clearfix form-group" id="trextrafieldseparatortimespent">
							<div class="col-md-2">'.$langs->trans('TotalTimeSpent').'</div>
							<div class="col-md-10">'.$out2.'</div>
						</div>';


					// Informations générale sur l'assistance du client
					dol_include_once('/cliatm/lib/cliatm_assistance.lib.php');
					$infos = getAssistanceInfosFromProject($object->fk_project);


					$purchasedNWarning = '';
					if($infos['consumedN'] > $infos['purchasedN']) $purchasedNWarning.= '<i class="fa fa-warning" ></i>';

					$out.= '
						<div class="row clearfix form-group">

							<div class="col-md-2">'.$langs->trans('Conso', date('Y')).'</div>
							<div class="col-md-4">'.price($infos['consumedN']).' h / '.price($infos['purchasedN']).' h '.$purchasedNWarning.'</div>

							<div class="col-md-2">'.$langs->trans('Conso', date('Y') - 1).'</div>
							<div class="col-md-4">'.price($infos['consumedN']).' h / '.price($infos['purchasedNM1']).' h '.'</div>

						</div>';

					$out.= '
						<div class="row clearfix form-group" >

							<div class="col-md-2">'.$langs->trans('SalesRepresentative').'</div>
							<div class="col-md-4">'.(is_object($infos['commercial']) ? $infos['commercial']->getFullName($langs, '') : $infos['commercial']).'</div>

							<div class="col-md-2">'.$langs->trans('ProjectManager').'</div>
							<div class="col-md-4">'.(is_object($infos['cdp']) ? $infos['cdp']->getFullName($langs, '') : $infos['cdp']).'</div>

						</div>';

					$parameters['ticketMorePanelBodyBottom'] = $out;
				} else {

					$parameters['ticketMorePanelBodyBottom'] =  '<div class="row clearfix form-group" id="trextrafieldseparatortimespent">
						<div class="col-md-4">'.$langs->trans('TicketTimeSpentInformations').'</div>
						<div class="col-md-8">'.$langs->trans('ErrorLoadingTicketTimeSpentInformations').'</div>
					</div>';
				}
			}
		}


		return 0;
	}

	/**
	 * Overloading the formObjectOptions function :
	 * - On the ticket card, display the total spent time from the 'Ticket' or 'Ticket interne' task linked to the project
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function formObjectOptions($parameters, &$object, &$action, $hookmanager) {
		global $db, $langs;
		$contexts = explode(':',$parameters['context']);

		// On the ticket card, display the total spent time from the 'Ticket' or 'Ticket interne' task linked to the project and specific custiomer infos about assistance
		if(in_array('ticketcard',$contexts)) {
			if(!empty($object->fk_project)) {
				require_once DOL_DOCUMENT_ROOT.'/projet/class/task.class.php';

				// Get total of time spent on this ticket. Found because the note of the time spent is "Ticket#" + ticket ref
				$sql = "SELECT pt.rowid, SUM(ptt.task_duration) as total_time";
				$sql.= " FROM ".MAIN_DB_PREFIX."projet_task pt";
				$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."projet_task_time ptt ON ptt.fk_task = pt.rowid";
				$sql.= " WHERE ptt.note = 'Ticket#".$object->ref."'";
				$sql.= " GROUP BY pt.rowid";

				$resql = $db->query($sql);
				if($resql) {
					$out = $out2 = '';
					if($db->num_rows($resql) > 0) {
						// Time spent on ticket can be stored on 2 tasks : "Tickets" and "Tickets internes" (when ticket type is "Retours projet")
						while ($obj = $db->fetch_object($resql)) {
							$task = new Task($db);
							$task->fetch($obj->rowid);

							$total_time = $obj->total_time / 3600;
							$out2.= $total_time . ' h';
							$out2.= ' - ' . $task->getNomUrl(1, 'withproject', 'time', 1) . '<br>';

							// @TODO : add a "?" showing the details of time spent per user
						}
					} else {
						$out2 = $langs->trans('None');
					}

					// Total temp passé
					$out.= '<tr class="trextrafieldseparator trextrafieldseparatortimespent"><td colspan="2">';
					$out.= '<strong>'.img_object('', $object->picto, 'class="paddingright classfortooltip valignmiddle"').'  '.$langs->trans('TicketTimeSpentInformations').'</strong></td></tr>';

					$out.= '<tr>';
					$out.= '<td>'.$langs->trans('TotalTimeSpent').'</td>';
					$out.= '<td>'.$out2.'</td>';
					$out.= '</tr>';

					// Informations générale sur l'assistance du client
					dol_include_once('/cliatm/lib/cliatm_assistance.lib.php');
					$infos = getAssistanceInfosFromProject($object->fk_project);

					$out.= '<tr>';
					$out.= '<td>'.$langs->trans('Conso', date('Y')).'</td>';
					$out.= '<td>'.price($infos['consumedN']).' h / '.price($infos['purchasedN']).' h';
					if($infos['consumedN'] > $infos['purchasedN']) $out.= img_warning();
					$out.= '</td>';
					$out.= '</tr>';
					$out.= '<tr>';
					$out.= '<td>'.$langs->trans('Conso', date('Y') - 1).'</td>';
					$out.= '<td>'.price($infos['consumedNM1']).' h / '.price($infos['purchasedNM1']).' h'.'</td>';
					$out.= '</tr>';
					$out.= '<tr>';
					$out.= '<td>'.$langs->trans('SalesRepresentative').'</td>';
					$out.= '<td>'.(is_object($infos['commercial']) ? $infos['commercial']->getLoginUrl(1) : $infos['commercial']).'</td>';
					$out.= '</tr>';
					$out.= '<tr>';
					$out.= '<td>'.$langs->trans('ProjectManager').'</td>';
					$out.= '<td>'.(is_object($infos['cdp']) ? $infos['cdp']->getLoginUrl(1) : $infos['cdp']).'</td>';
					$out.= '</tr>';
					if(count($infos['retours']) > 0) {
						$out.= '<tr>';
						$out.= '<td>'.$langs->trans('LastDelivery');
						if($object->type_code != 'PROJECT') $out.= img_warning($langs->trans('LastDeliveryHelp'));
						$out.= '</td>';
						$out.= '<td>';

						foreach ($infos['retours'] as $bl) {
							$out.= $bl->getNomUrl(1) . '<br>';
						}
						$out.= '</td>';
						$out.= '</tr>';
					}

					$this->resprints = $out;
				} else {
					dol_print_error($db);
				}
			}
		}

		return 0;
	}

	/**
	 * Overloading the doMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array()         $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    &$object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          &$action        Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	function doMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $db, $langs, $user, $conf, $maxformassaction;
		if(empty($maxformassaction)) $maxformassaction = empty($conf->global->MAIN_LIMIT_FOR_MASS_ACTIONS) ? 1000 : $conf->global->MAIN_LIMIT_FOR_MASS_ACTIONS;
		$langs->load('cliatm@cliatm');
		$error = 0; // Error counter

		if(in_array('agendalist', explode(':', $parameters['context']))) {
			$massaction = $parameters['massaction'];
			$toSelect = $parameters['toselect'];
			$reprogramDateMonth = $parameters['reprogramdate_month'];
			$reprogramDateDay = $parameters['reprogramdate_day'];
			$reprogramDateYear = $parameters['reprogramdate_year'];
			$reprogramup = empty($parameters['reprogramup']) ? 0 : $parameters['reprogramup'];
			$nbToSelect = count($toSelect);

			$reprogramStr = $reprogramDateMonth.$reprogramDateDay.$reprogramDateYear;
			if(! empty($reprogramStr)) $dateReprogram = dol_mktime(0, 0, 0, $reprogramDateMonth, $reprogramDateDay, $reprogramDateYear);

			if(! empty($massaction) && $nbToSelect < 1) {
				$this->errors[] = $langs->trans("NoActionSelected");
				$error++;
			}
			else if($nbToSelect > $maxformassaction) {
				$this->errors[] = $langs->trans('TooManyRecordForMassAction',$maxformassaction);
				$error++;
			}
			else if($massaction == 'confirm_reprogram') {
				$action_tmp = new ActionComm($db);

				$db->begin();
				foreach($toSelect as $toSelectId) {
					$result = $action_tmp->fetch($toSelectId);

					if($result > 0) {
						$action_tmp->fetch_userassigned();
						if(empty($dateReprogram)) $action_tmp->datep = dol_time_plus_duree($action_tmp->datep, $reprogramup, 'd');
						else $action_tmp->datep = $dateReprogram;

						if ($user->rights->agenda->allactions->create || (($user->rights->agenda->myactions->create && $action_tmp->authorid == $user->id || $action_tmp->userownerid == $user->id))) {
							$action_tmp->update($user);
						}
					}
					else {
						setEventMessages($action_tmp->error, $action_tmp->errors, 'errors');
						$error++;
						break;
					}
				}

				if(empty($error)) {
					setEventMessages($langs->trans('ActionsPostponed', $nbToSelect), null);
					$db->commit();
				}
				else $db->rollback();
			}
		}

		if (! $error)
		{
			$this->results = array();
			$this->resprints = '';
			return 0; // or return 1 to replace standard code
		}
		else
		{
			$this->errors[] = '';
			return -1;
		}
	}

	function printFieldListSelect($parameters, &$object, &$action, $hookmanager) {
		$contexts = explode(':',$parameters['context']);
		if(in_array('tasklist',$contexts)) {
			$this->resprints = ', t.description';
		}
	}

	/**
	 * @param mixed[] $parameters
	 * @param CommonObject $object
	 * @param string $action
	 * @param HookManager $hookmanager
	 * @return int
	 */
	public function printFieldListOption($parameters, &$object, &$action, $hookmanager)
	{
		global $langs, $db;
		$TContext = explode(':', $parameters['context']);
		if (in_array('tasklist', $TContext)) {
			$arrayfields = $parameters['arrayfields'];
			$this->resprints = '';

			$fieldName = 't.description';
			if (!empty($arrayfields[$fieldName]['checked'])) {
				$search_value = GETPOST('search_description', 'alpha');
				$searchInput = '<input name="search_description" value="' . dol_escape_htmltag($search_value) . '" />';
				$this->resprints .= '<td class="liste_titre">' . $searchInput .'</td>';
			}
		}
		return 0;
	}

	/**
	 * @param mixed[] $parameters
	 * @param CommonObject $object
	 * @param string $action
	 * @param HookManager $hookmanager
	 * @return int
	 */
	public function printFieldListWhere($parameters, &$object, &$action, $hookmanager)
	{
		global $langs, $db;
		$TContext = explode(':', $parameters['context']);
		if (in_array('tasklist', $TContext)) {
			$search_desc = GETPOST('search_description', 'alpha');
			if(!empty($search_desc)) $this->resprints = natural_search('t.description', $search_desc);
		}
		return 0;
	}

	/**
	 * @param mixed[] $parameters
	 * @param CommonObject $object
	 * @param string $action
	 * @param HookManager $hookmanager
	 * @return int
	 */
	public function printFieldListTitle($parameters, &$object, &$action, $hookmanager)
	{
		global $langs;
		$TContext = explode(':', $parameters['context']);
		if (in_array('tasklist', $TContext)) {
			$arrayfields = $parameters['arrayfields'];
			$sortfield = GETPOST("sortfield", 'alpha');
			$sortorder = GETPOST("sortorder", 'alpha');
			if (empty($sortfield)) $sortfield = 'c.ref';
			if (empty($sortorder)) $sortorder = 'DESC';
			$fieldName = 't.description';
			if ($arrayfields[$fieldName]['checked']) {
				print_liste_field_titre(
					$arrayfields[$fieldName]['label'],
					$_SERVER["PHP_SELF"],
					$fieldName,
					'',
					'',
					'',
					$sortfield,
					$sortorder,
					'');
			}
		}
		return 0;
	}

	/**
	 * @param mixed[] $parameters
	 * @param CommonObject $object
	 * @param string $action
	 * @param HookManager $hookmanager
	 * @return int
	 */
	public function printFieldListValue($parameters, &$object, &$action, $hookmanager)
	{
		global $langs;
		$TContext = explode(':', $parameters['context']);
		if (in_array('tasklist', $TContext)) {

			// $totalarray and $i are not in $parameters.
			// @TODO : remove after PR 13845 is merged
			if (array_key_exists('totalarray', $parameters))
				$totalarray = $parameters['totalarray'];
			else global $totalarray;
			if (array_key_exists('i', $parameters))
				$i = $parameters['i'];
			else global $i;

			$arrayfields = $parameters['arrayfields'];
			$obj = $parameters['obj'];
			$this->resprints = '';

			$fieldName = 't.description';
			if ($arrayfields[$fieldName]['checked']) {
				$this->resprints .= '<td class="tdoverflowmax200">'
					. $obj->description
					. '</td>';

				if (! $i) $totalarray['nbfield']++;
			}
		}
		return 0;
	}

	/**
	 * Affichage en rouge et gras du nom du tiers si impayé en retard
	 */
	function getNomUrl($parameters, &$object, &$action, $hookmanager)
	{
		if($object->element == 'societe') {
			global $langs;
			$langs->load('bills');
			$dataClient = $this->_getDataClient();
			if(isset($dataClient[$object->id])) {

				$dom = new DOMDocument();
				@$dom->loadHTML (mb_convert_encoding($parameters['getnomurl'], 'HTML-ENTITIES', "UTF-8"));
				$links = $dom->getElementsByTagName ( 'a');

				foreach ($links as $link){
					$style = $link->getAttribute('style');
					$style.= 'color: red; font-weight: bold;';
					$link->setAttribute('style', $style);

					$title = $link->getAttribute('title');
					$title.= '<div style="color:red; font-weight: bold;">'. $langs->trans('Unpaid').' : '.price($dataClient[$object->id]['unpaid']).'</div>';
					$link->setAttribute('title', $title);
				}

				$this->resprints = $dom->saveHTML();

				return 1;
			}
		}

		return 0;
	}

	/**
	 * Récupération des données qui permettent de définir si le tiers est à afficher en rouge
	 * - Facture impayée dont la date d'échénace et dépasée
	 * - Tag Pimet TODO ?
	 * - Top 10 client TODO ?
	 */
	function _getDataClient() {
		global $db, $conf, $data;

		if(!empty($data)) return $data;

		$data = array();

		$sql = "SELECT f.fk_soc, SUM(f.total_ttc) - COALESCE(SUM(pf.amount),0) as total_unpaid";
		$sql.= " FROM ".MAIN_DB_PREFIX."facture f";
		$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."paiement_facture pf ON pf.fk_facture = f.rowid";
		$sql.= " WHERE f.fk_statut = 1 AND f.paye = 0";
		$sql.= " AND f.date_lim_reglement < (DATE_FORMAT(NOW() , '%Y-%m-%d')";
		if($conf->global->MAIN_DELAY_CUSTOMER_BILLS_UNPAYED > 0) $sql.= " - INTERVAL ".$conf->global->MAIN_DELAY_CUSTOMER_BILLS_UNPAYED." DAY";
		$sql.= ') GROUP BY fk_soc';

		$resql = $db->query($sql);
		while ($obj = $db->fetch_object($resql)) {
			$data[$obj->fk_soc]['unpaid'] = $obj->total_unpaid;
		}

		return $data;
	}
}
