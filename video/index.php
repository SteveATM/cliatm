<?php require '../config.php';

$context = new stdClass();
$context->meta_title = '';
$context->title = 'Titre de la tâche';
$context->taskRef = 'Txxxx';
$context->date = time();

/**
 * ACTION
 */



/**
 * VIEW
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="ATM">

	<title><?php echo empty($context->meta_title)?$context->title:$context->meta_title;?></title>

	<!-- core CSS -->
	<link href="<?php print dol_buildpath('cliatm/video/asset/css/normalize.css',1); ?>" rel="stylesheet">
	<link href="<?php print dol_buildpath('cliatm/video/asset/css/skeleton.css',1); ?>" rel="stylesheet">
	<link href="<?php print dol_buildpath('cliatm/video/asset/css/presentation.css',1); ?>" rel="stylesheet">

	<!-- FONT -->
<!--	<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">-->
	<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@200;400&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php print DOL_URL_ROOT.'/theme/common/fontawesome-5/css/all.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php print DOL_URL_ROOT.'/theme/common/fontawesome-5/css/v4-shims.min.css'?>">

	<?php
	if (! empty($conf->global->MAIN_FAVICON_URL)){
		$favicon=$conf->global->MAIN_FAVICON_URL;
		print '<link rel="icon" type="image/png" href="'.$favicon.'">'."\n";
	}

	// JQuery. Must be before other includes
	print '<!-- Includes JS for JQuery -->'."\n";
	if (defined('JS_JQUERY') && constant('JS_JQUERY')) print '<script src="'.JS_JQUERY.'jquery.min.js"></script>'."\n";
	else print '<script src="'.DOL_URL_ROOT.'/includes/jquery/js/jquery.min.js"></script>'."\n";

	?>

	<script>

		$( document ).ready(function() {

			getUrlForDemoButton();

			$("#get-url-btn").click(function() {
				getUrlForDemoButton();
			});

			$("#demo-btn").click(function(e) {
				e.preventDefault(); //will stop the link href to call the blog page

				$(this).hide(); // cache le bouton
				$('[contenteditable]').prop('contenteditable', false ); // désactive l'edition dynamique ce qui désactive la vérification orthographique

				// démarre l'animation au bout de 3 sec le temps de lancer la video
				setTimeout(function () {
					$("#bottom-timer").addClass("round-time-bar");
				}, 3000); //will call the function after 3 secs.


				setTimeout(function () {
					window.location.href = $("#demo-btn").attr("href"); //will redirect to your blog page (an ex: blog.html)
				}, 8000); //will call the function after 8 secs.

			});
		});

		function getUrlForDemoButton(){
			demoUrl = window.prompt('Url de la démo ?', $("#demo-btn").attr("href"));
			$("#demo-btn").attr("href",  demoUrl);
		}
	</script>
</head>
<body class="start" >

<div id="top-banner">
	<i class="fa fa-cogs" id="get-url-btn" title="modifier l'url de la démo" ></i>
</div>

<!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
	<div class="row">
		<div class="four column" style="margin-left: 115px; margin-top: 350px">
			<h1 contenteditable="true" ><?php print dol_htmlentities($context->taskRef); ?></h1>
			<h1 contenteditable="true"><?php print dol_htmlentities($context->title); ?></h1>
			<h1>Date de production: <span contenteditable="true"><?php print date('d/m/Y',$context->date); ?></span></h1>
			<p contenteditable="true" ><?php print dol_htmlentities($context->description); ?></p>
		</div>
	</div>
	<div style="text-align: center;" ><a title="Apres le click vous avez 8 secondes pour lancer l'enregistrement." id="demo-btn" class="button button-primary" href="#">START</a></div>
</div>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="bottom-timer" class=""  style="">
	<div></div>
</div>
</body>
</html>
