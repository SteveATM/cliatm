<?php

require 'config.php';
dol_include_once('contrat/class/contrat.class.php');
dol_include_once('webhost/class/webinstance.class.php');
dol_include_once('webhost/class/webmoduleversion.class.php');
if(!dol_include_once('webpassword/class/listviewhelper.class.php')) dol_include_once('webhost/class/listviewhelper.class.php');

if (empty($user->rights->contrat->lire)) accessforbidden();

$langs->load('abricot@abricot');
$langs->load('cliatm@cliatm');

$PDOdb = new TPDOdb;

$status = GETPOST('status');

$hookmanager->initHooks(array('cliatmabolist'));

$action = GETPOST('action', 'alpha');
$confirm = GETPOST('confirm', 'alpha');

$show_files = GETPOST('show_files', 'int');
$toselect = GETPOST('toselect', 'array');
$contextpage = GETPOST('contextpage', 'aZ') ?GETPOST('contextpage', 'aZ') : 'contractlist'; // To manage different context of search


$massaction = GETPOST('massaction', 'alpha');
$confirmmassaction = GETPOST('confirmmassaction', 'alpha');
$toselect = GETPOST('toselect', 'array');
$arrayofselected = is_array($toselect) ? $toselect : array();

if (GETPOST('cancel', 'alpha')) { $action = 'list'; $massaction = ''; }
if (!GETPOST('confirmmassaction', 'alpha') && $massaction != 'presend' && $massaction != 'confirm_presend') { $massaction = ''; }

$diroutputmassaction = $conf->contrat->dir_output.'/temp/massgeneration/'.$user->id;

/*
* Actions
*/
$hookmanager->initHooks(array('atmlistcontractmaintwebhost', 'globallist'));

$parameters = array('id' => $id, 'ref' => $ref, 'mode' => $mode);
$reshook = $hookmanager->executeHooks('doActions', $parameters, $object, $action); // Note that $action and $object may have been modified by some
if ($reshook < 0)
	setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

if (empty($reshook))
{
	$objectclass = 'Contrat';
	$objectlabel = 'Contracts';
	$permissiontoread = $user->rights->contrat->lire;
	$permissiontodelete = $user->rights->contrat->supprimer;
	$uploaddir = $conf->contrat->dir_output;
	include DOL_DOCUMENT_ROOT.'/core/actions_massactions.inc.php';
}

/*
* View
*/

llxHeader('', $langs->trans("AtmListAboHebergement"));



$object = new Contrat($db);
$emailMassActionList = array('predeletedraft', 'predelete', 'presend');
if (in_array($massaction, $emailMassActionList))
{
	// Send email form :  obligé de créer un nouveau
	print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';

	if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
	print '<input type="hidden" name="action" value="list">';
	print '<input type="hidden" name="sortfield" value="'.$sortfield.'">';
	print '<input type="hidden" name="sortorder" value="'.$sortorder.'">';
	print '<input type="hidden" name="page" value="'.$page.'">';
	print '<input type="hidden" name="contextpage" value="'.$contextpage.'">';

	//  presend email
	$topicmail = "SendContractRef";
	$modelmail = "contract";
	$objecttmp = new Contrat($db);
	$trackid = 'con'.$object->id;
	include DOL_DOCUMENT_ROOT.'/core/tpl/massactions_pre.tpl.php';

	if ($massactionbutton || $massaction)   // If we are in select mode (massactionbutton defined) or if we have already selected and sent an action ($massaction) defined
	{
		foreach ($arrayofselected as $selected){
			print '<input id="cb'.$obj->rowid.'" class="flat checkforselect" type="hidden" name="toselect[]" value="'.$selected.'" >';
		}
	}

	print '</form>';
}



$form = new Form($db);

$formcore = new TFormCore($url, 'form_list_contract', 'POST');

$listViewRenderName = 'contract-maint-list';
$list = new ListViewHelper($db, $listViewRenderName);

$sqlFields = ' c.rowid as rowid , ';
$sqlFields .= ' c.rowid as contract , ';
$sqlFields .= ' c.rowid as goto , ';
$sqlFields .= ' winst.rowid as instance , ';
$sqlFields .= ' winst.version as instance_version , ';
$sqlFields .= ' winst.url_admin as url_admin , ';
$sqlFields .= ' winstext.last_minor_version_update as last_minor_version_update , ';
$sqlFields .= ' winstext.last_main_version_update as last_main_version_update , ';
$sqlFields .= ' winstext.vpn_needed as vpn_needed , ';
$sqlFields .= ' cdet.date_ouverture as cdet_date_ouverture, ';
$sqlFields .= ' cdet.date_fin_validite as cdet_date_fin_validite, ';
$sqlFields .= ' c.fk_soc as c_fk_soc, ';
//$sqlFields .= ' sext.dolibarr_version as sext_dolibarr_version, ';
$sqlFields .= ' cext.date_next_maintenance as date_next_maintenance, ';
$sqlFields .= ' cext.workflow_main_maj_launched_date as workflow_main_maj_launched_date, ';
$sqlFields .= ' c.note_private as contract_note_private ';


$sql = 'SELECT  ' . $sqlFields;

$sql .= ' FROM ' . MAIN_DB_PREFIX . 'contratdet as cdet ';
$sql .= ' JOIN ' . MAIN_DB_PREFIX . 'contrat as c ON ( cdet.fk_contrat = c.rowid ) ';
$sql .= ' JOIN ' . MAIN_DB_PREFIX . 'societe as soc ON ( c.fk_soc = soc.rowid) ';
$sql .= ' JOIN ' . MAIN_DB_PREFIX . 'societe_extrafields as sext ON ( c.fk_soc = sext.fk_object) ';

$sql .= ' LEFT JOIN ' . MAIN_DB_PREFIX . 'element_element as elel ON ( (c.rowid = elel.fk_source AND elel.sourcetype = \'contrat\' AND elel.targettype = \'webhost_webinstance\' ) OR (c.rowid = elel.fk_target AND elel.sourcetype = \'webhost_webinstance\' AND elel.targettype = \'contrat\' ) ) ';
$sql .= ' LEFT JOIN ' . MAIN_DB_PREFIX . 'webinstance as winst ON ( (winst.rowid = elel.fk_target AND elel.sourcetype = \'contrat\' AND elel.targettype = \'webhost_webinstance\' AND winst.status = 1 ) OR (c.rowid = elel.fk_source AND elel.sourcetype = \'webhost_webinstance\' AND elel.targettype = \'contrat\' )  ) ';

$sql .= ' LEFT JOIN ' . MAIN_DB_PREFIX . 'webinstance_extrafields as winstext ON ( winstext.fk_object = winst.rowid) ';
$sql .= ' LEFT JOIN ' . MAIN_DB_PREFIX . 'contrat_extrafields as cext ON ( c.rowid = cext.fk_object)  ';
$sql .= ' WHERE (cdet.fk_product = 144 AND cdet.statut = 4)'; // TODO : add form select on list


$sqlGroupFields = ' c.rowid, ';
$sqlGroupFields .= ' winst.rowid , ';
$sqlGroupFields .= ' winst.version , ';
$sqlGroupFields .= ' winst.url_admin , ';
$sqlGroupFields .= ' winstext.last_minor_version_update, ';
$sqlGroupFields .= ' winstext.last_main_version_update, ';
$sqlGroupFields .= ' winstext.vpn_needed , ';
$sqlGroupFields .= ' cdet.date_ouverture, ';
$sqlGroupFields .= ' cdet.date_fin_validite, ';
$sqlGroupFields .= ' c.fk_soc, ';
//$sqlGroupFields .= ' sext.dolibarr_version, ';
$sqlGroupFields .= ' cext.date_next_maintenance, ';
$sqlGroupFields .= ' cext.workflow_main_maj_launched_date, ';
$sqlGroupFields .= ' c.note_private ';

$sql .= ' GROUP BY '.$sqlGroupFields;


$listViewConfig = array(
	'view_type' => 'list', // default = [list], [raw], [chart]
	'limit' => array('nbLine' => 50),
	'subQuery' => array(),
	'allow-fields-select' => true,

	'sortfield' => 'date_next_maintenance',
	'sortorder' => 'DESC',
	'type' => array(
		'cdet_date_fin_validite' => 'date', // [datetime], [hour], [money], [number], [integer]
		'date_next_maintenance' => 'date',
		'last_minor_version_update' => 'date',
		'last_main_version_update' => 'date',
		'workflow_main_maj_launched_date' => 'date',
		'cdet_date_ouverture' => 'date'
	),
	'search' => array(
		'cdet_date_fin_validite' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'date_next_maintenance' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'cdet_date_ouverture' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'instance_version' => array('search_type' => true, 'table' => array('winst', 'winst'), 'field' => array('version')),
		'workflow_main_maj_launched_date' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'last_minor_version_update' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'last_main_version_update' => array('search_type' => 'calendars', 'allow_is_null' => true),
		'contract' => array('search_type' => true, 'table' => array('c', 'c'), 'field' => array('ref')),
		'c_fk_soc' => array('search_type' => true, 'table' => array('soc', 'soc'), 'field' => array('nom')),
	),
	'translate' => array(),
	'list' => array(
		'title' => $langs->trans('ContractMaintList'),
		'messageNothing' => $langs->trans('Nothing'),
		'picto_search' => img_picto('', 'search.png', '', 0),

		// ,'orderUp'=>''
        'massactions'=>array(
                'presend' => $langs->trans("SendByMail")
		),
		'selected' => $toselect
	),
	'title' => array(
		'contract' => $langs->trans('Contract'),
		'c_fk_soc' => $langs->trans('Customer'),
		'instance_ref'  => $langs->trans('WebInstance'),
		'instance_version' => $langs->trans('Version'),
		'instance_online' => $langs->trans('Online'),
		'cdet_date_ouverture' => $langs->trans('DateOuverture'),
//		'cdet_date_fin_validite' => $langs->trans('DateServiceEnd'),
		'last_minor_version_update' => $langs->trans('DateLastMinUpdate'),
		'last_main_version_update' => $langs->trans('DateLastMainUpdate'),
		'date_next_maintenance' => $langs->trans('DateNextMaintenance'),
		'workflow_main_maj_launched_date' => $langs->trans('DateWorkflowMainMajLaunchedDate'),
		'url_admin' => $langs->trans('URL'),
		'vpn_needed' => $langs->trans('VPN'),
		'contract_note_private' => $langs->trans('NotesPrivatesContract'),
		'goto' => $langs->trans('Goto')
//        ,'selectedfields' => '' // active for mass action
	),
	'eval' => array(
		'c_fk_soc' => '_getSocUrl(\'@val@\')',
		'contract' => '_getContractUrl(\'@val@\')',
		'goto' => '_getGotoField(\'@val@\')',
		'instance_ref' => get_class($list).'::evalGetObjectOutputField(\'WebInstance\', \'ref\', \'@instance@\', \'@val@\')',
		'url_admin' => get_class($list).'::evalGetObjectOutputField(\'WebInstance\', \'url_admin\', \'@instance@\', \'@val@\')',
		'date_next_maintenance' => '_date_next_maintenance(\'@val@\', \'@contract@\', \'@last_minor_version_update@\')',
		'vpn_needed' => '_VPN(\'@val@\')',

		'last_main_version_update' => '_date_last_main_maj(\'@val@\',  \'@contract@\', \'@workflow_main_maj_launched_date@\', \'@cdet_date_ouverture@\')',
		'instance_online' => get_class($list).'::evalGetObjectOutputField(\'WebInstance\', \'last_monitor_status\', \'@instance@\', \'@val@\')',
	)
);

if($sortfield == 'winst.version'){
	$list['sortOrderOverride'] = WebModuleVersion::sqlVersionCompareQuery('winst.version',false);
}




// Change view from hooks
$parameters = array('listViewConfig' => $listViewConfig);

$reshook = $hookmanager->executeHooks('listViewConfig', $parameters, $list);    // Note that $action and $object may have been modified by hook
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
if ($reshook > 0) {
	$listViewConfig = $hookmanager->resArray;
}

if (!in_array($massaction, $emailMassActionList)) {
	// Render list
	echo $list->render($sql, $listViewConfig);
}


$parameters = array('sql' => $sql);
$reshook = $hookmanager->executeHooks('printFieldListFooter', $parameters, $object);    // Note that $action and $object may have been modified by hook
print $hookmanager->resPrint;
$formcore->end_form();


print '</div>';
llxFooter();
$db->close();


/*
 * PSEUDO LIB
 */

function _getSocUrl($id) {
	global $db;

	$obj = new Societe($db);
	$obj->fetch($id);

	return $obj->getNomUrl(1);

}

/**
 * @param $id
 * @return bool|Contrat
 */
function _getContract($id) {
	global $db, $contractCache;

	$id = intval($id);

	if(!empty($contractCache[$id])){
		return $contractCache[$id];
	}

	if(!empty($id)){
		$obj = new Contrat($db);
		$res = $obj->fetch($id);
		if($res>0) {
			return $obj;
		}
	}

	return false;
}


function _getContractUrl($id) {
	global $db, $contractCache;

	$obj = _getContract($id);

	if($obj !== false){
		return $obj->getNomUrl(1);
	}else{
		return '';
	}
}

function _date_next_maintenance($time = '', $id = '', $last_minor_version_update = '') {
	global $db, $contractCache;

	$obj = _getContract($id);

	$url = DOL_URL_ROOT.'/contrat/card.php?id='.$id.'&action=edit_extras&attribute=date_next_maintenance&ignorecollapsesetup=1';

	$time = intval($db->jdate($time));
	$date = date("d/m/Y", $time);


	$nextMaint = new DateTime();
	$nextMaint->setTimestamp($time);
	$now = new DateTime();
	$interval = $now->diff($nextMaint);
	$intervalDay = intval($interval->format('%r%a'));


	if(!empty($last_minor_version_update)){
		$last_minor_version_update = $db->jdate($last_minor_version_update);
		$lastMaint = new DateTime();
		$lastMaint->setTimestamp($last_minor_version_update);
		$lastMaintInterval = $lastMaint->diff($nextMaint);
		$lastMaintIntervalDay = intval($lastMaintInterval->format('%r%a'));
	}

	if($obj !== false){
		$output = $date;

		if($intervalDay < 0 && (empty($last_minor_version_update) || $lastMaintIntervalDay < 0)){
			$output = '<span class="badge badge-warning"><i class="fa fa-exclamation-triangle" ></i> '.$output.'</span>';
		}

		$output.= ' <a target="_blank" href="'.$url.'" ><i class="fa fa-edit"></i></a>';

		return $output;
	}else{
		return $time;
	}
}

function _date_last_main_maj($lastMainUpdate = '', $id = '', $workflow_launch_update = '', $date_ouverture_contrat = '') {
	global $db, $langs;

	$day = 60*60*24;
	$year = $day * 365;
	$dateOffset = time() - $year + 90*$day; // ok j'ai eu la flemme de faire des maths

	$lastMainUpdate = $dateToCompare = intval($db->jdate($lastMainUpdate));
	$date = date("d/m/Y", $lastMainUpdate);

	$date_ouverture_contrat = intval($db->jdate($date_ouverture_contrat));
	$workflow_launch_update = intval($db->jdate($workflow_launch_update));

	// si la maj majeur n'a jamais été faite alors a defaut il s'agit de la date de démarage du service
	if(empty($dateToCompare)){
		$dateToCompare = $date_ouverture_contrat;
	}


	if($dateToCompare < $dateOffset){
		if($workflow_launch_update < $dateOffset) {
			$tooltype = $langs->trans('MainMajMaintToDo');
			return '<span class="badge badge-warning classfortooltip" title="'.$tooltype.'"><i class="fa fa-exclamation-triangle" ></i> ' . (!empty($lastMainUpdate)?$date:'') . '</span>';
		}
		else{
			$tooltype = $langs->trans('MainMajMaintLaunchedAt', !empty($workflow_launch_update)?date("d/m/Y", $workflow_launch_update):'');
			return '<span class="badge badge-primary classfortooltip" title="'.$tooltype.'"><i class="fa fa-exclamation-triangle" ></i> ' . (!empty($lastMainUpdate)?$date:'') . '</span>';
		}
	}

	return !empty($lastMainUpdate)?$date:'';
}


function _outLink($url = '')
{
	if (filter_var($url, FILTER_VALIDATE_URL)) {
		return '<a target="_blank" href="'.$url.'" >'.$url.'</a>';
	}

	return '';
}

function _getGotoField($contractId = '')
{
	global $conf;

	if(empty($contractId)){
		return '';
	}

	$contractId = intval($contractId);

	$contactCodeForMaintenance = $conf->global->CLIATM_CONTACT_CODE_FOR_MAINTENANCE;
	if(empty($contactCodeForMaintenance )){
		$contactCodeForMaintenance = 'TECHNIC';
	}

	$url = DOL_MAIN_URL_ROOT . '/contrat/card.php?id='.$contractId.'&action=presend&mode=init&modelmailselected=50';

	// autofill contact
	$contract = _getContract($contractId);
	if($contract){
		$TContactId = $contract->getIdContact('external', $contactCodeForMaintenance);
		if(!empty($TContactId)){
			foreach ($TContactId as $contactId){
				$url.= '&receiver[]='.$contactId;
			}
		}
		else{
			// if no contact for contract try from customer
			if(empty($contract)){
				$contract->thirdparty->fetch_thirdparty();
			}
			if(!empty($contract->thirdparty)){
				$TContactId = $contract->thirdparty->getIdContact('external', $contactCodeForMaintenance);
				if(!empty($TContactId)){
					foreach ($TContactId as $contactId){
						$url.= '&receiver[]='.$contactId;
					}
				}
			}
		}
	}

	return '<a target="_blank" href="'.$url.'#formmailbeforetitle" ><i class="fa fa-envelope-o"></i></a>';

}

function _VPN($id=0){
	global $langs;

	$array = array(0=>'', 1=>$langs->trans('Yes') );
	if(!empty($array[$id])) return $array[$id];
	return $array[0];
}
