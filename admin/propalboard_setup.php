<?php
/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015 ATM Consulting <support@atm-consulting.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file		admin/cliatm.php
 * 	\ingroup	cliatm
 * 	\brief		This file is an example module setup page
 * 				Put some comments here
 */
// Dolibarr environment
$res = @include("../../main.inc.php"); // From htdocs directory
if (! $res) {
    $res = @include("../../../main.inc.php"); // From "custom" directory
}

// Libraries
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once '../lib/cliatm.lib.php';

// Translations
$langs->load("cliatm@cliatm");

// Access control
if (! $user->admin) {
    accessforbidden();
}

// Parameters
$action = GETPOST('action', 'alpha');

/*
 * Actions
 */
if (preg_match('/set_(.*)/',$action,$reg))
{
	$code=$reg[1];
	if (dolibarr_set_const($db, $code, GETPOST($code), 'chaine', 0, '', $conf->entity) > 0)
	{
		header("Location: ".$_SERVER["PHP_SELF"]);
		exit;
	}
	else
	{
		dol_print_error($db);
	}
}

if (preg_match('/del_(.*)/',$action,$reg))
{
	$code=$reg[1];
	if (dolibarr_del_const($db, $code, 0) > 0)
	{
		Header("Location: ".$_SERVER["PHP_SELF"]);
		exit;
	}
	else
	{
		dol_print_error($db);
	}
}

if (empty($conf->global->MAIN_DURATION_OF_WORKDAY)) { 	
	setEventMessages('Please set MAIN_DURATION_OF_WORKDAY param', array(), 'errors'); 
}
if (! empty($conf->global->MAIN_DURATION_OF_WORKDAY) && $conf->global->MAIN_DURATION_OF_WORKDAY == '86400') {
	setEventMessages('Are you really working H24? Please check MAIN_DURATION_OF_WORKDAY parameter', array(), 'warnings');
}

/*
 * View
 */
$page_name = "CliATMSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . DOL_URL_ROOT . '/admin/modules.php">'
    . $langs->trans("BackToModuleList") . '</a>';
print_fiche_titre($langs->trans($page_name), $linkback);

// Configuration header
$head = cliatmAdminPrepareHead();
dol_fiche_head(
    $head,
    'propalboard',
    $langs->trans("Module104990Name"),
    0,
    "cliatm@cliatm"
);

/*
 * - sélection de la catégorie pour 'développements'
 - sélection de la catégorie pour 'direction de projet'
 - sélection de la catégorie pour 'accompagnement'
 - ratio direction (1JH pour *X*JH dev)
 - Seuil montant
 - ratio accompagnement si devis > *seuil*
 - ratio accompagnement normal (1JH pour *X*JH accompagnement)
 - facteur multiplicateur pour la durée de réalisation
 */

// Setup page goes here
$form=new Form($db);
$var=false;
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("Parameters").'</td>'."\n";
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="center" width="100">'.$langs->trans("Value").'</td>'."\n";


// Catégorie développement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_CATEG_DEV").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_CATEG_DEV">';
print $form->select_all_categories('product', $conf->global->ATM_PRBOARD_CATEG_DEV, 'ATM_PRBOARD_CATEG_DEV');
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Catégorie direction de projet
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_CATEG_PROJECT").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_CATEG_PROJECT">';
print $form->select_all_categories('product', $conf->global->ATM_PRBOARD_CATEG_PROJECT, 'ATM_PRBOARD_CATEG_PROJECT');
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Catégorie accompagnement
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_CATEG_ACCOMPA").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_CATEG_ACCOMPA">';
print $form->select_all_categories('product', $conf->global->ATM_PRBOARD_CATEG_ACCOMPA, 'ATM_PRBOARD_CATEG_ACCOMPA');
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

foreach(array('PROVISION', 'MAINTENANCE') as $categ) {
    $confName = 'ATM_PRBOARD_SERVICE_'.$categ;

    $var=!$var;
    print '<tr '.$bc[$var].'>';
    print '<td>'.$langs->trans("Param$confName").'</td>';
    print '<td align="center" width="20">&nbsp;</td>';
    print '<td align="right" width="300">';
    print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
    print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
    print '<input type="hidden" name="action" value="set_'.$confName.'">';
    print $form->select_produits($conf->global->{$confName}, $confName, 1, 20, 0, -1, 2, '', 1);
    print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
    print '</form>';
    print '</td></tr>';
}

// Base jour dev pour ratio
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_RATIO_BASE_DEV").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_RATIO_BASE_DEV">';
print '<input type="text" class="flat" name="ATM_PRBOARD_RATIO_BASE_DEV" size="3" value="'.$conf->global->ATM_PRBOARD_RATIO_BASE_DEV.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Ratio direction projet
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_RATIO_DIRECTION").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_RATIO_DIRECTION">';
print '<input type="text" class="flat" name="ATM_PRBOARD_RATIO_DIRECTION" size="3" value="'.$conf->global->ATM_PRBOARD_RATIO_DIRECTION.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Seuil montant
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_AMOUNT_GAP").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_AMOUNT_GAP">';
print '<input type="text" class="flat" name="ATM_PRBOARD_AMOUNT_GAP" size="3" value="'.$conf->global->ATM_PRBOARD_AMOUNT_GAP.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// ratio accompagnement si devis > *seuil*
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_RATIO_ACCOMP_GAP").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_RATIO_ACCOMP">';
print '<input type="text" class="flat" name="ATM_PRBOARD_RATIO_ACCOMP" size="3" value="'.$conf->global->ATM_PRBOARD_RATIO_ACCOMP.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// ratio accompagnement normal (1JH pour *X*JH accompagnement)
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_RATIO_ACCOMP_NORM").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_RATIO_ACCOMP_NORM">';
print '<input type="text" class="flat" name="ATM_PRBOARD_RATIO_ACCOMP_NORM" size="3" value="'.$conf->global->ATM_PRBOARD_RATIO_ACCOMP_NORM.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

foreach(array('PROVISION', 'MAINTENANCE') as $categ) {
    $confName = 'ATM_PRBOARD_RATIO_'.$categ;

    $var=!$var;
    print '<tr '.$bc[$var].'>';
    print '<td>'.$langs->trans("Param$confName").'</td>';
    print '<td align="center" width="20">&nbsp;</td>';
    print '<td align="right" width="300">';
    print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
    print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
    print '<input type="hidden" name="action" value="set_'.$confName.'">';
    print '<input type="text" class="flat" name="'.$confName.'" size="3" value="'.$conf->global->{$confName}.'" />%';
    print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
    print '</form>';
    print '</td></tr>';
}

// facteur multiplicateur pour la durée de réalisation
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamATM_PRBOARD_FACTOR_DURATION").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_ATM_PRBOARD_FACTOR_DURATION">';
print '<input type="text" class="flat" name="ATM_PRBOARD_FACTOR_DURATION" size="3" value="'.$conf->global->ATM_PRBOARD_FACTOR_DURATION.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

print '</table>';

llxFooter();

$db->close();
