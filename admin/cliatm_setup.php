<?php
/* <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015 ATM Consulting <support@atm-consulting.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file		admin/cliatm.php
 * 	\ingroup	cliatm
 * 	\brief		This file is an example module setup page
 * 				Put some comments here
 */
// Dolibarr environment
$res = @include("../../main.inc.php"); // From htdocs directory
if (! $res) {
    $res = @include("../../../main.inc.php"); // From "custom" directory
}

// Libraries
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once '../lib/cliatm.lib.php';
require_once '../class/cliatm.class.php';
dol_include_once('abricot/includes/lib/admin.lib.php');

// Translations
$langs->load("cliatm@cliatm");

// Access control
if (! $user->admin) {
    accessforbidden();
}

// Parameters
$action = GETPOST('action', 'alpha');

$PDOdb = new TPDOdb;
$orderType = new TOrderType;
$TOrderType = $orderType->LoadAllBy($PDOdb);
/*
 * Actions
 */
if (preg_match('/set_(.*)/',$action,$reg))
{
	$code=$reg[1];
	$value = GETPOST($code);
	if(is_array($value)) $value = implode(',', $value);
//	if(in_array($code, $TOrderType)) $value = implode(',', GETPOST($code));
	if (dolibarr_set_const($db, $code, $value, 'chaine', 0, '', $conf->entity) > 0)
	{
		header("Location: ".$_SERVER["PHP_SELF"]);
		exit;
	}
	else
	{
		dol_print_error($db);
	}
}

if (preg_match('/del_(.*)/',$action,$reg))
{
	$code=$reg[1];
	if (dolibarr_del_const($db, $code, 0) > 0)
	{
		Header("Location: ".$_SERVER["PHP_SELF"]);
		exit;
	}
	else
	{
		dol_print_error($db);
	}
}

/*
 * View
 */
$page_name = "CliATMSetup";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . DOL_URL_ROOT . '/admin/modules.php">'
    . $langs->trans("BackToModuleList") . '</a>';
print load_fiche_titre($langs->trans($page_name), $linkback);

// Configuration header
$head = cliatmAdminPrepareHead();
$notab = -1;
dol_fiche_head(
    $head,
    'settings',
    $langs->trans("Module104990Name"),
	$notab,
    "cliatm@cliatm"
);

dol_fiche_end($notab);

// Setup page goes here
$form=new Form($db);
$var=false;
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("Parameters").'</td>'."\n";
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="center" width="100">'.$langs->trans("Value").'</td>'."\n";


// Percentage for initial deposit
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamMAIN_DEPOSIT_PERCENTAGE_INITIAL").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_MAIN_DEPOSIT_PERCENTAGE_INITIAL">';
print '<input type="text" class="flat" name="MAIN_DEPOSIT_PERCENTAGE_INITIAL" size="3" value="'.$conf->global->MAIN_DEPOSIT_PERCENTAGE_INITIAL.'" />%';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Amount used to calculate deposit
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamMAIN_AMOUNT_CALCULATE_DATE_LIVRAISON").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON">';
print '<input type="text" class="flat" name="MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON" size="3" value="'.$conf->global->MAIN_AMOUNT_CALCULATE_DATE_LIVRAISON.'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Group for prject managers
$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamMAIN_ATM_PROJECT_MANAGER_GROUP_ID").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_MAIN_ATM_PROJECT_MANAGER_GROUP_ID">';
print $form->select_dolgroups($conf->global->MAIN_ATM_PROJECT_MANAGER_GROUP_ID, 'MAIN_ATM_PROJECT_MANAGER_GROUP_ID');
//print '<input type="text" class="flat" name="MAIN_ATM_PROJECT_MANAGER_GROUP_ID" size="3" value="'..'" />';
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';

// Example with a yes / no select
/*$var=!$var;
print '<tr '.$bc[$var].'>';
print '<td>'.$langs->trans("ParamLabel").'</td>';
print '<td align="center" width="20">&nbsp;</td>';
print '<td align="right" width="300">';
print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="set_CONSTNAME">';
print $form->selectyesno("CONSTNAME",$conf->global->CONSTNAME,1);
print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
print '</form>';
print '</td></tr>';
*/

foreach($TOrderType as $type) {
	$var=!$var;

	$conf_name = 'MAIN_ORDER_CATEG_'.strtoupper($type->code);

	$TCategories = $form->select_all_categories('product', '', $conf_name, 64, 0, 1);
	$TConfValue = explode(',', $conf->global->{$conf_name});

	print '<tr '.$bc[$var].'>';
	print '<td>'.$langs->trans("Param".$conf_name).'</td>';
	print '<td align="center" width="20">&nbsp;</td>';
	print '<td align="right" width="300">';
	print '<form method="POST" action="'.$_SERVER['PHP_SELF'].'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="action" value="set_'.$conf_name.'">';
	print $form->multiselectarray($conf_name, $TCategories, $TConfValue);
	print '<input type="submit" class="button" value="'.$langs->trans("Modify").'">';
	print '</form>';
	print '</td></tr>';
}

// **************************
// CONFIGURATION WEBHOST   **
// **************************
setup_print_title('OptionForWebHost');

dol_include_once('/cliatm/class/webModuleStoreUpdate.class.php');

setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_DOMAIN');
setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_LOGIN');
setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_PASSWORD');
setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_PORT');
setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_PATH');
setup_print_on_off('WEBMOD_BLACKMIRROR_FTP_USE_PASSIVE');

$TProtocol = WebModuleStoreUpdate::availableFtpProtocols();
$type = $form->selectArray('WEBMOD_BLACKMIRROR_FTP_PROTOCOL', $TProtocol, $conf->global->WEBMOD_BLACKMIRROR_FTP_PROTOCOL, 0, 0, 1);

$desc = '';
if(!function_exists('ssh2_connect')){
	// if ssh2_connect missing, install libssh2-php : sudo apt-get install libssh2-php
	$desc.= $langs->trans('libssh2PhpIsMissing');
}

setup_print_input_form_part('WEBMOD_BLACKMIRROR_FTP_PROTOCOL', false, $desc, array(), $type);


// **************************
// CONFIGURATION WEBHOST   **
// **************************
setup_print_title('OptionForMaintenance');

setup_print_input_form_part('CLIATM_CONTACT_CODE_FOR_MAINTENANCE');

// **************************
// CONFIGURATION CRON      **
// **************************

setup_print_title($langs->trans('CRON_workflow_facture_acompte'));

$confkey = "CLIATM_DEPOSIT_MAIL_MODEL";
$select = selectModelMail($confkey, 'facture_send');
setup_print_input_form_part($confkey, $langs->trans($confkey), $confkey . '_desc', '', $select);

// **************************
// CONFIGURATION CRON      **
// **************************

setup_print_title($langs->trans('CRON_workflow_contrat'));


// @TODO remplacer ce SQL par ReferenceLetters->fetchAll()??
$confkey = 'CLIATM_CRON_DEFAULT_DOCEDIT_TEMPLATE_FOR_CONTRACT';
$sql = /** @lang SQL */
	'SELECT docedittemplate.rowid, docedittemplate.title'
	. ' FROM ' . MAIN_DB_PREFIX . 'referenceletters as docedittemplate'
	. ' WHERE docedittemplate.entity IN (' . getEntity('referenceletters') . ')'
	. '  AND docedittemplate.status = 1'
	//. '  AND docedittemplate.element_type = "' . $this->db->escape(Contrat::element) . '"'
	. '  AND docedittemplate.element_type = "contract"';
$resql = $db->query($sql);
if ($resql && $db->num_rows($resql)) {
	$selected = !empty($conf->global->{$confkey}) ? $conf->global->{$confkey} : '';
	$select_docedit_tpl = '<select id="' . $confkey . '" name="' . $confkey . '">';
	$select_docedit_tpl .= '<option value=""' . (empty($selected) ? ' selected ' : '') . '> — </option>';
	for ($i = 0; $i < $db->num_rows($resql); $i++) {
		if ($obj = $db->fetch_object($resql)) {
			$select_docedit_tpl .= '<option value="' . $obj->rowid . '"'
								   . ($selected == $obj->rowid ? ' selected ' : '')
								   . '>'
								   . $obj->title
								   . '</option>';
		}
		$test = $select_docedit_tpl;
	}
	$select_docedit_tpl .= '</select>';
	setup_print_input_form_part(
		$confkey,
		$langs->trans($confkey),
		$confkey . '_desc',
		'',
		$select_docedit_tpl
	);
}

$confkey = "CLIATM_CONTRACT_MAIL_MODEL";
$select = selectModelMail($confkey, 'contract');
setup_print_input_form_part($confkey, $langs->trans($confkey), $confkey . '_desc', '', $select);


// **************************
// CONFIGURATION TRIGGERS  **
// **************************

setup_print_title($langs->trans('Triggers_ATM'));

$confkey = 'CLIATM_CONTACT_CODES_REQUIRED_FOR_PROPOSAL_SIGNATURE';

include_once DOL_DOCUMENT_ROOT . 'contact/class/contact.class.php';
$contactStatic = new Contact($db);
$contactTypes = $contactStatic->listeTypeContacts('external', '', 1);
$selectContactTypes = $form->multiselectarray($confkey, $contactTypes, getConfArray($confkey));
setup_print_input_form_part($confkey, $langs->trans($confkey), $confkey . '_desc', '', $selectContactTypes);


print '</table>';

print '<script type="text/javascript">'
	. '		$(document).ready(function() {'
	. '			$("select[multiple]").select2("destroy").select2({ width: "element" });'
	. '		});'
	. '</script>';


llxFooter();

$db->close();
