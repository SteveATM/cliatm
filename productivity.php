<?php

require 'config.php';

dol_include_once("/sig/filtres.php");
dol_include_once("/sig/lib/sig.lib.php");
dol_include_once("/sig/class/compte_de_resultat.class.php");

$TTotal=array('nb_month'=>0,'nb_day'=>0,'facturable'=>0);
$year = GETPOST('year','int');
$use_session_cache = false;
if(empty($year)) {
	$year = date('Y');
	$use_session_cache = true;
}

if(GETPOST('filterdate')!='') {
	$tFiscal = strtotime(GETPOST('t_fiscal_startyear').'-'.GETPOST('t_fiscal_startmonth').'-'.GETPOST('t_fiscal_startday'));
	$tFiscalFin = strtotime(GETPOST('t_fiscal_endyear').'-'.GETPOST('t_fiscal_endmonth').'-'.(GETPOST('t_fiscal_endday')) );
}
else {
	$tFiscal = strtotime(date($year.'-'.$conf->global->SOCIETE_FISCAL_MONTH_START.'-01'));
	$tFiscalFin = strtotime(date(($year+1).'-'.$conf->global->SOCIETE_FISCAL_MONTH_START.'-01') . ' -1day');
}

if($tFiscalFin > time()) $tFiscalFin = strtotime('last day of this month');


// Données de productivités => user du groupe "Technique"
$TUserDev = get_data_salary($tFiscal, $tFiscalFin, array(9));
$TUserCDP = get_data_salary($tFiscal, $tFiscalFin, array(8));

$TUserProd = array_merge($TUserDev, $TUserCDP);

// Données de masse salariale => user du groupe "Commercial"
$TUserComm = get_data_salary($tFiscal, $tFiscalFin, array(2));

$TGroup = array(
	'Technique' => $TUserProd
	,'Commercial' => $TUserComm
);

// Données provenant du compte de résultat (CA et nb jours facturés)
$PDOdb=new TPDOdb;
$TResultat=array();

_rapport_ca_fetch_data($PDOdb, $TResultat, $tFiscal, $tFiscalFin);

$time = 0;
$price = 0;
$catotal= 0;
foreach($TResultat['Projet'] as &$data) {
	$price+=$data['price'];
	$time+=$data['time'];
}

foreach($TResultat as &$categ) {
	foreach($categ as &$data) {
		$catotal+=$data['price'];
	}
}

llxHeader();

$show_presence_day  = false;
if(!empty($conf->absence->enabled)) {
	dol_include_once('/absence/class/absence.class.php');
	$show_presence_day = true;

}
echo '<center>';
echo $langs->trans('AnalysisFromTo', date('d/m/Y', $tFiscal), date('d/m/Y', $tFiscalFin));
echo '</center>';

$formhtml = '<form action="'.$_SERVER['REQUEST_URI'].'">';

$formhtml.= '<select name="year" onchange="this.form.submit()">';
for ($i=2012; $i <= date('Y'); $i++) {
	$sel = ($i == $year) ? ' selected="selected"' : '';
	$formhtml.= '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
}
$formhtml.= '</select> '.$langs->trans('Or').' ';

$formhtml.=$form->select_date($tFiscal,'t_fiscal_start',0,0,0,'',1,0,1);
$formhtml.=$form->select_date($tFiscalFin,'t_fiscal_end',0,0,0,'',1,0,1);
$formhtml.='<input type="submit" value="Ok" name="filterdate" />';

$formhtml.='</form>';

// Productivité
print load_fiche_titre($langs->trans('Productivity'),$formhtml);

?>
<style type="text/css">
table.liste td {
	border-right:1px solid #ddd;
}
</style>
<?php

echo '<table class="liste">';
echo '<tr>
<th>'.$langs->trans('User').'</th>
<th>'.$langs->trans('DateEmployment').'</th>
<!--<th>'.$langs->trans('NbMonthSinceStart').'</th>-->
<th>'.$langs->trans('NbWorkingDayPerMonth').'</th>
<th>'.$langs->trans('NbMonthFiscalYear').'</th>
<th>'.$langs->trans('NBTimeClientEnterWorkingFiscalYear').'</th>
<th>'.$langs->trans('NBTimeSupportEnterWorkingFiscalYear').'</th>
<th>'.$langs->trans('NBTimeRetDEnterWorkingFiscalYear').'</th>
<th>'.$langs->trans('NBTimeOtherEnterWorkingFiscalYear').'</th>
<th>'.$langs->trans('NBTimeTotalEnterWorkingFiscalYear').'</th>
<th>'.$langs->trans('NbWorkingDayPerMonthFiscalYear').'</th>
<th>'.$langs->trans('NbBilledDays').'</th>';

if ($show_presence_day) {
	echo '<th>'.$langs->trans('PresenceDays').'</th>';
	echo '<th colspan=2>'.$langs->trans('HiddenDays').'</th>';
}

echo '<th colspan="3">'.$langs->trans('Productivity').img_help(1,$langs->trans('OnPreviAndOnRealTime')).'</th>
</tr>';



$colspan = 14;

if($show_presence_day)
{
	$colspan+= 3;
}



echo '<tr><td colspan="' . $colspan . '"><b>DÉVELOPPEURS</b></td></tr>';
print_productivity_data_for_group($TUserDev, $TTotal, $tFiscal, $tFiscalFin, $show_presence_day, $use_session_cache, $time);

echo '<tr><td colspan="' . $colspan . '"><b>CHEFS DE PROJET</b></td></tr>';
print_productivity_data_for_group($TUserCDP, $TTotal, $tFiscal, $tFiscalFin, $show_presence_day, $use_session_cache, $time);

echo '<tr><td colspan="' . $colspan . '"><b>TOTAL</b></td></tr>';

echo '<tr class="liste_total">';
echo '<td colspan="2">&nbsp;</td>';
echo '<td class="right">' . price($TTotal['nb_day_capacity'], 0, '', 1, -1, 0) . '</td>';
echo '<td class="right">&nbsp;</td>';
echo '<td class="right">' . price($TTotal['nb_day_client'], 0, '', 1, -1, 0) . '</td>';
echo '<td class="right">&nbsp;</td>';
echo '<td class="right">' . price($TTotal['nb_day_retd'], 0, '', 1, -1, 0) . '</td>';

echo '<td colspan="2" class="right">' . $langs->trans('NbDaysBillableTotal') . '</td>';
echo '<td class="right">' . price($TTotal['nb_day'], 0, '', 1, -1, 0) . '</td>';
echo '<td class="right">' . price($TTotal['nb_day_billed'], 0, '', 1, -1, 0) . '</td>';
if ($show_presence_day) echo '<td colspan="3">&nbsp;</td>';

echo '<td></td>';
echo '<td class="right">' . price(100* $TTotal['nb_day_billed'] / $TTotal['nb_day_client'], 0, '', 1, -1, 0) . '%</td>';
echo '<td></td>';
echo '</tr>';


echo '<tr class="liste_total">';
echo '<td colspan="9" class="right">' . $langs->trans('NbDaysBilledTotal') . '</td>';
echo '<td class="right">' . price($time, 0, '', 1, -1, 0) . '</td>';
echo '<td class="right">' . price($time - $TTotal['nb_day'], 0, '', 1, -1, 0) . ' (' . price(100 * $time / $TTotal['nb_day'], 0, '', 1, -1, 0) . ' %)</td>';
echo '</tr>';


function myprice($amount)
{
    return price(floatval($amount), 0, '', 1, -1, 1);
}


function print_productivity_data_for_group($TUserGroup, &$TTotal, $tFiscal, $tFiscalFin, $show_presence_day, $use_session_cache, $time)
{
    global $db, $PDOdb, $langs;

    $TTotalGroup = array();

	foreach ($TUserGroup as &$u) {

		echo '<tr class="oddeven">';

		echo '<td>' . $u->getNomUrl(1) . '</td>';
		echo '<td class="center">' . dol_print_date($u->dateemployment);
		if (!empty($u->array_options['options_date_sortie'])) {
			echo ' - ' . dol_print_date($u->array_options['options_date_sortie']);
		}
		echo '</td>';

		// echo '<td class="right">' . price($u->nb_month, 0, '', 1, 0) . '</td>';
		echo '<td class="right">' . price($u->array_options['options_nb_working_day']) . '</td>';
		echo '<td class="right">' . price($u->nb_month_exercice_pond, 0, '', 1, 0) . '</td>';

		// Time
		$TTimes = _get_times_enter($u, $tFiscal, $tFiscalFin);
		echo '<td class="right">' . myprice($TTimes['client']['workingdays']) . '</td>';
		echo '<td class="right">' . myprice($TTimes['support']['workingdays']) . '</td>';
		echo '<td class="right">' . myprice($TTimes['retd']['workingdays']) . '</td>';
		echo '<td class="right">' . myprice($TTimes['other']['workingdays']) . '</td>';
		echo '<td class="right">' . myprice($TTimes['total']['workingdays']) . '</td>';

		// Days working
		echo '<td class="right">' . myprice($u->nb_working_days_exercice) . '</td>';
		echo '<td class="right">' . myprice($u->nb_billed_days_exercice) . '</td>';
		if ($show_presence_day) {
			$nb_presence_day = 0;

			if (isset($_SESSION['nb_presence_day_' . $u->id]) && GETPOST('without-session') == '' && $use_session_cache) {
				$nb_presence_day = $_SESSION['nb_presence_day_' . $u->id];
			} else {

				$t_start_absence = $tFiscal;
				$t_end_absence = $tFiscalFin;
				if (!empty($u->dateemployment) && $t_start_absence < $u->dateemployment) $t_start_absence = $u->dateemployment;
				if (!empty($u->array_options['options_date_sortie']) && $t_end_absence > $u->array_options['options_date_sortie']) {
					$t_end_absence = strtotime($u->array_options['options_date_sortie']);
				}

				$t_cur = $t_start_absence;

				$absence = new TRH_absence;
				$absence->fk_user = $u->id;
				while ($t_cur <= $t_end_absence) {

					//list($isWorkingDay, $isNotFerie, $isNotAbsence) = $absence->isWorkingDay($PDOdb, date('Y-m-d', $t_cur));
					list($isWorkingDay, $isNotFerie, $isNotAbsence) = _isWorkingDay($PDOdb, date('Y-m-d', $t_cur), $absence);
					if ($isWorkingDay && $isNotFerie && $isNotAbsence) $nb_presence_day++;
					$t_cur = strtotime('+1day', $t_cur);
				}

				$_SESSION['nb_presence_day_' . $u->id] = $nb_presence_day;
			}

			echo '<td class="right">' . myprice($nb_presence_day)/*. dol_print_date($t_start_absence).' - '.dol_print_date($t_end_absence)*/ . '</td>';
			echo '<td class="right">' . myprice($nb_presence_day - $TTimes['total']['workingdays']) . '</td>';
			echo '<td class="right">' . round(($nb_presence_day - $TTimes['total']['workingdays']) / $nb_presence_day * 100) . '%</td>';
			//.' '.dol_print_date($t_start_absence).'>'.dol_print_date($t_end_absence)
		}


		$productivity_presence = round(($u->nb_billed_days_exercice + $TTimes['support']['workingdays'] + $TTimes['retd']['workingdays']) / $nb_presence_day * 100);
		$style_prod = '';
		if (GETPOST('with-color')) {
			if ($productivity_presence >= 75) $style_prod = 'background-color:green;';
			else if ($productivity_presence >= 65) $style_prod = 'background-color:#60ff60;';
			else if ($productivity_presence >= 60) $style_prod = 'background-color:#e0ffe0;';
			else if ($productivity_presence < 40) $style_prod = 'background-color:red;';
			else if ($productivity_presence < 50) $style_prod = 'background-color:orange;';
			else if ($productivity_presence < 60) $style_prod = 'background-color:#fbc97f;';

		}

		if ($u->nb_working_days_exercice == 0) $estimated_capacity = 0;
		else $estimated_capacity = round($u->nb_billed_days_exercice / $u->nb_working_days_exercice * 100);

		if ($TTimes['client']['workingdays'] == 0) $customer_days = 0;
		else $customer_days = round($u->nb_billed_days_exercice / $TTimes['client']['workingdays'] * 100);
		echo '<td class="right">' . $estimated_capacity . '%</td>'
			. '<td class="right"> ' . $customer_days . '%</td>'
			. '<td class="right" style="' . $style_prod . '"> ' . $productivity_presence . '%</td>';
//	echo '<td align="right">'.round($u->nb_billed_days_exercice / $TTimes['client']['workingdays'] * 100).'%</td>';

		echo '</tr>';

		$TTotalGroup['nb_day'] += $u->nb_working_days_exercice;
		$TTotalGroup['nb_day_client'] += $TTimes['client']['workingdays'];
		$TTotalGroup['nb_day_retd'] += $TTimes['retd']['workingdays'];
		$TTotalGroup['nb_day_billed'] += $u->nb_billed_days_exercice;

		if ($u->statut == 1) {
			$TTotalGroup['nb_day_capacity'] += $u->array_options['options_nb_working_day'];
		}
	}


	$TTotal['nb_day'] += $TTotalGroup['nb_day'];
	$TTotal['nb_day_client'] += $TTotalGroup['nb_day_client'];
	$TTotal['nb_day_retd'] += $TTotalGroup['nb_day_retd'];
	$TTotal['nb_day_billed'] += $TTotalGroup['nb_day_billed'];
	$TTotal['nb_day_capacity'] += $TTotalGroup['nb_day_capacity'];


	echo '<tr class="liste_total">';
	echo '<td colspan=2 class="right">TOTAL</td>';
	echo '<td class="right">' . price($TTotalGroup['nb_day_capacity'], 0, '', 1, -1, 0) . '</td>';
	echo '<td class="right">&nbsp;</td>';
	echo '<td class="right">' . price($TTotalGroup['nb_day_client'], 0, '', 1, -1, 0) . '</td>';
	echo '<td class="right">&nbsp;</td>';
	echo '<td class="right">' . price($TTotalGroup['nb_day_retd'], 0, '', 1, -1, 0) . '</td>';

	echo '<td colspan="2" class="right">' . $langs->trans('NbDaysBillableTotal') . '</td>';
	echo '<td class="right">' . price($TTotalGroup['nb_day'], 0, '', 1, -1, 0) . '</td>';
	echo '<td class="right">' . price($TTotalGroup['nb_day_billed'], 0, '', 1, -1, 0) . '</td>';
	if ($show_presence_day) echo '<td colspan="3">&nbsp;</td>';

	echo '<td></td>';
	echo '<td class="right">' . price(100* $TTotalGroup['nb_day_billed'] / $TTotalGroup['nb_day_client'], 0, '', 1, -1, 0) . '%</td>';
	echo '<td></td>';
	echo '</tr>';
/*
	echo '<tr class="liste_total">';
	echo '<td colspan=10 class="right">' . $langs->trans('NbDaysBilledTotal') . '</td>';
	echo '<td class="right">' . price($time, 0, '', 1, -1, 0) . '</td>';
	echo '</tr>';

	echo '<tr class="total">';
	echo '<th colspan=9 class="right">' . $langs->trans('PercentageProductivity') . '</td>';
	echo '<th class="right">' . price($time / $TTotal['nb_day'] * 100, 0, '', 1, -1, 0) . ' %</td>';
	echo '</tr>';
*/
}


echo '</table>';

echo '<br>';

// Masse salariale
print load_fiche_titre($langs->trans('Salaries'));

echo '<table class="liste">';
echo '<tr>';
echo '<th>'.$langs->trans('User').'</th>';
echo '<th>'.$langs->trans('DateEmployment').'</th>';
echo '<th>'.$langs->trans('NbMonthFiscalYear').'</th>';
echo '<th>'.$langs->trans('Salary').'</th>';
//echo '<th>'.$langs->trans('MonthlyCost').'</th>';
echo '<th>'.$langs->trans('TotalCostFiscalYear').'</th>';
echo '</tr>';

foreach($TGroup as $groupe => $TUser) {
	echo '<tr><th colspan="5">'.$groupe.'</th></tr>';
	
	foreach($TUser as &$u) {
		$var=!$var;
		echo '<tr '.$bc[$var].'>';
		echo '<td>'.$u->getNomUrl(1).'</td>';
		echo '<td align="center">'.dol_print_date($u->dateemployment);
		if(!empty($u->array_options['options_date_sortie'])) {
			echo ' - '.dol_print_date($u->array_options['options_date_sortie']);
		}
		echo '</td>';
		
		echo '<td align="right">'.price($u->nb_month_exercice,0,'',1,0,0).'</td>';
		echo '<td align="right">'.price($u->salary,0,'',1,0,0).'</td>';
		//echo '<td align="right">'.price($u->array_options['options_monthly_cost'],0,'',1,0,0).'</td>';
		echo '<td align="right">'.price($u->total_cost,0,'',1,0,0).'</td>';
		
		echo '</tr>';
	
		$TTotal[$groupe]['nb_month']+=$u->nb_month_exercice;
		$TTotal[$groupe]['salary']+=$u->salary;
		//$TTotal[$groupe]['monthly_cost']+=$u->array_options['options_monthly_cost'];
		$TTotal[$groupe]['total_cost']+=$u->total_cost;

		$TTotal['nb_month']+=$u->nb_month_exercice;
		$TTotal['salary']+=$u->salary;
		//$TTotal['monthly_cost']+=$u->array_options['options_monthly_cost'];
		$TTotal['total_cost']+=$u->total_cost;
	}

	echo '<tr class="liste_total">';
	echo '<td colspan="2">'.$langs->trans('Total').' '.$groupe.'</td>';
	echo '<td align="right">'.price($TTotal[$groupe]['nb_month'],0,'',1,0,0).'</td>';
	echo '<td align="right">'.price($TTotal[$groupe]['salary'],0,'',1,0,0).'</td>';
	//echo '<td align="right">'.price($TTotal[$groupe]['monthly_cost'],0,'',1,0,0).'</td>';
	echo '<td align="right">'.price($TTotal[$groupe]['total_cost'],0,'',1,0,0).'</td>';
	echo '</tr>';
}
echo '<tr><th colspan="5">'.$langs->trans('SocialContribution').'</th></tr>';

// Récupération des charges sociales
_get_charges_sociales($TTotal, $tFiscal, $tFiscalFin);
$TTotal['total_cost'] += $TTotal['charges']['total_cost'];

echo '<tr class="liste_total">';
echo '<td colspan="2">'.$langs->trans('Total').' '.$langs->trans('SocialContribution').'</td>';
echo '<td align="right">&nbsp;</td>';
echo '<td align="right">&nbsp;</td>';
//echo '<td align="right">&nbsp;</td>';
echo '<td align="right">'.price($TTotal['charges']['total_cost'],0,'',1,0,0).'</td>';
echo '</tr>';

echo '<tr class="liste_total">';
echo '<th colspan="2">'.$langs->trans('Total').'</td>';
echo '<th align="right">'.price($TTotal['nb_month'],0,'',1,0,0).'</td>';
echo '<th align="right">'.price($TTotal['salary'],0,'',1,0,0).'</td>';
//echo '<th align="right">'.price($TTotal['monthly_cost'],0,'',1,0,0).'</td>';
echo '<th align="right">'.price($TTotal['total_cost'],0,'',1,0,0).'</td>';
echo '</tr>';

echo '</table>';

echo '<br>';

// Récupération des autres charges (factures fournisseur, notes de frais, charges diverses)
$TCharge = _get_autre_charges_amount($tFiscal, $tFiscalFin);
@$TTotal['autres_charges']['total_cost'] = array_sum($TCharge);

// Calcul du résultat
$resultat = $catotal - $TTotal['total_cost'] - $TTotal['autres_charges']['total_cost'];

// Ratios entreprise
print load_fiche_titre('Ratios');

$totalnet = $TTotal['Technique']['total_cost'] + $TTotal['Commercial']['total_cost'];
$coeff = $totalnet > 0 ? $TTotal['Commercial']['total_cost'] / $totalnet : 0;
$TTotal['Commercial']['total_cost'] = $TTotal['Commercial']['total_cost'] + $TTotal['charges']['total_cost'] * $coeff;
$coeff = $totalnet > 0 ? $TTotal['Technique']['total_cost'] / $totalnet : 0;
$TTotal['Technique']['total_cost'] = $TTotal['Technique']['total_cost'] + $TTotal['charges']['total_cost'] * $coeff;

echo '<table class="noborder boxtable">';
echo '<tr class="liste_total"><th>CA TOTAL</th><th align="right">'.price($catotal,0,'',1,0,0).'</th><th align="right">100 %</th></tr>';
echo '<tr class="liste_total"><td>Coût technique</td><td align="right">'.price($TTotal['Technique']['total_cost'],0,'',1,0,0).'</td><td align="right">'.round($TTotal['Technique']['total_cost'] * 100 / $catotal).' %</td></tr>';
echo '<tr class="liste_total"><td>Coût commercial</td><td align="right">'.price($TTotal['Commercial']['total_cost'],0,'',1,0,0).'</td><td align="right">'.round($TTotal['Commercial']['total_cost'] * 100 / $catotal).' %</td></tr>';
//echo '<tr class="liste_total"><td>Charges sociales</td><td align="right">'.price($TTotal['charges']['total_cost'],0,'',1,0,0).'</td><td align="right">'.round($TTotal['charges']['total_cost'] * 100 / $catotal).' %</td></tr>';
echo '<tr class="liste_total"><th>Ratio MS / MB</th><th align="right">'.price($TTotal['total_cost'],0,'',1,0,0).'</th><th align="right">'.round($TTotal['total_cost'] * 100 / $catotal).' %</th></tr>';
echo '<tr class="liste_total"><td>Autres charges</td><td align="right" title="'.implode(',',$TCharge).'">'.price($TTotal['autres_charges']['total_cost'],0,'',1,0,0).'</td><td align="right">'.round($TTotal['autres_charges']['total_cost'] * 100 / $catotal).' %</td></tr>';
echo '<tr class="liste_total"><th>Résultat</th><th align="right">'.price($resultat,0,'',1,0,0).'</th><th align="right">'.round($resultat * 100 / $catotal).' %</th></tr>';

echo '</table>';

llxFooter();

function _get_autre_charges_amount($tStart,$tEnd) {
	global $db,$conf;
	$PDOdb = new TPDOdb;
	$cdr = new CompteDeResultat($PDOdb);
	$cdr->date_deb = $tStart;
	$cdr->date_fin = $tEnd;
	$cdr->_load_annuel('real');
	
	$totalCharges = $cdr->TResultat['real']['AchatsConsommes']['price'];
	$totalCharges+= $cdr->TResultat['real']['AchatsFournitures']['price'];
	$totalCharges+= $cdr->TResultat['real']['ServicesExterieurs']['price'];
	$totalCharges+= $cdr->TResultat['real']['AutresServicesExterieurs']['price'];
	
	return array($totalCharges,0,0);
}

function get_data_salary($tStart, $tEnd, $TGroupId=array()) {
	global $db;
	
	$TUser=array();
	
	$sql = "SELECT u.rowid, ugu.fk_usergroup ";
	$sql.= "FROM ".MAIN_DB_PREFIX."user u ";
	$sql.= "LEFT JOIN ".MAIN_DB_PREFIX."user_extrafields uext ON (uext.fk_object = u.rowid) ";
	$sql.= "LEFT JOIN ".MAIN_DB_PREFIX."usergroup_user ugu ON (ugu.fk_user = u.rowid) ";
	$sql.= "WHERE u.fk_soc IS NULL ";
	$sql.= "AND u.employee = 1 ";
	// TO have only activ user
	// $sql.= "AND u.statut = 1 ";
	if(!empty($TGroupId)) $sql.= "AND ugu.fk_usergroup IN (".implode(',', $TGroupId).") ";
	$sql.= "AND u.dateemployment < '".date("Y-m-d",$tEnd)."' ";
	$sql.= "AND (uext.date_sortie > '".date("Y-m-d",$tStart)."' OR uext.date_sortie IS NULL) ";
	$sql.= "GROUP BY u.rowid ";
	$sql.= "ORDER BY u.dateemployment ASC";


	$TTempsVendus = get_data_temps_vendus($tStart, $tEnd, $TGroupId=array());


	$res = $db->query($sql);
	
	while($obj = $db->fetch_object($res)) {
	
		$u = new User($db);
		if($u->fetch($obj->rowid)>0) {
			// Date de sortie de l'entreprise (extrafield pour le moment)
			$u->dates = strtotime('+50 year');
			if(!empty($u->array_options['options_date_sortie'])) $u->dates = strtotime($u->array_options['options_date_sortie'].' +1day');
			$u->dateemploymenteff = strtotime('+1 month', $u->dateemployment);
			
			// Nombre de mois depuis l'entrée dans l'entreprise
			$u->nb_month = _calcul_nb_month($u->dateemployment, ($u->dates < $tEnd) ? $u->dates : $tEnd);
			
			// Nombre de mois entre le début de la période observée et aujourdhui
			$u->nb_month_exercice = _calcul_nb_month(($u->dateemployment < $tStart) ? $tStart : $u->dateemployment, ($u->dates < $tEnd) ? $u->dates : $tEnd);
			// Idem pondéré à + 1 mois (efficacité)
			$u->nb_month_exercice_pond = _calcul_nb_month(($u->dateemploymenteff < $tStart) ? $tStart : $u->dateemploymenteff, ($u->dates < $tEnd) ? $u->dates : $tEnd);
			// Nombre de jours efficaces entre le début de la période et aujourd'hui
			$u->nb_working_days_exercice = $u->nb_month_exercice_pond * $u->array_options['options_nb_working_day'];
			
			$u->nb_billed_days_exercice = $TTempsVendus[$u->id]; // _get_billed_day_for_user($u->id,$tStart,$tEnd);
			//$u->total_cost = $u->nb_month_exercice * $u->array_options['options_monthly_cost'];
			$u->total_cost = get_salary_cost($u->id,$tStart,$tEnd);
			
			$TUser[] = $u;
		}
	}

	return $TUser;
}


function get_data_temps_vendus($tStart, $tEnd)
{
    global $db;

    $TData = array();

    $sql = '
        SELECT pt.rowid, pt.duration_effective, pt.planned_workload, COALESCE(pt.progress, 0) AS progress
        , SUM(ptt.task_duration) as duration_for_period
        FROM ' . MAIN_DB_PREFIX . 'projet_task pt
        INNER JOIN ' . MAIN_DB_PREFIX . 'projet_task_time ptt on ptt.fk_task = pt.rowid
        INNER JOIN ' . MAIN_DB_PREFIX . 'projet p on p.rowid = pt.fk_projet
        WHERE pt.ref REGEXP "^TA?[0-9]+$"
        AND p.fk_soc IS NOT NULL
        AND p.fk_soc > 0
        AND p.fk_soc != 3764
        AND ptt.task_date BETWEEN "' . date('Y-m-d', $tStart) . '" AND "' . date('Y-m-d', $tEnd) . '"
        GROUP BY pt.rowid
        HAVING COUNT(ptt.rowid) > 0';

    $resql = $db->query($sql);

    if ($resql === false)
    {
        dol_print_error($db);
        exit;
    }

    $num = $db->num_rows($resql);


    for ($i = 0; $i < $num; $i++)
    {
        $taskStatic = $db->fetch_object($resql);

        $TData[$taskStatic->rowid] = array(
            'task' => $taskStatic
            , 'TUserTimeSpent' => array()
            , 'TUserTimeSold' => array()
        );

        /* Temps passé par utilisateur */

        $sqlTimes = '
            SELECT fk_user, SUM(task_duration) as time_spent
            FROM ' . MAIN_DB_PREFIX . 'projet_task_time
            WHERE fk_task = ' . $taskStatic->rowid . '
            AND task_date BETWEEN "' . date('Y-m-d', $tStart) . '" AND "' . date('Y-m-d', $tEnd) . '"
            GROUP BY fk_user';

        $resqlTimes = $db->query($sqlTimes);

        if ($resqlTimes === false)
        {
	        dol_print_error($db);
	        exit;
        }

        $numTimes = $db->num_rows($resqlTimes);

        for ($j = 0; $j < $numTimes; $j++)
        {
            $objTimes = $db->fetch_object($resqlTimes);

            $TData[$taskStatic->rowid]['TUserTimeSpent'][$objTimes->fk_user] = $objTimes->time_spent;
	        $TData[$taskStatic->rowid]['TUserTimeSold'][$objTimes->fk_user] = 0;
        }


        /* Evolution de la progression */

        $sqlProgress = '
            SELECT oldprogress, newprogress, tms
            FROM ' . MAIN_DB_PREFIX . 'projet_task_progress_log
            WHERE fk_task = ' . $taskStatic->rowid . '
            AND tms <= ' . date('Y-m-d', $tEnd);

        $resqlProgress = $db->query($sqlProgress);

        if ($resqlProgress === false)
        {
	        dol_print_error($db);
	        exit;
        }

        $numProgress = $db->num_rows($sqlProgress);

        $progress = 0;

        if ($taskStatic->duration_effective == $taskStatic->duration_for_period)
        {
            $progress = $taskStatic->progress;
        }
        elseif ($numProgress < 2)
        {
            $progress = $taskStatic->progress * $taskStatic->duration_for_period / $taskStatic->duration_effective;
        }
        else
        {
            $oldProgress = 0;
            $newProgress = 0;

	        for ($j = 0; $j < $numProgress; $j++)
	        {
		        $objProgress = $db->fetch_object($resqlProgress);

		        if ($objProgress->tms < date('Y-m-d', $tStart))
                {
                    $oldProgress = $objProgress->newProgress;
                }
		        else
                {
	                $newProgress = $objProgress->newProgress;
                }
            }

	        $progress = $newProgress - $oldProgress;
        }

	    $TData[$taskStatic->rowid]['task']->progress_for_period = $progress;


        /* Temps vendu par utilisateur */

        foreach (array_keys($TData[$taskStatic->rowid]['TUserTimeSold']) as $fk_user)
        {
	        $TData[$taskStatic->rowid]['TUserTimeSold'][$fk_user] = $progress * $TData[$taskStatic->rowid]['TUserTimeSpent'][$fk_user] * $taskStatic->planned_workload / 100 / $taskStatic->duration_for_period;
        }

        $db->free($resqlTimes);
        $db->free($resqlProgress);
    }

	$db->free($resql);

    $TRes = array();

    foreach ($TData as $fk_task => $TTaskData)
    {
        foreach($TTaskData['TUserTimeSold'] as $fk_user => $timeSold)
        {
            if (empty($TRes[$fk_user])) $TRes[$fk_user] = 0;

	        $TRes[$fk_user] += $timeSold / 3600 / 7;
        }
    }

    return $TRes;
}


function get_salary_cost($fk_user,$tStart,$tEnd) {
	global $db;
	
	$sql = "SELECT SUM(amount) as salary ";
	$sql.= "FROM ".MAIN_DB_PREFIX."payment_salary ";
	$sql.= "WHERE fk_user = ".$fk_user." ";
	$sql.= "AND datesp BETWEEN '".date("Y-m-d",$tStart)."' AND '".date("Y-m-d",$tEnd)."' ";
	
	//echo $sql;
	
	$res = $db->query($sql);
	
	if($obj = $db->fetch_object($res)) {
		return $obj->salary;
	}
	
	return 0;
}

function _get_billed_day_for_user($fk_user,$tStart,$tEnd) {
	global $db;

	$TProductId = _get_product_production() ;

	$sql = '
        SELECT t.ref
        , t.planned_workload
        , t.progress
        , t.rowid as fk_task
        , (
            SELECT SUM(task_duration)
            FROM ' . MAIN_DB_PREFIX . 'projet_task_time
            WHERE fk_task = t.rowid
			AND task_date < "' .date('Y-m-d', $tStart) . '"
		) as tt_before
		, (
		    SELECT count(*)
		    FROM ' . MAIN_DB_PREFIX . 'element_contact ec2
		    WHERE ec2.element_id = ec.element_id
		) as nb_linked_contact
		
		FROM ' . MAIN_DB_PREFIX . 'projet_task_time tt
		INNER JOIN ' . MAIN_DB_PREFIX . 'element_contact ec ON tt.fk_user = ec.fk_socpeople AND ec.element_id = tt.fk_task
		INNER JOIN ' . MAIN_DB_PREFIX . 'projet_task t ON t.rowid = tt.fk_task
		INNER JOIN ' . MAIN_DB_PREFIX . 'projet p ON t.fk_projet = p.rowid
		INNER JOIN ' . MAIN_DB_PREFIX . 'societe s ON s.rowid = p.fk_soc
		
		WHERE tt.fk_user = ' . $fk_user . '
		AND s.rowid != 3764
		AND tt.task_date BETWEEN "' . date('Y-m-d', $tStart) . '"  AND "' . date('Y-m-d', $tEnd) . '"
		AND ec.fk_c_type_contact IN (180,181)
		
		GROUP BY t.rowid';

	$res = $db->query($sql);

	if($res === false) {
		var_dump($db);exit;
	}

	$day = 0;
//echo $db->lastquery;exit;
	while($obj = $db->fetch_object($res))
    {
		$ref = $obj->ref;

		if(preg_match('/^TA?[0-9]+$/', $ref))
		{
			$coef = 1 - ($obj->tt_before / $obj->planned_workload); // temps saisie sur 2 plages distincte, proratise le temps efficace restant


			if($coef < 0) $coef = 0;

			if($obj->nb_linked_contact > 1)
			{
				$sql = '
                    SELECT COUNT(DISTINCT fk_user) as nb
                    FROM ' . MAIN_DB_PREFIX . 'projet_task_time tt
                    WHERE tt.fk_task = ' . $obj->fk_task . '
                    AND tt.task_date BETWEEN "' . date('Y-m-d', $tStart) . '" AND "' . date('Y-m-d', $tEnd) . '"';

				$res2 = $db->query($sql);
				$obj2  =$db->fetch_object($res2);

				$nb_user_on_task = $obj2->nb;
			}
			else
			{
				$nb_user_on_task = 1;
			}

			$day += $obj->planned_workload * $coef * ($obj->progress / 100) / 3600  / 7 / $nb_user_on_task;
		}

	}
	

	return $day;

}

function _gpp_product_id_from_cat(&$TIdProductProduction, $fk_cat) {
	global $db;
	dol_include_once('/categories/class/categorie.class.php');
        $cat = new Categorie($db);	
	$cat->id = $fk_cat;

	$TProduct = $cat->getObjectsInCateg('product',false);
	foreach($TProduct as &$product) { 
		if(!in_array($product->id,$TIdProductProduction)) $TIdProductProduction[] = $product->id;
	}

	$TFille = $cat->get_filles();
	foreach($TFille as &$sc) {
		_gpp_product_id_from_cat($TIdProductProduction, $sc->id);
	}

}

function _get_product_production() {
	global  $TIdProductProduction;

	if(empty($TIdProductProduction)) $TIdProductProduction=array();
	else return $TIdProductProduction;

	_gpp_product_id_from_cat($TIdProductProduction, 30);
	
	return $TIdProductProduction;
}

function _get_times_enter($u,$tStart, $tEnd) {
	global $db;
	
	$TRes = array(
			'client'=>array('days'=>0,'hours'=>0)
			,'support'=>array('days'=>0,'hours'=>0)
			,'other'=>array('days'=>0,'hours'=>0)
			,'total'=>array('days'=>0,'hours'=>0)
	);
	
	$sql = "SELECT SUM(task_duration) as tasktime, t.label, p.title as 'project_title', p.fk_soc, t.planned_workload,p.rowid as fk_project, t.rowid as fk_task";
	$sql.=" FROM ".MAIN_DB_PREFIX."projet_task_time tt";
	$sql.=" LEFT JOIN ".MAIN_DB_PREFIX."projet_task t on (t.rowid = tt.fk_task)";
	$sql.=" LEFT JOIN ".MAIN_DB_PREFIX."projet p on (p.rowid = t.fk_projet)";
	$sql.=" WHERE tt.fk_user = ".$u->id;
	$sql.=" AND tt.task_date <= '".date("Y-m-d",$tEnd)."'";
	$sql.=" AND tt.task_date >= '".date("Y-m-d",$tStart)."'";
	$sql.=" GROUP BY t.rowid, t.label, p.fk_soc";
		
	$res = $db->query($sql);
	
	while($obj = $db->fetch_object($res)) {
		$time = $obj->tasktime;
		$hours = $time/60/60;
		$totaldays = $hours/24;
		$days = $totaldays*(24/7);
		$planned = $obj->planned_workload/60/60/24 * (24/7);
		// Pas client ATM ou aternatik

		if(($obj->label == 'Tickets' || $obj->label == 'Tickets internes')) {
                        // Cas Support
                        $TRes['support']['hours'] += $hours;
                        $TRes['support']['workingdays'] += $days;
                        $TRes['support']['days'] += $totaldays;
                }
		else if($obj->fk_project == 139 || $obj->fk_project == 347 || strpos($obj->project_title, "ATM R&D")!==false || $obj->fk_task == 2444 || $obj->fk_task == 347) {
                        // Cas R&D
                        $TRes['retd']['hours'] += $hours;
                        $TRes['retd']['planned'] += $planned;
                        $TRes['retd']['workingdays'] += $days;
                        $TRes['retd']['days'] += $totaldays;
                } 
		else if($obj->fk_soc != '3764' && $obj->fk_soc != '14423') {
			// Cas Client
			$TRes['client']['hours'] += $hours;
			$TRes['client']['planned'] += $planned;
			$TRes['client']['workingdays'] += $days;
			$TRes['client']['days'] += $totaldays;
		} 

		else {
			// Cas Autre
			$TRes['other']['hours'] += $hours;
			$TRes['other']['workingdays'] += $days;
			$TRes['other']['days'] += $totaldays;
		}
		// Cas Total
		$TRes['total']['hours'] += $hours;
		$TRes['total']['workingdays'] += $days;
		$TRes['total']['days'] += $totaldays;
	}
	
	return $TRes;
}

function _get_charges_sociales(&$TTotal, $tStart, $tEnd) {
	global $db;
	
	$TTotal['charges'] = array(
			'nb_month' => 0
			,'monthly_cost' => 0
			,'total_cost' => 0
	);
		
	$sql = "SELECT SUM(amount) as total";
	$sql.=" FROM ".MAIN_DB_PREFIX."chargesociales cs";
	$sql.=" INNER JOIN ".MAIN_DB_PREFIX."c_chargesociales ccs on (ccs.id = cs.fk_type)";
	$sql.=" WHERE cs.periode <= '".date("Y-m-d",$tEnd)."'";
	$sql.=" AND cs.periode >= '".date("Y-m-d",$tStart)."'";
	$sql.=" AND (
		accountancy_code LIKE '43____'
		OR accountancy_code LIKE '641300'
		OR accountancy_code LIKE '633300'
		OR accountancy_code LIKE '695200'
		OR accountancy_code LIKE '648000'
		OR accountancy_code LIKE '646___'
		OR accountancy_code LIKE '6411__'
		OR accountancy_code LIKE '6481__'
		)";
	
	$res = $db->query($sql);
		
	while($obj = $db->fetch_object($res)) {
		$TTotal['charges']['total_cost'] = $obj->total;
	}
	
	return $TTotal;
}

function _calcul_nb_month($tStart, $tEnd) {
	if($tStart > $tEnd) return 0;
	
	$d1 = new DateTime();
	$d1->setTimestamp($tStart);
	$d2 = new DateTime();
	$d2->setTimestamp($tEnd);
	
	$diff = $d1->diff($d2);
//	echo date('d/m/Y',$tStart) . ' > ' . date('d/m/Y',$tEnd);
	//var_dump($diff,$diff->format('%m'));exit;
	$res =  round($diff->d / date('t',$tEnd) ) + $diff->m + $diff->y*12;
//var_dump(date('d/m/Y',$tStart), date('d/m/Y',$tEnd),$res,$diff);
	return $res;
}


// Code récupéré depuis le module absence mais adapté pour ne pas prendre en compte certains
// types d'absence comme le home-office

function _isWorkingDay(&$PDOdb, $date, $absence)
{
    $isNotFerie = false;
    $isNotAbsence = false;
    $isWorkingDay = false;


	$isNotFerie = !(TRH_JoursFeries::estFerie($PDOdb, $date));

	if($isNotFerie) $isNotAbsence = _isNotAbsenceDay($PDOdb, $date, $absence);

	if($isNotFerie && $isNotAbsence) $isWorkingDay = (TRH_EmploiTemps::estTravaille($PDOdb, $absence->fk_user, $date) != 'NON');

	return array($isWorkingDay, $isNotFerie, $isNotAbsence);
}


function _isNotAbsenceDay(&$PDOdb, $date, $absence)
{
    $sql = $absence->rechercheAbsenceUser($PDOdb, $absence->fk_user, $date, $date);
	$Tab = $PDOdb->ExecuteAsArray($sql);

    $TabWithoutHomeOffice = array();

    foreach ($Tab as $absenceObj)
    {
        if ($absenceObj->type != 'homeoffice')
        {
            $TabWithoutHomeOffice[] = $absenceObj;
        }
    }

	return (count($TabWithoutHomeOffice) == 0);
}

