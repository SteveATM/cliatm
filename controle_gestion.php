<?php
ini_set('display_errors', true);
ini_set('memory_limit', '500M');
require 'config.php';
require_once DOL_DOCUMENT_ROOT . '/comm/propal/class/propal.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/html.formother.class.php';

$year = GETPOST('year');
if(empty($year)) $year = date('Y');

// Action de cocher la case "Propale contrôlée"
$TIDpropales = array();
$propal = new Propal($db);
//Traitement de la mise à jour du contrôle des Propositions Commerciales
if (! empty($_REQUEST['TIDPropales'])) {
	$TIDpropales = $_REQUEST['TIDPropales'];
	foreach ($TIDpropales as $PropalId){
		$propal->fetch($PropalId);
		$propal->fetch_optionals($propal->id);
		$propal->array_options['options_propal_control'] = 1;
		$propal->update($user);
	}
}

// Récupération des propositions signées ou facturées et pas encore contrôlées
$sql = 'SELECT p.rowid
        FROM '.MAIN_DB_PREFIX.'propal p
        LEFT JOIN '.MAIN_DB_PREFIX.'propal_extrafields as pe ON (pe.fk_object = p.rowid )
        WHERE p.fk_statut IN (2,4) AND YEAR(p.datep) = '.$year.' AND pe.propal_control IS NULL';
$resql = $db->query($sql);
$TRes = array();

if($resql) {
	while($obj = $db->fetch_object($resql)) {
		$p = new Propal($db);
		$p->fetch($obj->rowid);
		$TRes[]= $p;
	}
}


// Vérification de l'état d'avancement et de facturation
$TProd = array();
$TFact = array();
$TTotal = array();
$formother = new FormOther($db);
$nbOK = $nbPropal = 0;

/** @var Propal $propal */
foreach ($TRes as $propal) {
	$propal->fetchObjectLinked();

	$total_propal = $propal->total_ht;
	$total_commande = $total_expedition = $total_facture = 0;

	// Avec commandes
	if(isset($propal->linkedObjects['commande'])) {
		foreach ($propal->linkedObjects['commande'] as $commande) {
			$total_commande += $commande->total_ht;

			$commande->fetchObjectLinked();
			if(isset($commande->linkedObjects['shipping'])) {
				foreach ($commande->linkedObjects['shipping'] as $expedition) {
					$total_expedition += $expedition->total_ht;
				}
			}

			if(isset($commande->linkedObjects['facture'])) {
				foreach ($commande->linkedObjects['facture'] as $facture) {
					if(isset($TFact[$facture->id])) continue;
					$total_facture += $facture->total_ht;
					$TFact[$facture->id] = $facture->id;
				}
			}
		}
	}

	// Sans commandes
	if(isset($propal->linkedObjects['facture'])) {
		foreach ($propal->linkedObjects['facture'] as $facture) {
			if(isset($TFact[$facture->id])) continue;
			$total_facture += $facture->total_ht;
			$TFact[$facture->id] = $facture->id;
		}
	}

	// Données projet
	$res = $propal->fetch_projet();
	$propal->fetch_optionals($propal->id);

	// Une propale est OK si le montant de la propale est égal au montant de la facture et que la propale et le projet sont clos
	$TProd[$propal->id]['propal_control'] = '';
	$nbPropal++;
	if(!empty($propal->fk_project) && $total_propal == $total_facture && $propal->statut == Propal::STATUS_BILLED && $propal->project->statut == Project::STATUS_CLOSED) {
		$TProd[$propal->id]['propal_control'] = 'checked="checked"';
		$nbOK++;
	}

	// Stockage résultats
	$TProd[$propal->id]['propal'] = $propal->getNomUrl(1);
	$TProd[$propal->id]['project'] = !empty($propal->fk_project) ? $propal->project->getNomUrl(1, '', 1) : '';
	$TProd[$propal->id]['status'] = $propal->getLibStatut(3). ' ' . (!empty($propal->fk_project) ? $propal->project->getLibStatut(3) : '');
	$TProd[$propal->id]['total_propal'] = $total_propal;
	$TProd[$propal->id]['total_commande'] = $total_commande;
	$TProd[$propal->id]['total_expedition'] = $total_expedition;
	$TProd[$propal->id]['total_facture'] = $total_facture;

	$TTotal['propal'] += $total_propal;
	$TTotal['commande'] += $total_commande;
	$TTotal['expedition'] += $total_expedition;
	$TTotal['facture'] += $total_facture;
}

// Affichage
llxHeader();

?>
	<script type="text/javascript">
		$(document).ready(function() {

			// **This check determines if using a jQuery version 1.7 or newer which requires the use of the prop function instead of the attr function when not called on an attribute
			if ($().prop) {
				$("#checkall").click(function() {
					$(".checkforgen").prop('checked', true);
				});
				$("#checknone").click(function() {
					$(".checkforgen").prop('checked', false);
				});
			}
			else {
				$("#checkall").click(function() {
					$(".checkforgen").attr('checked', true);
				});
				$("#checknone").click(function() {
					$(".checkforgen").attr('checked', false);
				});
			}


		});
	</script>
<?php
$formhtml = $formother->selectYear($year, 'year', 0, 10, 0, 0, 0, 'onchange="this.form.submit();"');
print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
print load_fiche_titre($langs->trans('ATMPropalControlTitle', $year, $nbPropal, $nbOK), $formhtml);
print '</form>';

print '<form method="POST" action="' . $_SERVER["PHP_SELF"] . '">';
print '<input type="hidden" name="year" value="'.$year.'" />';
print '<table class="centpercent notopnoleftnoright liste">
<tr>
	<th>Propale</th>
	<th>Projet</th>
	<th>Statuts</th>
	<th>Total Propale</th>
	<th>Total Commande</th>
	<th>Total Expédition</th>
	<th>Total Facture</th>
	<th>Proposition contrôlée '.img_help(1, $langs->trans('ATMPropalControlOKHelp')).'
		<br><a href="#" id="checkall">' . $langs->trans("All") . '</a> / <a href="#" id="checknone">' . $langs->trans("None") . '</a>
	</th>
</tr>';

foreach ($TProd as $propalId => $data) {
	print '<tr>
		<td>'.$data['propal'].'</td>
		<td>'.$data['project'].'</td>
		<td align="center">'.$data['status'].'</td>
		<td align="right">'.price($data['total_propal']).'</td>
		<td align="right">'.price($data['total_commande']).'</td>
		<td align="right">'.price($data['total_expedition']).'</td>
		<td align="right">'.price($data['total_facture']).'</td>
		<td align="center">
			<input class="checkforgen" type="checkbox" ' . $data['propal_control'] . ' name="TIDPropales[]" value="' . $propalId . '" /></td>
		</tr>';
}

print '<tr class="liste_total">
	<td>'.$langs->trans("Total").'</td>
	<td></td>
	<td></td>
	<td align="right">'.price($TTotal['propal']).'</td>
	<td align="right">'.price($TTotal['commande']).'</td>
	<td align="right">'.price($TTotal['expedition']).'</td>
	<td align="right">'.price($TTotal['facture']).'</td>
	<td></td>
</tr>';


print '</table>';
print '<br /><input style="float:right" class="butAction" type="submit" name="subCreateShip" value="' . $langs->trans('Save') . '" />';
print '</form>';

dol_fiche_end();
