<?php
/*
 * Script créant et vérifiant que les champs requis s'ajoutent bien
 */
if(!defined('INC_FROM_DOLIBARR')) {
	define('INC_FROM_CRON_SCRIPT', true);
	require('../config.php');
}
else
{
	global $db;
}

dol_include_once('/cliatm/class/cliatm.class.php');

$PDOdb=new TPDOdb;
$PDOdb->db->debug=true;

$o=new TOrderType;
$o->init_db_by_vars($PDOdb);


$o = new TaskProgressLog($db);
$o->init_db_by_vars();

dol_include_once('/cliatm/script/migration.php');
