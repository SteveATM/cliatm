<?php
/* Copyright (C) 2014 Alexis Algoud        <support@atm-conuslting.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *    \file       /cliatm/script/migration_contrat_abo.php
 *    \ingroup    projet
 *    \brief      Project card
 *
 * forceRollback : Sert à ne pas commit les transactions
 */

require_once('../config.php');
set_time_limit(0);

dol_include_once('/cliatm/lib/cliatm.lib.php');
dol_include_once('/cliatm/class/cliatm.class.php');
require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture-rec.class.php';
require_once DOL_DOCUMENT_ROOT.'/contrat/class/contrat.class.php';

$debug = GETPOST('debug', 'int');
$limit = GETPOST('limit', 'int');
$forceRollback = GETPOST('forceRollback', 'int');
$cancelRun = GETPOST('cancelRun', 'int');
/**
 * Actions
 */
global $db, $conf, $user;

llxHeader('', 'Migration contrat abonnement');

$sql = 'SELECT c.rowid, ca.rowid as fk_contrat_abo, fr.coeffrepetition as coef, fr.nomfrequencerepetition as frequency_label';
$sql .= ' FROM '.MAIN_DB_PREFIX.'contrat as c';
$sql .= ' LEFT JOIN '.MAIN_DB_PREFIX.'contratdet as cd ON c.rowid=cd.fk_contrat';
$sql .= ' LEFT JOIN '.MAIN_DB_PREFIX.'contratabonnement as ca ON cd.rowid=ca.fk_contratdet';
$sql .= ' LEFT JOIN '.MAIN_DB_PREFIX.'frequence_repetition as fr ON ca.fk_frequencerepetition=fr.rowid';
$sql .= ' WHERE cd.statut=4';
$sql .= ' AND ca.statut=1';
$sql .= " AND c.rowid NOT IN (SELECT fk_source FROM ".MAIN_DB_PREFIX."element_element WHERE sourcetype='contrat' AND targettype='facturerec')";
$sql .= ' GROUP BY c.rowid, ca.rowid, fr.coeffrepetition, fr.nomfrequencerepetition';
$sql .= ' ORDER BY c.rowid';
if(! empty($limit)) $sql .= ' LIMIT '.$limit;

$resql = $db->query($sql);
$nbLines = $db->num_rows($resql);
if(! empty($debug)) {
    print $sql.'<br/>';
    print '<br/>Nb Lines : '.$nbLines;
    print '<br/>DB last error : ';
    var_dump($db->lasterror);
    print '<br/>';
}

if(empty($cancelRun) && $resql) {
    $TContractCreated = array();
    $i = $nbInserted = $nbRollback = $nbIgnored = $tmpFreq = $subprice = 0;

    // Parcourir tous les enregistrements
    while($obj = $db->fetch_object($resql)) {
        $i++;
        if(! empty($debug)) {
            print '<br/><strong>--------- [LOOP '.$i.'] ---------</strong><br/><br/>';
            print 'SQL object : ';
            var_dump($obj);
            print '<br/>--------------------------------<br/>';
        }

        // We already created a template invoice for this contract
        if(! empty($TContractCreated[$obj->rowid])) {
            if(! empty($debug)) print '<pre><strong><span style="background-color: #FF8000;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Contract already created : Next one</strong></pre>';
            $nbIgnored++;
            continue;
        }

        $TContractCreated[$obj->rowid] = true;
        $db->begin();

        $contrat = new Contrat($db);
        $contrat->fetch($obj->rowid);
        if(empty($contrat->thirparty->id)) $contrat->fetch_thirdparty();
        if(empty($contrat->lines)) $contrat->fetch_lines();

        $facture = new Facture($db);
        $facture->brouillon = 1;
        $facture->socid = $contrat->socid;
        $facture->fk_project = $contrat->fk_project;
        $facture->note_public = $contrat->note_public;
        $facture->date = dol_now();
        $facture->fk_account = 2;    // Compte "Caisse d'épargne"
        $facture->cond_reglement_id = $contrat->thirdparty->cond_reglement_id;
        $facture->mode_reglement_id = $contrat->thirdparty->mode_reglement_id;

        $unit_frequency = 'm';
        switch($obj->frequency_label) {
            case 'AllMonths':
                $frequency = 1;
                break;
            case 'AllTrimesters':
                $frequency = 3;
                break;
            case 'AllSemesters':
                $frequency = 6;
                break;
            case 'AllYears':
                $frequency = 1;
                $unit_frequency = 'y';
                break;
            case 'AllQuadrimesters':
                $frequency = 4;
                break;
            case 'AllThreeYears':
                $frequency = 3;
                $unit_frequency = 'y';
                break;
            default:
                print '<br/>Frequency label not handle : ['.$obj->frequency_label.']<br/>';
                $forceRollback = 1;
                break;
        }

        // Facturation tous les mois
        if($frequency == 1 && $unit_frequency == 'm') {
            // Prélèvement ...
            $sql3 = 'SELECT id';
            $sql3 .= ' FROM '.MAIN_DB_PREFIX.'c_paiement';
            $sql3 .= " WHERE code='PRE'";
            $resql3 = $db->query($sql3);
            if($resql3) {
                if($paiement = $db->fetch_object($resql3)) {
                    $facture->mode_reglement_id = $paiement->id;
                }
            }
            $db->free($resql3);
            // ... Sur le compte CIC
            $facture->fk_account = 16;
        }

        $fk_facture = $facture->create($user);

        // Retreive dates from contratabonnement module
        if(! empty($debug)) {
            print '<strong>Next generation :</strong><br/><br/>';
        }



        if($fk_facture > 0) {
            if(! empty($debug)) {
                print '<br/><br/>--------------------------------<br/>';
                print '<strong>Lines added :</strong><br/>';
            }

            // Lines to add
            foreach($contrat->lines as &$line) {
                // Do not add lines with not active status
                if($line->statut != 4) {
                    if(! empty($debug)) print '<br/> Line status not active (!= 4) : Not added<br/>';
                    continue;
                }

                $sql2 = 'SELECT cat.rowid, cat.datedebutperiode, cat.datefinperiode, cat.montantperiode';
                $sql2 .= ' FROM '.MAIN_DB_PREFIX.'contratabonnement_term as cat';
                $sql2 .= ' LEFT JOIN '.MAIN_DB_PREFIX.'contratabonnement as ca ON (ca.rowid = cat.fk_contratabonnement)';
                $sql2 .= ' WHERE ca.fk_contratdet='.$line->id;
                $sql2 .= ' AND ca.statut=1';
                $sql2 .= ' AND cat.datedebutperiode<=NOW() AND cat.datefinperiode>=NOW()';
                $resql2 = $db->query($sql2);

                if(! empty($debug)) {
                    print $sql2.'<br/>';
                    print '<br/>DB last error : ';
                    var_dump($db->lasterror);
                    print '<br/>';
                }

                if($resql2) {
                    if($objDate = $db->fetch_object($resql2)) {
                        if(! empty($debug)) {
                            print '<br/>SQL Date object : ';
                            var_dump($objDate);
                            print '<br/>';
                            var_dump(strtotime($objDate->datedebutperiode));
                        }

                        $date_when = strtotime($objDate->datefinperiode.' +1 day');
                        $montant_periode = $objDate->montantperiode;
                    } else {
                        $date_when = 0;
                        $montant_periode = 0;
                    }
                }
                $db->free($resql2);



                if(! empty($montant_periode) && ! empty($frequency)) {
                    $tmpFreq = $frequency;
                    if($unit_frequency == 'y') $tmpFreq *= 12;
                    $montant_periode /= $tmpFreq;
                    $montant_periode /= $line->qty;
                }
                $res = $facture->addline(
                    $line->description,
                    $montant_periode,
                    (! empty($tmpFreq) ? $line->qty * $tmpFreq : $line->qty),
                    $line->tva_tx,
                    $line->localtax1_tx,
                    $line->localtax2_tx,
                    $line->fk_product,
                    $line->remise_percent,
                    $line->date_ouverture,
                    $line->date_cloture,
                    0,
                    0,
                    '',
                    'HT'
                );
                if(! empty($debug)) {
                    var_dump($res);
                    print '<br/>';
                    print 'Line Subprice : '.$montant_periode.'<br/>';
                    print 'Line QTY : '.(! empty($tmpFreq) ? $line->qty * $tmpFreq : $line->qty).'<br/>';
                }
            }

            // Create template invoice
            $factureRec = new FactureRec($db);
            $factureRec->titre = 'Forfait '.$contrat->ref;
            $factureRec->socid = $contrat->socid;
            $factureRec->fk_project = $contrat->fk_project;
            $factureRec->note_public = $contrat->note_public;
            $factureRec->generate_pdf = 1;
            $factureRec->modelpdf = 'crabe_ATM';
            $factureRec->auto_validate = 1;


            // Set element_element link
            $factureRec->origin = $contrat->element;
            $factureRec->origin_id = $contrat->id;
            $factureRec->linked_objects[$factureRec->origin] = $factureRec->origin_id;

            // Keep frequency from contratabonnement module if set
            $factureRec->frequency = $frequency;
            $factureRec->unit_frequency = $unit_frequency;
            $factureRec->date_when = $date_when;

            if(! empty($debug)) print '<br/>--------------------------------<br/><pre><strong>';

            if($factureRec->create($user, $fk_facture) > 0 && empty($forceRollback)) {
                if(! empty($debug)) print '<br/><span style="background-color: green;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;COMMIT !<br/>';

                $nbInserted++;
                $db->commit();
            }
            else {
                if(! empty($debug)) {
                    print '<br/><span style="background-color: red;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;ROLLBACK !<br/>';
                    print '<br/>DB last error : ';
                    var_dump($db->lasterror);
                    print '<br/>';
                }

                $nbRollback++;
                $db->rollback();
            }
            if(! empty($debug)) print '</strong></pre>';

            // Delete draft invoice
            $facture->delete($user);
        }
        else {
            if(! empty($debug)) {
                print '<br/><span style="background-color: red;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;ROLLBACK !<br/>';
                dol_print_error($db);
            }

            $nbRollback++;
            $db->rollback();
        }
    }
    if(! empty($debug)) print '<br/>--------------------------------<br/>';

    // Execution summary
    print '<table>';

    print '<tr>';
    print '<td width="125px">NB LINES :</td>';
    print '<td>'.$nbLines.'</td>';
    print '</tr>';

    print '<tr><td colspan="2">&nbsp;</td></tr>';

    print '<tr>';
    print '<td>NB INSERTED :</td>';
    print '<td>'.$nbInserted.'</td>';
    print '</tr>';

    print '<tr>';
    print '<td>NB ROLLBACK :</td>';
    print '<td>'.$nbRollback.'</td>';
    print '</tr>';

    print '<tr>';
    print '<td>NB IGNORED :</td>';
    print '<td>'.$nbIgnored.'</td>';
    print '</tr>';

    print '<tr>';
    print '<td class="liste_titre">TOTAL :</td>';
    print '<td class="liste_titre">'.($nbInserted + $nbRollback + $nbIgnored).'</td>';
    print '</tr>';

    print '</table>';
}

llxFooter();
