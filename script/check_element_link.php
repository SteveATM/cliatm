<?php
/* Copyright (C) 2014 Alexis Algoud        <support@atm-conuslting.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       /cliatm/script/migration_sepa.php
 *	\ingroup    projet
 *	\brief      Project card
 */

require('../config.php');
set_time_limit(0);

require_once DOL_DOCUMENT_ROOT.'/societe/class/societe.class.php';
require_once DOL_DOCUMENT_ROOT.'/commande/class/commande.class.php';
require_once DOL_DOCUMENT_ROOT.'/expedition/class/expedition.class.php';
require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php';
require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
require_once DOL_DOCUMENT_ROOT.'/projet/class/task.class.php';
require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.class.php';
require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.facture.class.php';
require_once DOL_DOCUMENT_ROOT.'/contrat/class/contrat.class.php';
require_once DOL_DOCUMENT_ROOT.'/comm/action/class/actioncomm.class.php';
require_once DOL_DOCUMENT_ROOT.'/compta/sociales/class/chargesociales.class.php';
dol_include_once('/lead/class/lead.class.php');

$debug = GETPOST('debug', 'int');
$limit = GETPOST('limit', 'int');
$forceRollback = GETPOST('forceRollback', 'int');

/**
 * Actions
 */
global $db, $conf;

$sql = 'SELECT rowid, fk_source, sourcetype, fk_target, targettype';
$sql .= ' FROM '.MAIN_DB_PREFIX.'element_element';
if(! empty($limit)) $sql .= ' LIMIT '.$limit;

$resql = $db->query($sql);
if(! $resql) {
    dol_print_error($db);
    exit;
}

$nbLines = $db->num_rows($resql);
if(! empty($debug)) {
	print '<pre>'.$sql.'</pre><br/>';
	print '<br/>Nb Lines : '.$nbLines;
	print '<br/>DB last error : <pre>';
	var_dump($db->lasterror);
	print '</pre><br/>';
}

$error = $nbDeleted = 0;
$db->begin();

for($i = 1 ; $obj = $db->fetch_object($resql) ; $i++) {
    $sourceOK = $targetOK = false;

    if(! empty($debug)) {
        print '<br/><strong>--------- [LOOP '.$i.'] ---------</strong><br/><br/><pre>';
        var_dump($obj);
        print '</pre><br/>';
    }
    // On vérifie les sources
    $source = getCorrespondingObject($obj->sourcetype);
    if(! is_null($source)) {
        $source->fetch($obj->fk_source);
        if(! empty($source->id)) $sourceOK = true;
    }

    if($sourceOK) { // On vérifie les targets seulement si la source existe toujours
        $target = getCorrespondingObject($obj->targettype);

        if(! is_null($target)) {
            $target->fetch($obj->fk_target);
            if(! empty($target->id)) $targetOK = true;
        }
    }

    if(! $sourceOK || ! $targetOK) {
        $sqlDelete = 'DELETE FROM '.MAIN_DB_PREFIX.'element_element WHERE rowid = '.$obj->rowid;
        $resDelete = $db->query($sqlDelete);

        if(! $resDelete) $error++;
        else $nbDeleted++;
    }
}
print '<br/>Nb deleted: '.$nbDeleted;
print '<br/>Nb error: '.$error;
if(! empty($forceRollback) || ! empty($error)) {
    print '<br/><pre><strong><span style="background-color: red;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;ROLLBACK !</strong></pre><br/>';
    $db->rollback();
}
else {
    print '<br/><pre><strong><span style="background-color: green;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;COMMIT !</strong></pre><br/>';
    $db->commit();
}

function getCorrespondingObject($object_type) {
    global $db;

    switch($object_type) {
        case 'action':
            $target = new ActionComm($db);
            break;
        case 'chargesociales':
            $target = new ChargeSociales($db);
            break;
        case 'commande':
            $target = new Commande($db);
            break;
        case 'orderline':
            $target = new OrderLine($db);
            break;
        case 'order_supplier':
            $target = new CommandeFournisseur($db);
            break;
        case 'contrat':
            $target = new Contrat($db);
            break;
        case 'facture':
            $target = new Facture($db);
            break;
        case 'invoice_supplier':
            $target = new FactureFournisseur($db);
            break;
        case 'lead':
            $target = new Lead($db);
            break;
        case 'project':
            $target = new Project($db);
            break;
        case 'project_task':
            $target = new Task($db);
            break;
        case 'propal':
            $target = new Propal($db);
            break;
        case 'propaldet':
            $target = new PropaleLigne($db);
            break;
        case 'shipping':
            $target = new Expedition($db);
            break;
        case 'product':
            $target = new Product($db);
            break;
        case 'contratabonnement':   // Ces liens ne seront plus utilisés donc a delete plus tard
        default:
            $target = null;
            break;
    }

    return $target;
}