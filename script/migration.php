<?php
/* Copyright (C) 2014 Alexis Algoud        <support@atm-conuslting.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       /cliatm/script/migration.php
 *	\ingroup    projet
 *	\brief      Project card
 */

//require('config.php');
set_time_limit(0);

dol_include_once('/cliatm/lib/cliatm.lib.php');
dol_include_once('/cliatm/class/cliatm.class.php');
require_once DOL_DOCUMENT_ROOT . '/projet/class/task.class.php';

/**
 * Actions
 */
global $db, $conf;

// Ajoute un lien element_element entre les tâches et la ligne de propale associée
$taskPrefix = 'TA';

$sql = 'SELECT rowid, ref, fk_projet';
$sql .= ' FROM '.MAIN_DB_PREFIX.'projet_task';
$sql .= " WHERE ref LIKE '".$taskPrefix."%'";
$sql .= " AND ref NOT LIKE 'TK%'";	// Ne pas prendre en compte les tâches créées depuis le scrumboard
$sql .= " AND rowid NOT IN (SELECT fk_target FROM ".MAIN_DB_PREFIX."element_element WHERE targettype='project_task')";	// On ne prend pas la tâche si elle possède déjà un lien

$resql = $db->query($sql);
if($resql) {
	$db->begin();
	while($obj = $db->fetch_object($resql)) {
		$fk_line = substr($obj->ref, strlen($taskPrefix));	// Récupère l'identifiant de la ligne de propale en omettant le préfixe des tâches : 'TA125' ==> '125'

		$task = new Task($db);
		$task->fetch($obj->rowid);
		$task->fetchObjectLinked();

		if($taskPrefix == 'TA') $sourceType = 'propaldet';
		if(! empty($sourceType) && empty($task->linkedObjectsIds[$sourceType])) $task->add_object_linked($sourceType, $fk_line);
	}
	$db->commit();
}

// Vérification des données du dictionnaire c_order_type
$PDOdb = new TPDOdb;
$orderType = new TOrderType;

$TCode = array('projet', 'formation', 'abonnement');
foreach($TCode as $code) {
	$res = $orderType->loadBy($PDOdb, $code, 'code');

	if(empty($res)) {
		$orderType->code = $code;
		$orderType->label = ucfirst($code);
		$orderType->active = 1;
		$orderType->entity = $conf->entity;
	}
}
