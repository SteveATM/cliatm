<?php


define('INC_FROM_CRON_SCRIPT',true);
define('NOCSRFCHECK','1');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

require('../config.php');
require_once DOL_DOCUMENT_ROOT . '/core/class/html.form.class.php';
dol_include_once('/projet/class/project.class.php');
dol_include_once('mantis/class/mantis.class.php');
dol_include_once('/contrat/class/contrat.class.php');
dol_include_once('/contact/class/contact.class.php');
dol_include_once('/compta/facture/class/facture.class.php');

$ref_project = GETPOST('project_ref');
$fk_soc = GETPOST('fk_soc');

if(empty($user->id)) $user->fetch(8); // Got
$user->getrights();

if(!empty($fk_soc)) { // Parcours de tous les projets de la société

	$TProjets = array();
	$res = $db->query("SELECT rowid FROM ".MAIN_DB_PREFIX."projet WHERE fk_soc=".$fk_soc);
	while($obj = $db->fetch_object($res)) {
		$TProjets[] = $obj->rowid;
	}

	$res = $db->query("SELECT p.rowid FROM ".MAIN_DB_PREFIX."projet as p
                        LEFT JOIN ".MAIN_DB_PREFIX."contrat as c on c.fk_projet = p.rowid
                        WHERE c.fk_soc=".$fk_soc);
	while($obj = $db->fetch_object($res)) {
		$TProjets[] = $obj->rowid;
	}

	$TProjets = array_unique($TProjets);
	foreach ($TProjets as $pid){
		$projet=new Project($db);
		$projet->fetch($pid);
		_get_project_ass($projet);
	}
}

else{ // Projet passé en paramètre
	$projet=new Project($db);
	$projet->fetch('', GETPOST('project_ref'));
	_get_project_ass($projet);
}

function _get_project_ass(&$projet) {
	global $db,$langs,$user,$conf;

	if($projet->id==0) return false;

	$langs->load('cliatm@cliatm');
	$langs->load('contracts');
	$langs->load('commercial');
	$langs->load('main');
	$langs->load('bills');

	$projet->fetch_thirdparty();

	// Data to display
	$data = array();
	// Customer
	//$data['customer'] = $projet->thirdparty->getNomURL();
	$data['customer'] = '<a href="'.dol_buildpath('/societe/card.php', 2).'?id='.$projet->thirdparty->id.'">'.$projet->thirdparty->name.'</a>';
	$data['customer_phone'] = dol_print_phone($projet->thirdparty->phone);

	// Customer contact
	// TODO : revoir car le contact assistance est maintenant lié au contrat
	$TContact = $projet->liste_contact();
	$contact = new Contact($db);
	foreach($TContact as $TCont) {
		if($TCont['code'] == 'ASSIST') {
			$contact->fetch($TCont['id']);
			break;
		}
	}
	$data['customer_contact'] = '<a href="'.dol_buildpath('/contact/card.php', 2).'?id='.$contact->id.'">'.$contact->getFullName($langs).'</a>';
	$data['customer_contact_phone'] = $contact->phone_pro;
	$data['customer_contact_mobile'] = $contact->phone_mobile;

	// Commercial
	$TCommerciaux = $projet->thirdparty->getSalesRepresentatives($user);
	$data['commercial'] = '';
	if(! empty($TCommerciaux)) {
		$comm = new User($db);
		$comm->fetch($TCommerciaux[0]['id']);
		$data['commercial'] = '<a href="'.dol_buildpath('/user/card.php', 2).'?id='.$comm->id.'">'.$comm->getFullName($langs).'</a>';
	}

	// Chef de projets
	$data['cdp'] = '';
	$cdp = new User($db);
	$cdp->fetch($projet->thirdparty->array_options['options_fk_cdp']);
	$data['cdp'] = '<a href="'.dol_buildpath('/user/card.php', 2).'?id='.$cdp->id.'">'.$cdp->getFullName($langs).'</a>';

	// Projet et accès Dolibarr
	$data['project'] = '<a href="'.dol_buildpath('/projet/card.php', 2).'?id='.$projet->id.'">'.$projet->ref.'</a>';
	$data['access'] = '<a href="'. str_replace('/check.php', '/switch-user.php', $projet->thirdparty->array_options['options_serverobserverchecker']).'" target="_blank">'.$langs->trans('DolibarrAccess').'</a>';

	// N-1
	$datedeb = date('Y-01-01', strtotime('-1 year'));
	$datefin = date('Y-12-31', strtotime('-1 year'));
	// Heures facturées / consommées
	$data['purchasedNM1'] = getHoursPurchased($projet->id, $datedeb, $datefin);
	$data['consumedNM1'] = getHoursConsumed($projet->id, $datedeb, $datefin);

	// N
	$datedeb = date('Y-01-01');
	$datefin = date('Y-12-31');
	// Heures facturées / consommées
	$data['purchasedN'] = getHoursPurchased($projet->id, $datedeb, $datefin);
	$data['consumedN'] = getHoursConsumed($projet->id, $datedeb, $datefin);

	// Expéditions de moins de 3 mois ?
	$datedeb = date('Y-m-d', strtotime('-3 month'));
	$datefin = date('Y-m-d');
	$data['retours'] = getRecentExpeditions($projet->id, $datedeb, $datefin);

	printTabInfos($data);
}


/**
 * Renvoie le nombre d'heures d'assistance facturées entre 2 dates
 *
 * @param $projectid Id du projet
 * @param $datedeb Date de début de la période à prendre en compte
 * @param $datefin Date de fin de la période à prendre en compte
 * @return float|int Nombre d'heures facturées liées au projet
 * @throws Exception
 */
function getHoursPurchased($projectid, $datedeb, $datefin) {
	global $db;

	$sql= "SELECT s.nom, f.ref as facnumber, p.label, (CASE WHEN f.type = 0 THEN fd.qty ELSE fd.qty * -1 END) qte, pro.ref as projet, fd.date_start, fd.date_end, p.ref
			FROM ".MAIN_DB_PREFIX."facturedet fd
			LEFT JOIN ".MAIN_DB_PREFIX."facture f ON f.rowid = fd.fk_facture
			LEFT JOIN ".MAIN_DB_PREFIX."product p ON p.rowid = fd.fk_product
			LEFT JOIN ".MAIN_DB_PREFIX."societe s ON s.rowid = f.fk_soc
			LEFT JOIN ".MAIN_DB_PREFIX."projet pro ON pro.rowid = f.fk_projet
			WHERE p.ref IN ('SP015', 'SP020', 'SP022', 'SP026', 'SP029', 'SP027', 'SP031', 'SP028')
			AND f.datef BETWEEN '$datedeb' AND '$datefin'
			AND pro.rowid = $projectid
			AND f.fk_statut > 0";

	$resql = $db->query($sql);

	$total = 0;
	while($obj = $db->fetch_object($resql)) {
		if($obj->ref == 'SP028' && $obj->qte < 0) continue; // Régul négative = avoir commercial sur le dépassement. On ne le déduit pas

		$nb_mois = 1;
		if($obj->ref != 'SP028' && strtotime($obj->date_start) < strtotime('2019-08-01')) { // Factures avant le 1er aout 2019, il faut multiplier la quantité par la périodicité
			$d1 = new DateTime($obj->date_start);
			$d2 = new DateTime($obj->date_end);
			$interval = $d1->diff($d2);
			$nb_mois = $interval->m + 1;
		}

		$total += (float)$obj->qte * $nb_mois;
	}

	return $total;
}

/**
 * Renvoie le nombre d'heures consommées entre 2 dates
 *
 * @param $projectid Id du projet
 * @param $datedeb  Date de début de la période à prendre en compte
 * @param $datefin Date de fin de la période à prendre en compte
 * @return float|int Nombre d'heures consommées liées au projet
 */
function getHoursConsumed($projectid, $datedeb, $datefin) {
	global $db;

	$sql= "SELECT s.nom, p.ref, p.title, SUM(ptt.task_duration / 3600) heure
			FROM llx_projet_task_time ptt
			LEFT JOIN llx_projet_task pt ON pt.rowid = ptt.fk_task
			LEFT JOIN llx_projet p ON p.rowid = pt.fk_projet
			LEFT JOIN llx_societe s ON s.rowid = p.fk_soc
			WHERE pt.label = 'Tickets'
			AND ptt.task_date BETWEEN '$datedeb' AND '$datefin'
			AND p.rowid = $projectid
			GROUP BY p.rowid";

	$resql = $db->query($sql);

	$total = 0;
	if($obj = $db->fetch_object($resql)) {
		$total = (float)$obj->heure;
	}

	return $total;
}

function getRecentExpeditions($projectid, $datedeb, $datefin) {
	global $db;
	require_once DOL_DOCUMENT_ROOT . '/expedition/class/expedition.class.php';

	$sql= "SELECT e.rowid
			FROM llx_expedition e
			LEFT JOIN llx_expeditiondet ed ON e.rowid = ed.fk_expedition
			LEFT JOIN llx_commandedet cd ON cd.rowid = ed.fk_origin_line
			LEFT JOIN llx_product p ON cd.fk_product = p.rowid
			LEFT JOIN llx_categorie_product cp ON cp.fk_product = p.rowid
			LEFT JOIN llx_categorie c ON cp.fk_categorie = c.rowid
			LEFT JOIN llx_societe s ON e.fk_soc = s.rowid
			LEFT JOIN llx_projet pro ON pro.fk_soc = s.rowid
			WHERE COALESCE(e.date_delivery, e.date_valid) BETWEEN '$datedeb' AND '$datefin'
			AND c.rowid IN (23, 45)
			AND pro.rowid = $projectid
			GROUP BY e.rowid";

	$resql = $db->query($sql);

	$TExped = array();
	while($obj = $db->fetch_object($resql)) {
		$exp = new Expedition($db);
		$exp->fetch($obj->rowid);
		$TExped[] = $exp;
	}

	return $TExped;
}


function printTabInfos($data, $mode='customer') {
	global $langs;
	$stylewarning = 'color: red; font-weight: bold; font-size: 14px;';
	$warn = empty($data['purchasedN']) ? '  <span title="Compteur heures achetées à 0">&#9888;</span>  ' : '';

	?>
	<div id="compteur_mantis" width="100%">
		<table class="table table-bordered table-condensed" width="100%">
			<tbody>
			<tr>
				<th class="category text-center" colspan="2"><?php print $langs->trans('Customer') ?></th>
				<th class="category text-center" colspan="2"><?php print $langs->trans('Contract') ?></th>
				<th class="category text-center" colspan="2"><?php print $langs->trans('ATMContact') ?></th>
				<th class="category text-center"><?php print $langs->trans('Infos') ?></th>
			</tr>
			<tr>
				<td><?php print $data['customer'] ?></td>
				<td><?php print dol_print_phone($data['customer_phone']) ?></td>
				<th class="category"><?php print $langs->trans('Conso', date('Y')) ?></th>
				<td style="<?php ($data['consumedN'] > $data['purchasedN']) ? print $stylewarning : '' ?>"><?php print $warn.price($data['consumedN']).'h / '.price($data['purchasedN']).' h'.$warn; ?></td>
				<th class="category"><?php print $langs->trans('Commercial') ?></th>
				<td><?php print $data['commercial'] ?></td>
				<td><?php print $data['access'] ?></td>
			</tr>
			<tr>
				<td><?php print $data['customer_contact'] ?></td>
				<td><?php print dol_print_phone($data['customer_contact_phone']) ?></td>
				<th class="category"><?php print $langs->trans('Conso', date('Y') - 1) ?></th>
				<td style="<?php ($data['consumedNM1'] > $data['purchasedNM1']) ? $stylewarning : '' ?>"><?php print price($data['consumedNM1']).'h / '.price($data['purchasedNM1']).' h' ?></td>
				<th class="category"><?php print $langs->trans('CDP') ?></th>
				<td><?php print $data['cdp'] ?></td>
				<td><?php print $data['project'] ?></td>
			</tr>
			</tbody>
		</table>
		<?php
		if(!empty($data['retours'])) {
			print '<hr>PHASE RETOURS CLIENT, LISTE DES BL DE MOINS DE 3 MOIS :<hr>';
			foreach($data['retours'] as $exp) {
				print '<a href="'.dol_buildpath('/expedition/card.php', 2).'?id='.$exp->id.'">'.$exp->ref . ' - ' . dol_print_date($exp->date_delivery).'</a>';
			}
		}
		?>
	</div>
	<?php
}
