<?php
/* Copyright (C) 2014 Alexis Algoud        <support@atm-conuslting.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       /cliatm/script/migration_sepa.php
 *	\ingroup    projet
 *	\brief      Project card
 */

require('../config.php');
set_time_limit(0);

dol_include_once('/cliatm/lib/cliatm.lib.php');
dol_include_once('/cliatm/class/cliatm.class.php');
require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';

$debug = GETPOST('debug', 'int');
$limit = GETPOST('limit', 'int');
$forceRollback = GETPOST('forceRollback', 'int');

/**
 * Actions
 */
global $db, $conf;

llxHeader('', 'Migration SEPA');

$sql = 'SELECT se.idclient as fk_soc, se.rum, se.seqtp, se.dateedeal as date_rum';
$sql .= ' FROM '.MAIN_DB_PREFIX.'societe_rib as sr';
$sql .= ' INNER JOIN '.MAIN_DB_PREFIX.'societe as s ON sr.fk_soc=s.rowid';
$sql .= ' INNER JOIN '.MAIN_DB_PREFIX.'sepa as se ON s.rowid=se.idclient';
$sql .= ' WHERE sr.default_rib=1';
$sql .= ' AND se.active=1';
$sql .= ' AND (sr.rum <> se.rum OR sr.rum is null OR sr.frstrecur <> se.seqtp)';    // Permet de ne pas passer 2 fois sur le même enregistrement
if(! empty($limit)) $sql .= ' LIMIT '.$limit;

$resql = $db->query($sql);
$nbLines = $db->num_rows($resql);
if(! empty($debug)) {
	print '<pre>'.$sql.'</pre><br/>';
	print '<br/>Nb Lines : '.$nbLines;
	print '<br/>DB last error : <pre>';
	var_dump($db->lasterror);
	print '</pre><br/>';
}

if($resql) {
	$db->begin();
	for($i = 1 ; $obj = $db->fetch_object($resql) ; $i++) {
        if(! empty($debug)) {
            print '<br/><strong>--------- [LOOP '.$i.'] ---------</strong><br/><br/><pre>';
            var_dump($obj);
            print '</pre><br/>';
        }
        $soc = new Societe($db);
        $soc->fetch($obj->fk_soc);
        $TRib = $soc->get_all_rib();
        foreach($TRib as &$rib) {
            if(! empty($rib->default_rib)) {
                $rib->rum = $obj->rum;
                $rib->frstrecur = $obj->seqtp;
                $rib->date_rum = $obj->date_rum;
                $rib->update();
            }
        }
	}
    if(! empty($forceRollback)) {
        if(! empty($debug)) print '<br/><pre><strong><span style="background-color: red;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;ROLLBACK !</strong></pre><br/>';
        $db->rollback();
    }
    else {
        if(! empty($debug)) print '<br/><pre><strong><span style="background-color: green;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;COMMIT !</strong></pre><br/>';
        $db->commit();
    }
}
