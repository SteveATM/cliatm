<?php
/* Copyright (C) 2007-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  \file       webmodule_note.php
 *  \ingroup    webhost
 *  \brief      Car with notes on WebModule
 */

// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include substr($tmp, 0, ($i+1))."/main.inc.php";
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include dirname(substr($tmp, 0, ($i+1)))."/main.inc.php";
// Try main.inc.php using relative path
if (! $res && file_exists("../main.inc.php")) $res=@include "../main.inc.php";
if (! $res && file_exists("../../main.inc.php")) $res=@include "../../main.inc.php";
if (! $res && file_exists("../../../main.inc.php")) $res=@include "../../../main.inc.php";
if (! $res) die("Include of main fails");

dol_include_once('/webhost/class/webmodule.class.php');
dol_include_once('/webhost/lib/webhost.lib.php');
include_once __DIR__ .'/lib/cliatm_webhost_tools.lib.php';

// Load translation files required by the page
$langs->loadLangs(array("webhost@webhost","cliatmdolistore@cliatm","companies"));

// Get parameters
$id			= GETPOST('id', 'int');
$ref        = GETPOST('ref', 'alpha');
$action		= GETPOST('action', 'alpha');
$cancel     = GETPOST('cancel', 'aZ09');
$backtopage = GETPOST('backtopage', 'alpha');

// Initialize technical objects
$object=new WebModule($db);
$extrafields = new ExtraFields($db);
$diroutputmassaction=$conf->webhost->dir_output . '/temp/massgeneration/'.$user->id;
$hookmanager->initHooks(array('webmodulenote','globalcard'));     // Note that conf->hooks_modules contains array
// Fetch optionals attributes and labels
$extralabels = $extrafields->fetch_name_optionals_label('webmodule');

// Security check - Protection if external user
//if ($user->societe_id > 0) accessForbidden();
//if ($user->societe_id > 0) $socid = $user->societe_id;
//$result = restrictedArea($user, 'webhost', $id);

// Load object
include DOL_DOCUMENT_ROOT.'/core/actions_fetchobject.inc.php';  // Must be include, not include_once  // Must be include, not include_once. Include fetch and fetch_thirdparty but not fetch_optionals
if ($id > 0 || ! empty($ref)) $upload_dir = $conf->webhost->multidir_output[$object->entity] . "/" . $object->id;



/*
 * Actions
 */

/*
 * View
 */

$form = new Form($db);

//$help_url='EN:Customers_Orders|FR:Commandes_Clients|ES:Pedidos de clientes';
$help_url='';
llxHeader('', $langs->trans('WebModule'), $help_url);

if ($id > 0 || ! empty($ref))
{
	$object->fetch_thirdparty();

	$head = webmodulePrepareHead($object);
	$notab = -1;
	dol_fiche_head($head, 'dolistore', $langs->trans("WebModule"), $notab, $object->picto);

	// Object card
	// ------------------------------------------------------------
	$linkback = '<a href="' .dol_buildpath('/webhost/webmodule_list.php', 1) . '?restore_lastsearch_values=1' . (! empty($socid) ? '&socid=' . $socid : '') . '">' . $langs->trans("BackToList") . '</a>';

	$morehtmlref='<div class="refidno">';

	$morehtmlref.='</div>';


	dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', $morehtmlref);

	print dol_get_fiche_end($notab);

	print '<div class="underbanner clearboth"></div>';
	print '<div class="fichecenter">';


	// French
	$language ='fr';
	$dolistoreTemplate = generateDolistoreDescription($object, $language);

	print '<div class="fichehalfleft">';

	print '<h1>'.$langs->trans('TemplateFrench').' :</h1>';
	print '<h4>'.$langs->trans('TemplateSourceCode').' :</h4>';

	print '<textarea class="dolistore-output-source" style="min-width: 100%; min-height: 150px;" onclick="this.focus();this.select()" readonly="readonly" >';
	print dol_htmlentities($dolistoreTemplate, ENT_QUOTES);
	print '</textarea>';
	print '<hr/>';
	print '<h4>'.$langs->trans('TemplateView').' :</h4>';
	print '<div class="dolistore-output-view">'.$dolistoreTemplate.'</div>';
	print '</div>';

	// English
	$language ='en';
	$dolistoreTemplate = generateDolistoreDescription($object, $language);

	print '<div class="fichehalfright">';

	print '<h1>'.$langs->trans('TemplateEnglish').' :</h1>';
	print '<h4>'.$langs->trans('TemplateSourceCode').' :</h4>';

	print '<textarea class="dolistore-output-source" style="min-width: 100%; min-height: 150px;" onclick="this.focus();this.select()" readonly="readonly" >';
	print dol_htmlentities($dolistoreTemplate, ENT_QUOTES);
	print '</textarea>';
	print '<hr/>';
	print '<h4>'.$langs->trans('TemplateView').' :</h4>';
	print '<div class="dolistore-output-view">'.$dolistoreTemplate.'</div>';
	print '</div>';



	print '</div>';

}

// End of page
llxFooter();
$db->close();
