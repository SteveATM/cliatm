<?php

/**
 * Ce fichier est là juste pour les appels directs à ce virtual host
 */

header('Status: 301 Moved Permanently', false, 301);
header('Location: https://www.atm-consulting.fr');
