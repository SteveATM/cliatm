<?php


define('NOREQUIREUSER',1);	// Not disabled cause need to load personalized language
define('NOCSRFCHECK',1);
define('NOTOKENRENEWAL',1);
define('NOLOGIN',1);
define('NOREQUIREMENU',1);
define('NOREQUIREHTML',1);
define('NOREQUIREAJAX',1);
define('NOREQUIREHOOK',1);  // Disable "main.inc.php" hooks

define('INC_FROM_CRON_SCRIPT',true);
require(__DIR__.'/../config.php');
