<?php

require_once __DIR__ . '/../inc.config.php';
require_once __DIR__ . '/../../class/techatm.class.php';

global $conf;
/**
 * Ce fichier permet de mettre a jour en auto a partir d'une url atm (en dur dans nos modules) et qui dl une page html à afficher en lieu et place de la page about.
 *
 * prendre en compte le cache et stocker une version dans le dossier documents renouvelée tout les 30jours par exemple
 *
 * Doit être appeler avec une url de type https://tech.atm-consulting.fr/modules/modules-page-about.php
 */

$module = GETPOST('module', 'alphanohtml');
$module_id = GETPOST('id', 'alphanohtml');
$module_version = GETPOST('version', 'alphanohtml');
$srclang = GETPOST('langs', 'alphanohtml');

/**
 * il s'agit de la version de la class TechATM de l'appelant
 * Si jamais on change la façon de faire
 * Au moins on peut gérer des redescentes d'info différentes ex json au lieu de html simple
 */
$callv = GETPOST('callv', 'int');


$TLang = array(
	'fr_FR',
	'en_US'
);

if(!in_array($srclang, $TLang)){
	$srclang = 'fr_FR';
	if($mysoc->default_lang != 'auto'){
		$srclang = $mysoc->default_lang;
	}
}

$outputlangs = new Translate('', $conf);
$outputlangs->setDefaultLang($srclang);
$outputlangs->load('cliatmabout@cliatm');




$v = new stdClass();
$v->siteUrl = 'https://www.atm-consulting.fr';
$v->siteShort = 'www.atm-consulting.fr';
$v->contactUsUrl = $v->siteUrl . '/contact-dolibarr/';
$v->assetsPathUrl = \cliatm\TechATM::ATM_TECH_URL . '/assets/img/about-page/';
$v->phone = '09 77 19 50 70';


// NB of employees
$v->employees =  25; // vrai le 06/07/2021
$sql = "SELECT count(*) nb FROM ".MAIN_DB_PREFIX."user u WHERE u.employee > 0 AND u.statut > 0  ";
$res = $db->getRow($sql);
if($res) { $v->employees =  $res->nb; }

// NB of customers
$v->customers =  630; // vrai le 06/07/2021
$sql = "SELECT count(*) nb FROM ".MAIN_DB_PREFIX."societe s WHERE s.status > 0 AND s.client IN (1,3)  ";
$res = $db->getRow($sql);
if($res) { $v->customers =  ceil($res->nb / 10) * 10 ; }



/**
 * View file
 */
?>

<!-- PAGE ABOUT GENERATED ON <?php print date('d/m/Y H:i:s'); ?> -->
<style >
	:root{
		--atm-bleu-color: #0d3e59;
		--atm-orange-color: #e44b18;
	}

	.atm-page-about{
		font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
		color: var(--atm-bleu-color);
		letter-spacing: 0.15em;
	}

	/* ~~~~~~~~~~~~ GLOBAL SETTINGS ~~~~~~~~~~~~ */

	.atm-page-about *, .atm-page-about *:before, .atm-page-about *:after {
		box-sizing: border-box;
	}


	.developped-by-title_enphasis{
		display: inline-block;
		line-height: 48px;
		font-weight: lighter;
		background-color: var(--atm-bleu-color);
		color: #ffffff;
		padding: 0px 15px;
		letter-spacing: 0.3em;
	}
	.developped-by-title img{
		vertical-align : bottom;
	}

	.about-title{
		line-height: 48px;
		font-weight: lighter;
		background-color: var(--atm-bleu-color);
		color: #ffffff;
		text-align: center;
		padding: 0px 15px;
		letter-spacing: 0.3em;
	}


	.contactus{
		padding: 30px 0;
		text-align: center;
		color: var(--atm-bleu-color);
		font-size: 1.5em;
		font-weight: normal;
	}
	a.contactus_button{
		display: inline-block;
		margin-top: 20px;
		padding: 15px 10px;
		color: var(--atm-bleu-color);
		border-bottom: 1px solid var(--atm-orange-color);
		border-top: 1px solid var(--atm-orange-color);
		/*font-variant: small-caps;*/
		text-transform: uppercase;
		font-size: 0.7em;
	}
	a.contactus_button:hover{
		text-decoration: none;
		color: var(--atm-orange-color);
		border-bottom: 1px solid var(--atm-bleu-color);
		border-top: 1px solid var(--atm-bleu-color);
	}

	.atm-page-about h3{
		color: var(--atm-orange-color);
		font-size: 1.3em;
		font-weight: normal;
		padding-bottom: 20px;
		margin-top: 40px;
		text-align: center;
	}

	.atm-page-about h3:nth-child(even){
		color: var(--atm-bleu-color);
	}

	.atm-page-about h3 span {
		display: inline-block;
		padding-bottom: 2px;
		border-bottom: 1px solid var(--atm-bleu-color) ;
	}

	.atm-page-about__grid {
		display: flex;
		flex-wrap: wrap;
	}


	.atm-page-about__grid__item{
		flex: 1 1 320px;
	}

	.atm-page-about__grid__item.--picto-text{
		flex: 1 1 220px;
		text-align: center;
	}

	.atm-page-about__grid__item.--big-block{
		flex: 1 1 300px;
		padding: 0px 15px;
	}

	.atm-page-about__grid__item.--big-block:not(:first-child){
		border-left : 1px solid var(--atm-bleu-color);
	}


	.atm-page-about-picto-text__title {
		display: block;
		margin-top: 5px;
	}

	.strong-secondary{
		color: var(--atm-orange-color);
	}

	.strong-enphasis{
		color: var(--atm-orange-color);
	}
</style>



<section class="atm-page-about">

	<h2 class="developped-by-title" >
		<span class="developped-by-title_enphasis"><?php print $outputlangs->trans('ModuleDeveloppedBy'); ?></span>
		<a href="https://www.atm-consulting.fr"><img height="48" src="../img/ATM_logo.jpg" alt="ATM-CONSULTING" /></a>
	</h2>

	<div class="contactus" >
		<div><?php print $outputlangs->trans('ContactUsForAsking'); ?></div>
		<a class="contactus_button atm-about-btn --btn-animate-from-center" href="<?php print $v->contactUsUrl; ?>" ><?php print $v->siteShort; ?></a>
	</div>


	<h2 class="about-title" ><?php print $outputlangs->trans('AboutAtmConsultingTitle'); ?></h2>

	<div class="atm-page-about__grid">

		<div class="atm-page-about__grid__item --big-block" >
			<h3><span><?php print $outputlangs->trans('whoAreWe'); ?></span></h3>

			<div class="atm-page-about__grid">
				<div class="atm-page-about__grid__item --picto-text" >
					<img class="atm-page-about-picto-text__picture"  height="64" src="<?php print $v->assetsPathUrl;  ?>about_picto_place.png" />
					<span class="atm-page-about-picto-text__title">Situé à <strong>Valence</strong> (Drôme)</span>
				</div>

				<div class="atm-page-about__grid__item --picto-text" >
					<img class="atm-page-about-picto-text__picture"  height="64" src="<?php print $v->assetsPathUrl;  ?>about_picto_experience.png" />
					<span class="atm-page-about-picto-text__title"><?php print $outputlangs->trans('XYearsOfExp', date('Y') - 2011); ?></span>
				</div>

			</div>
			<div style="height: 20px;"></div>
			<div class="atm-page-about__grid">
				<div class="atm-page-about__grid__item --picto-text" >
					<img class="atm-page-about-picto-text__picture"  height="64" src="<?php print $v->assetsPathUrl;  ?>about_picto_collab.png" />
					<span class="atm-page-about-picto-text__title"><?php print $outputlangs->trans('NbOfEmployees', $v->employees); ?></span>
				</div>

				<div class="atm-page-about__grid__item --picto-text" >
					<img class="atm-page-about-picto-text__picture"  height="64" src="<?php print $v->assetsPathUrl;  ?>about_picto_customer.png" />
					<span class="atm-page-about-picto-text__title"><?php print $outputlangs->trans('NbOfCustomers', $v->customers); ?></span>
				</div>
			</div>
		</div>

		<div class="atm-page-about__grid__item --big-block" >
			<h3 class="grid_title"><span><?php print $outputlangs->trans('ExpertOfDolibarr'); ?></span></h3>

			<p style="font-size: 1.2em; text-align: center;">Avec un développement sur <strong class="strong-secondary" >5 solutions métiers :</strong></p>

			<p class="center"><img src="<?php print $v->assetsPathUrl;  ?>solutions.png" /></p>
		</div>

		<div class="atm-page-about__grid__item --big-block" >
			<h3><span><?php print $outputlangs->trans('ButAlso'); ?></span></h3>
			<p style="font-size: 1.2em; text-align: center;">Une nouvelle offre <strong class="strong-enphasis">"Transition digitale"</strong> : Accompagnement de la <strong>digitalisation</strong> des TPE-PME avec les  partenaires de la <strong>Galaxie ATM</strong></p>

			<p class="center"><img src="<?php print $v->assetsPathUrl;  ?>super-atm.png" /></p>

		</div>

	</div>

	<h2 class="about-title" ><?php print $outputlangs->trans('MoreInfoAboutATM'); ?></h2>


	<div class="atm-page-about__grid">

		<div class="atm-page-about__grid__item --picto-text" >
			<a href="<?php print $v->siteUrl; ?>" ><img src="<?php print $v->assetsPathUrl;  ?>clic.png" /></a>
			<span class="atm-page-about-picto-text__title"><a href="<?php print $v->siteUrl; ?>" ><?php print $v->siteShort; ?></a></span>
		</div>

		<div class="atm-page-about__grid__item --picto-text" >
			<img src="<?php print $v->assetsPathUrl;  ?>phone.png" />
			<span class="atm-page-about-picto-text__title"><?php print $v->phone;  ?></span>
		</div>

		<div class="atm-page-about__grid__item --picto-text" >
			<a href="<?php print $v->contactUsUrl; ?>" ><img src="<?php print $v->assetsPathUrl;  ?>mail.png" /></a>
			<span class="atm-page-about-picto-text__title"><a href="<?php print $v->contactUsUrl; ?>" ><?php print $outputlangs->trans('ContactUs'); ?></a></span>
		</div>

	</div>

<!--	<h3 class="left"><span>--><?php //print $outputlangs->trans('FollowUsOn'); ?><!--</span></h3>
TODO add linked in,  facebook et youtube

-->


</section>
