<?php

require_once __DIR__ . '/../inc.config.php';

global $conf;
/**
 * Utilisé pour retourner le numéro de la dernière version du module avec la variable $this->url_last_version du descripteur de module
 */

$module = GETPOST('module', 'alphanohtml');
$module_number = GETPOST('number', 'alphanohtml');
$module_version = GETPOST('version', 'alphanohtml');
$dolibarr_version = GETPOST('dolversion', 'alphanohtml');

/**
 * Il s'agit de la version de la classe TechATM de l'appelant
 * Si jamais on change la façon de faire
 * Au moins on peut gérer des redescentes d'info différentes ex json au lieu de html simple
 */
$callv = GETPOST('callv', 'int');

if(!class_exists('WebModule')) dol_include_once('./webhost/class/webmodule.class.php');

$moduleToSearch = 'mod'.$module.'_'.$module_number; // e.g modUpbuttons_104830

$webModule = new WebModule($db);
$webModule->fetch('', '', ' AND technic_name LIKE "'.$db->escape($moduleToSearch).'"');
$module_last_version = $webModule->load_last_release();

echo $module_last_version->module_version;
