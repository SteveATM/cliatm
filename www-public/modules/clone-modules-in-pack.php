<?php



require_once __DIR__ . '/../inc.config.php';
if(!class_exists('WebModule')) dol_include_once('./webhost/class/webmodule.class.php');

global $conf;

define('DEBUG', false);

//header('Content-Type: text/plain');

// TODO : SECURITY CHECK TOKEN ??

/** @var DoliDB $db */

$vertical = GETPOST('vertical', 'aZ09');
$v = GETPOST('v', 'az09');

if (empty($vertical)) {
	die('ERROR: Argument "vertical" not set. Example: "AGEFODD" or "NEGOCE".');
} elseif (empty($v)) {
	die('ERROR: Argument "v" not set. Example: "9.0" or "13.0".');
}

$sql = /** @lang SQL */
"
	SELECT
		wm.rowid id
	FROM llx_webmodule wm
		JOIN llx_webmodule_extrafields wme ON (wme.fk_object = wm.rowid)

	WHERE
	  ( wme.verticaux REGEXP '[[:<:]]" . $db->escape($vertical) . "[[:>:]]' OR wme.verticaux IN ('DEFAULT') )
	ORDER BY wm.install_name ASC
";
header('Content-Type: text/plain');

$modules = $db->getRows($sql);
//print($db->error());
if ($modules) {
	foreach($modules as $module) {
		$webModule = new WebModule($db);
		if($webModule->fetch($module->id)>0){
			$module_last_version = $webModule->load_last_release();
			if ($module_last_version) {
				printf("url=%s branch=%s dir=%s\n",
				       $webModule->git_url,
				       $module_last_version->module_version,
				       $webModule->install_name
				);
			}
		}
	}
}
else{
	print ':ERROR:';
}

